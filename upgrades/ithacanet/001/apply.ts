import { run } from '../../../scripts/utils';
import { currentProfile } from '../../../common/profiles';
import { compileExpression } from '../../../common/compile';
import { runOperation } from '../../../common/tezos-toolkit';

run(async tezos => {
    let profile = currentProfile();

    let tldRegistrar = await tezos.contract.at(profile.deployed['TLDRegistrar:ith']);

    let tldCode = await compileExpression('upgrades/ithacanet/001/001_transfer_proceeds_to_treasury.mligo', 'lambda_executed_in_registrar');

    await runOperation(async () => await tldRegistrar.methods.admin_update(tldCode).send());
});
