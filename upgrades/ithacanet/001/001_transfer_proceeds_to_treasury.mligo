#include "../../mainnet/common.mligo"

// treasury address to transfer the proceeds to
let treasury_address = ("tz1VBLpuDKMoJuHRLZ4HrCgRuiLpEr7zZx2E" : address)

// how much is locked in bidder balances (calculated off-chain)
let locked_amount = 0tez

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (store : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_actions([("Buy", main_buy); ("Renew", main_renew); ("Settle", main_settle)], store) in
        let store = { store with store = { store.store with data = Big_map.update treasury_address_key (Some (Bytes.pack treasury_address)) store.store.data } } in

        let treasury = (Tezos.get_contract_with_error treasury_address "Invalid treasury address" : unit contract) in

        // total proceeds to transfer
        let proceeds = Option.unopt (Tezos.balance - locked_amount) in

        let send_proceeds = Tezos.transaction () proceeds treasury in

        [send_proceeds], store
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (_x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "Invalid TLDRegistrar contract" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract]
    )

let multisig_payload : multisig_payload = {
    counter = 4n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload