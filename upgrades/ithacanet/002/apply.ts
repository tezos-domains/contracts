import { run } from '../../../scripts/utils';
import { currentProfile } from '../../../common/profiles';
import { compileExpression } from '../../../common/compile';
import { runOperation } from '../../../common/tezos-toolkit';

run(async tezos => {
    let profile = currentProfile();

    let tldRegistrar = await tezos.contract.at(profile.deployed['TLDRegistrar:ith']);
    let nameRegistry = await tezos.contract.at(profile.deployed['NameRegistry']);

    let tldCode = await compileExpression('upgrades/ithacanet/002/002_enable_dns_registrar.mligo', 'lambda_executed_in_registrar');
    let nrCode = await compileExpression('upgrades/ithacanet/002/002_enable_dns_registrar.mligo', 'lambda_executed_in_name_registry');

    //await runOperation(async () => await tldRegistrar.methods.admin_update(tldCode).send());
    await runOperation(async () => await nameRegistry.methods.admin_update(nrCode).send());
});
