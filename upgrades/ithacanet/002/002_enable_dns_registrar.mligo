#include "../../mainnet/common.mligo"

let new_min_auction_period = 7n * 24n * 60n * 60n // 7 days

let new_bid_additional_period = 1n * 24n * 60n * 60n // 1 day

// TODO update
let oracle_registrar = ("KT1U9S4rj35r1QqzDVsPNnuN3j143EqiXnSP" : address)

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_config([(min_auction_period_key, new_min_auction_period); (bid_additional_period_key, new_bid_additional_period)], s) in

        ([] : operation list), store
    )

let lambda_executed_in_name_registry : name_registry_admin_update_param =
    fun (s : name_registry_main_storage) -> (
        let store = upgrade_name_registry_actions([("SetChildRecord", main_setchildrecord)], s) in

        // domain name as bytes: xyz
        let xyz_key = 0x78797a in
        let xyz : record = {
            address = (None : address option);
            owner = (oracle_registrar : address);
            data = (Map.empty : data_map);
            internal_data = (Map.empty : data_map);
            expiry_key = (None : bytes option);
            level = 1n;
            tzip12_token_id = (None : nat option);
        } in
        let new_records = Big_map.update xyz_key (Some xyz) store.store.records in

        // domain name as bytes: ith
        let ith_key = 0x697468 in
        let ith = Option.unopt (Big_map.find_opt ith_key store.store.records) in
        let ith = { ith with internal_data = Map.literal [(children_are_tzip12_tokens_key, children_are_tzip12_tokens_direct)]; } in
        let new_records = Big_map.update ith_key (Some ith) new_records in

        let new_inner_store = { store.store with records = new_records } in

        ([] : operation list), { store with store = new_inner_store }
    )

let signed_payload : unit = ()
