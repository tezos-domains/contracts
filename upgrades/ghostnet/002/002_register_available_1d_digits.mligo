#include "../common.mligo"

let one_letter_expiry = ("2025-12-10T15:00:00Z" : timestamp)
let one_letter_owner = ("tz1WfxWEeGVrftwv7iQFso7ec4z2Gex7bMXD": address)

let one_letter_labels = [
    0x7431;
    0x30;
    0x31;
    0x32;
    0x33;
    0x7432;
    0x34;
    0x35;
    0x36;
    0x37;
    0x38;
    0x39;
    0x7433
]

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let store = s in
        let reg (name, ret : bytes * tld_registrar_return) : tld_registrar_return = (
            let record_opt = Big_map.find_opt name ret.1.records in
            match record_opt with
                Some tld_record -> ret
                | None -> 
                    let (ops, s) = set_record_and_store_proceeds (name, one_letter_expiry, (None : address option), one_letter_owner, (Map.empty : data_map), 0tez, ret.1) in
                    ((List.cons (Option.unopt (List.head_opt ops)) ret.0), s)
        ) in
        let (ops, new_inner_store) = List.fold_right reg one_letter_labels (([] : operation list), store.store) in
        (ops, { store with store = new_inner_store })

    )
let lambda_executed_in_multisig : unit -> operation list =
    fun (x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in

        [
            Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract;
        ]
    )

let signed_payload = lambda_executed_in_multisig