#include "../../src/contracts/TLDRegistrar/types.mligo"
#include "../../src/actions/TLDRegistrar/Bid.mligo"
#include "../../src/actions/TLDRegistrar/Buy.mligo"
#include "../../src/actions/TLDRegistrar/Commit.mligo"
#include "../../src/actions/TLDRegistrar/Renew.mligo"
#include "../../src/actions/TLDRegistrar/Settle.mligo"
#include "../../src/actions/TLDRegistrar/Withdraw.mligo"
#include "../../src/contracts/NameRegistry/types.mligo"
#include "../../src/actions/NameRegistry/BalanceOf.mligo"
#include "../../src/actions/NameRegistry/CheckAddress.mligo"
#include "../../src/actions/NameRegistry/ClaimReverseRecord.mligo"
#include "../../src/actions/NameRegistry/SetChildRecord.mligo"
#include "../../src/actions/NameRegistry/SetExpiry.mligo"
#include "../../src/actions/NameRegistry/Transfer.mligo"
#include "../../src/actions/NameRegistry/UpdateOperators.mligo"
#include "../../src/actions/NameRegistry/UpdateRecord.mligo"
#include "../../src/actions/NameRegistry/UpdateReverseRecord.mligo"
#include "../../src/shared/proxy.mligo"
#include "./multisig_params.mligo"

let tld_registrar_addr = ("KT1UZmFPpSFWFkma6yGLTJVmkvUjxTaEqXqW" : address)
let name_registry_addr = ("KT1REqKBXwULnmU6RpZxnRBUgcBmESnXhCWs" : address)
let multisig_addr = ("KT19BNY25sA9DjXyqLMKgDJkp2dHGYrR3r5G" : address)

let nameregistry_checkaddress_addr = ("KT1B3j3At2XMF5P8bVoPD2WeJbZ9eaPiu3pD" : address)
let nameregistry_setchildrecord_addr = ("KT1HpddfW7rX5aT2cTdsDaQZnH46bU7jQSTU" : address)
let nameregistry_updaterecord_addr = ("KT1Ln4t64RdCG1bK8zkH6Xi4nNQVxz7qNgyj" : address)
let nameregistry_claimreverserecord_addr = ("KT1H19ouy5QwDBchKXcUw1QRFs5ZYyx1ezEJ" : address)
let nameregistry_updatereverserecord_addr = ("KT1HDUc2xtPHqWQcjE1WuinTTHajXQN3asdk" : address)
let tldregistrar_buy_addr = ("KT1Ks7BBTLLjD9PsdCboCL7fYEfq8z1mEvU1" : address)
let tldregistrar_renew_addr = ("KT1Bv32pdMYmBJeMa2HsyUQZiC6FNj1dX6VR" : address)
let tldregistrar_commit_addr = ("KT1PEnPDgGKyHvaGzWj6VJJYwobToiW2frff" : address)
let tldregistrar_bid_addr = ("KT1P3wdbusZK2sj16YXxRViezzWCPXpiE28P" : address)
let tldregistrar_withdraw_addr = ("KT1C7EF4c1pnPW9qcfNRiTPj5tBFMQJtvUhq" : address)
let tldregistrar_settle_addr = ("KT1DMNPg3b3fJQpjXULcXjucEXfwq3zGTKGo" : address)

let target_chain_id = ("NetXnHfVqm9iesp" : chain_id)

type config_update = (nat * nat)
type config_map = (nat, nat) map

type tld_registrar_action_update = (string * tld_registrar_action)
type name_registry_action_update = (string * name_registry_action)

let upgrade_tld_registrar_actions(action_updates, store: tld_registrar_action_update list * tld_registrar_main_storage): tld_registrar_main_storage =
    let update_action = fun(actions, update : tld_registrar_action_map * tld_registrar_action_update) -> Big_map.update update.0 (Some update.1) actions in

    let new_actions = List.fold_left update_action store.actions action_updates in

    { store with actions = new_actions }

let upgrade_name_registry_actions(action_updates, store: name_registry_action_update list * name_registry_main_storage): name_registry_main_storage =
    let update_action = fun(actions, update : name_registry_action_map * name_registry_action_update) -> Big_map.update update.0 (Some update.1) actions in

    let new_actions = List.fold_left update_action store.actions action_updates in

    { store with actions = new_actions }

let upgrade_tld_registrar_config(config_updates, store: config_update list * tld_registrar_main_storage): tld_registrar_main_storage = 
    let set_config = fun(config, prop : config_map * config_update) -> Map.update prop.0 (Some prop.1) config in
    let new_config = List.fold_left set_config store.store.config config_updates in
    let new_storage = { store.store with config = new_config } in
    
    { store with store = new_storage }
