// Represents parameter types in https://github.com/murbard/smart-contracts/blob/master/multisig/michelson/generic.tz

// Change key action param
type multisig_change_keys = [@layout:comb] {
    threshold: nat;
    keys: key list;
}

// Type of action to perform
type multisig_payload_action = [@layout:comb]
    | Operation of unit -> (operation list)
    | Change_keys of multisig_change_keys

// Signable payload
type multisig_payload = [@layout:comb] {
    counter: nat;
    action: multisig_payload_action;
}

// Main entrypoint param
type multisig_param = [@layout:comb] {
    payload: multisig_payload;
    sigs: signature option list;
}
