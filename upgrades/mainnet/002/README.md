# Upgrade 002: Add FA2 operators

## Upgrade summary

-   Add FA2 operator functionality to NameRegistry
-   Move contract metadata of NameRegistry to IPFS (see https://ipfs.io/ipfs/bafybeieyczpwpcx3v3ompr35h4i2osjv4wdmnjysxfueb63qyv6b44c2fq)

## Verifying that Michelson content matches Ligo source (optional)

(requires Ligo 0.22 installed)

```
ligo compile-expression --init-file=002_operators.mligo cameligo signed_payload 2>/dev/null \
    | diff -bB 002_operators.tz -
```

## Signing with tezos-client

First, we need to generate a packed representation:

```
tezos-client hash data "$(cat 002_operators.tz)" of type "$(cat ../multisig_signed_type.tz)"
```

This should give output that looks like:

```
Raw packed data: <long byte sequence>
...
Ledger Blake2b hash: ChhbsYWSMBsAyUpRk9M22SDSU7DZV33wVeChZdbYyVN3
...
```

To sign:

1. Verify that the hash above matches yours.
2. Copy the byte sequence on the first line and run:

```
tezos-client sign bytes <long byte sequence> for <your account>
```

During this step, you can verify the hash also matches the hash displayed on your Ledger device.
