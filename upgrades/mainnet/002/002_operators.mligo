#include "../common.mligo"

let lambda_executed_in_name_registry : name_registry_admin_update_param =
    fun (s : name_registry_main_storage) -> (
        let store = upgrade_name_registry_actions([("Transfer", main_transfer); ("UpdateOperators", main_updateoperators); ("UpdateRecord", main_updaterecord)], s) in

        let new_metadata = Big_map.remove "contents" store.store.metadata in

        // metadata url as bytes: ipfs://bafybeieyczpwpcx3v3ompr35h4i2osjv4wdmnjysxfueb63qyv6b44c2fq
        let new_metadata = Big_map.update "" (Some 0x697066733a2f2f626166796265696579637a70777063783376336f6d70723335683469326f736a763477646d6e6a7973786675656236337179763662343463326671) new_metadata in

        let new_inner_store = { store.store with metadata = new_metadata } in

        ([] : operation list), { store with store = new_inner_store }
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (x : unit) -> (
        let name_registry_contract = (match (Tezos.get_entrypoint_opt "%admin_update" name_registry_addr : name_registry_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : name_registry_admin_update_param contract)
            | Some c -> c) in 
        [Tezos.transaction lambda_executed_in_name_registry 0mutez name_registry_contract]
    )

let multisig_payload : multisig_payload = {
    counter = 2n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload