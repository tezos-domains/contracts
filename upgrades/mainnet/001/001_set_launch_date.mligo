#include "../../../src/contracts/TLDRegistrar/TLDRegistrar.mligo"
#include "../multisig_params.mligo"

// Wed, 21 Apr 2021 08:00:00 (UTC)
let launch_date = 1618992000n

let tld_registrar_addr = ("KT1Mqx5meQbhufngJnUAGEGpa4ZRxhPSiCgB" : address)

let multisig_addr = ("KT1W56o8dK5En5hM46VsD1zKtgpqWPhs3bLh" : address)

let target_chain_id = ("NetXdQprcVkpaWU" : chain_id)

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let new_config = Map.add launch_date_key_start launch_date s.store.config in
        let new_storage = { s.store with config = new_config } in
        ([] : operation list), { s with store = new_storage }
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (x : unit) -> (
        let contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez contract]
    )

let multisig_payload : multisig_payload = {
    counter = 1n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload