# Upgrade 001: Tezos Domains Launch

## Upgrade summary

The TLDRegistrar launch date is updated to `Wed, 21 Apr 2021 08:00:00 (UTC)` for domain labels of 5 letters or more.

## Verifying that Michelson content matches Ligo source (optional)

(requires Ligo 0.12 installed)

```
ligo compile-expression --init-file=001_set_launch_date.mligo cameligo signed_payload 2>/dev/null \
    | diff -bB 001_set_launch_date.tz -
```

## Signing with tezos-client

First, we need to generate a packed representation:

```
tezos-client hash data "$(cat 001_set_launch_date.tz)" of type "$(cat ../multisig_signed_type.tz)"
```

This should give output that looks like:

```
Raw packed data: <long byte sequence>
...
Ledger Blake2b hash: 8CoWm5vUDG7TNf2MCtCE8uLcYT8XUEAQxgEm9KfrHpne
...
```

To sign:

1. Verify that the hash above matches yours.
2. Copy the byte sequence on the first line and run:

```
tezos-client sign bytes <long byte sequence> for <your account>
```

During this step, you can verify the hash also matches the hash displayed on your Ledger device.
