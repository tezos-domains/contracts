#include "../common.mligo"

let one_letter_launch_date = 1730473200n // 2024-11-01T15:00:00.000Z
let one_letter_price = 8_219_178_082_191n // 3000tez
let one_letter_expiry = ("2025-12-10T15:00:00Z" : timestamp)
let one_letter_owner = ("tz1WfxWEeGVrftwv7iQFso7ec4z2Gex7bMXD": address)
let two_letter_launch_date = 1733842800n // 2024-12-10T15:00:00.000Z
let two_letter_price = 1_780_821_917_808n // 650tez

let one_letter_labels = [
    0x61;
    0x62;
    0x63;
    0x64;
    0x65;
    0x66;
    0x67;
    0x68;
    0x69;
    0x6a;
    0x6b;
    0x6c;
    0x6d;
    0x6e;
    0x6f;
    0x70;
    0x71;
    0x72;
    0x73;
    0x74;
    0x75;
    0x76;
    0x77;
    0x78;
    0x79;
    0x7a
]

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_config([
            (1001n, one_letter_launch_date);
            (1002n, two_letter_launch_date);
            (2001n, one_letter_price);
            (2002n, two_letter_price)
        ], s) in
        let reg (name, ret : bytes * tld_registrar_return) : tld_registrar_return = (
            let (ops, s) = set_record_and_store_proceeds (name, one_letter_expiry, (None : address option), one_letter_owner, (Map.empty : data_map), 0tez, ret.1) in
            ((List.cons (Option.unopt (List.head_opt ops)) ret.0), s)
        ) in
        let (ops, new_inner_store) = List.fold_right reg one_letter_labels (([] : operation list), store.store) in
        (ops, { store with store = new_inner_store })

    )
let lambda_executed_in_multisig : unit -> operation list =
    fun (x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in

        [
            Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract;
        ]
    )

let signed_payload = lambda_executed_in_multisig