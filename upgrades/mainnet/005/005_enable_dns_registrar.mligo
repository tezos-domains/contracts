#include "../common.mligo"

let new_min_auction_period = 7n * 24n * 60n * 60n // 7 days

let new_bid_additional_period = 1n * 24n * 60n * 60n // 1 day

// TODO update
let oracle_registrar = ("KT1MMkh1VMT8d5BVuBP2whf7RXCWeMdUfkXu" : address)

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_config([(min_auction_period_key, new_min_auction_period); (bid_additional_period_key, new_bid_additional_period)], s) in

        ([] : operation list), store
    )

let lambda_executed_in_name_registry : name_registry_admin_update_param =
    fun (s : name_registry_main_storage) -> (
        let store = upgrade_name_registry_actions([("SetChildRecord", main_setchildrecord)], s) in

        // domain name as bytes: xyz
        let xyz_key = 0x78797a in
        let xyz : record = {
            address = (None : address option);
            owner = (oracle_registrar : address);
            data = (Map.empty : data_map);
            internal_data = (Map.empty : data_map);
            expiry_key = (None : bytes option);
            level = 1n;
            tzip12_token_id = (None : nat option);
        } in
        let new_records = Big_map.update xyz_key (Some xyz) store.store.records in

        // domain name as bytes: tez
        let tez_key = 0x74657a in
        let tez = Option.unopt (Big_map.find_opt tez_key store.store.records) in
        let tez = { tez with internal_data = Map.literal [(children_are_tzip12_tokens_key, children_are_tzip12_tokens_direct)]; } in
        let new_records = Big_map.update tez_key (Some tez) new_records in

        let new_inner_store = { store.store with records = new_records } in

        ([] : operation list), { store with store = new_inner_store }
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (_x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        let name_registry_contract = (match (Tezos.get_entrypoint_opt "%admin_update" name_registry_addr : name_registry_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : name_registry_admin_update_param contract)
            | Some c -> c) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract; Tezos.transaction lambda_executed_in_name_registry 0mutez name_registry_contract]
    )

let multisig_payload : multisig_payload = {
    counter = 6n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload
