#include "../common.mligo"

// treasury address to transfer the proceeds to
let treasury_address = ("KT1W56o8dK5En5hM46VsD1zKtgpqWPhs3bLh" : address)

// proceeds to be transfered (calculated off-chain)
let proceeds_amount = 257050281000mutez

// delegate
let delegate = ("tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q" : key_hash)

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (store : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_actions([("Buy", main_buy); ("Renew", main_renew); ("Settle", main_settle)], store) in
        let store = { store with store = { store.store with data = Big_map.update treasury_address_key (Some (Bytes.pack treasury_address)) store.store.data } } in

        let treasury = (Tezos.get_contract_with_error treasury_address "Invalid treasury address" : unit contract) in

        let send_proceeds = Tezos.transaction () proceeds_amount treasury in

        [send_proceeds], store
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (_x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "Invalid TLDRegistrar contract" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        let set_delegate = Tezos.set_delegate (Some delegate) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract; set_delegate]
    )

let multisig_payload : multisig_payload = {
    counter = 4n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload