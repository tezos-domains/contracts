# Upgrade 004: Move proceeds to treasury

## About this upgrade

### Motivation

As the first step in our strategy towards establishing the Tezos Domains DAO, we propose using the current keyholders multisig (`KT1W56o8dK5En5hM46VsD1zKtgpqWPhs3bLh`) as an interim treasury for Tezos Domains. This setup would be in place until the Tezos Domain DAO upgrade is implemented.

Using the multisig as a temporary treasury lets us test the revenue redirection mechanism and allows staking the full amount stored in the treasury.

### Proceeds

All proceeds stored in the TLD registrar contract will move to the multisig. Only the balance required to return all [auction balances](https://tezos.domains/about/auctions) is kept locked in the contract.

This required remaining balance was calculated off-chain to be `9008180000 mutez` using the following command:

```
curl "https://api.tzkt.io/v1/contracts/KT1Mqx5meQbhufngJnUAGEGpa4ZRxhPSiCgB/bigmaps/bidder_balances/keys?limit=10000" | jq '[.[] | select(.active == true) | .value | tonumber] | add'
```

The contract balance at the time of preparing this upgrade was `266058461000 mutez`. This leaves `257050281000 mutez` as the transferable proceeds. The exact proceeds at the time of transfer can't be calculated on-chain. A small portion of the total proceeds will likely be left as part of the contract balance.

### Contract upgrade

The TLD registrar contract is upgraded so that:

-   A configurable treasury address is introduced with the initial value of `KT1W56o8dK5En5hM46VsD1zKtgpqWPhs3bLh`.
-   All new registration and renewal transactions automatically redirect the transaction amount to the treasury.
-   Auction settle transactions send the highest bid of the auction (the auction proceeds) to the treasury, withdrawing from the contract's balance.

### Delegation

The treasury will stake it's balance using the delegate `tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q` ([Happy Tezos](https://www.happytezos.com/)). A different delegate picked by the community can be used once the funds have moved to a permanent treasury.

## Optional: Verifying that Michelson content matches Ligo source

(requires Ligo 0.42 installed)

```
ligo compile expression --init-file 004_transfer_proceeds_to_treasury.mligo cameligo signed_payload 2>/dev/null \
    | diff -bB 004_transfer_proceeds_to_treasury.tz -
```

## Signing with tezos-client

First, we need to generate a packed representation:

```
tezos-client hash data "$(cat 004_transfer_proceeds_to_treasury.tz)" of type "$(cat ../multisig_signed_type.tz)"
```

This should give output that looks like:

```
Raw packed data: <long byte sequence>
...
Ledger Blake2b hash: 9Q1jV7B7G8qprVVsj4t3wWHLurfNdyaEYHgSfVtVZB5P
...
```

To sign:

1. Verify that the hash above matches yours.
2. Copy the byte sequence on the first line and run:

```
tezos-client sign bytes <long byte sequence> for <your account>
```

During this step, you should verify the hash also matches the hash displayed on your Ledger device.
