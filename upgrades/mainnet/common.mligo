#include "../../src/contracts/TLDRegistrar/types.mligo"
#include "../../src/actions/TLDRegistrar/Bid.mligo"
#include "../../src/actions/TLDRegistrar/Buy.mligo"
#include "../../src/actions/TLDRegistrar/Commit.mligo"
#include "../../src/actions/TLDRegistrar/Renew.mligo"
#include "../../src/actions/TLDRegistrar/Settle.mligo"
#include "../../src/actions/TLDRegistrar/Withdraw.mligo"
#include "../../src/contracts/NameRegistry/types.mligo"
#include "../../src/actions/NameRegistry/BalanceOf.mligo"
#include "../../src/actions/NameRegistry/CheckAddress.mligo"
#include "../../src/actions/NameRegistry/ClaimReverseRecord.mligo"
#include "../../src/actions/NameRegistry/SetChildRecord.mligo"
#include "../../src/actions/NameRegistry/SetExpiry.mligo"
#include "../../src/actions/NameRegistry/Transfer.mligo"
#include "../../src/actions/NameRegistry/UpdateOperators.mligo"
#include "../../src/actions/NameRegistry/UpdateRecord.mligo"
#include "../../src/actions/NameRegistry/UpdateReverseRecord.mligo"
#include "../../src/shared/proxy.mligo"
#include "./multisig_params.mligo"

let tld_registrar_addr = ("KT1Mqx5meQbhufngJnUAGEGpa4ZRxhPSiCgB" : address)
let name_registry_addr = ("KT1GBZmSxmnKJXGMdMLbugPfLyUPmuLSMwKS" : address)
let multisig_addr = ("KT1W56o8dK5En5hM46VsD1zKtgpqWPhs3bLh" : address)

let nameregistry_checkaddress_addr = ("KT1F7JKNqwaoLzRsMio1MQC7zv3jG9dHcDdJ" : address)
let nameregistry_setchildrecord_addr = ("KT1QHLk1EMUA8BPH3FvRUeUmbTspmAhb7kpd" : address)
let nameregistry_updaterecord_addr = ("KT1H1MqmUM4aK9i1833EBmYCCEfkbt6ZdSBc" : address)
let nameregistry_claimreverserecord_addr = ("KT1TnTr6b2YxSx2xUQ8Vz3MoWy771ta66yGx" : address)
let nameregistry_updatereverserecord_addr = ("KT1J9VpjiH5cmcsskNb8gEXpBtjD4zrAx4Vo" : address)
let tldregistrar_buy_addr = ("KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr" : address)
let tldregistrar_renew_addr = ("KT1EVYBj3f1rZHNeUtq4ZvVxPTs77wuHwARU" : address)
let tldregistrar_commit_addr = ("KT1P8n2qzJjwMPbHJfi4o8xu6Pe3gaU3u2A3" : address)
let tldregistrar_bid_addr = ("KT1CaSP4dn8wasbMsfdtGiCPgYFW7bvnPRRT" : address)
let tldregistrar_withdraw_addr = ("KT1CfuAbJQbAGYcjKfvEvbtNUx45LY5hfTVR" : address)
let tldregistrar_settle_addr = ("KT1MeFfi4TzSCc8CF9j3qq5mecTPdc6YVUPp" : address)

let target_chain_id = ("NetXdQprcVkpaWU" : chain_id)

type config_update = (nat * nat)
type config_map = (nat, nat) map

type tld_registrar_action_update = (string * tld_registrar_action)
type name_registry_action_update = (string * name_registry_action)

let upgrade_tld_registrar_actions(action_updates, store: tld_registrar_action_update list * tld_registrar_main_storage): tld_registrar_main_storage =
    let update_action = fun(actions, update : tld_registrar_action_map * tld_registrar_action_update) -> Big_map.update update.0 (Some update.1) actions in

    let new_actions = List.fold_left update_action store.actions action_updates in

    { store with actions = new_actions }

let upgrade_name_registry_actions(action_updates, store: name_registry_action_update list * name_registry_main_storage): name_registry_main_storage =
    let update_action = fun(actions, update : name_registry_action_map * name_registry_action_update) -> Big_map.update update.0 (Some update.1) actions in

    let new_actions = List.fold_left update_action store.actions action_updates in

    { store with actions = new_actions }

let upgrade_tld_registrar_config(config_updates, store: config_update list * tld_registrar_main_storage): tld_registrar_main_storage = 
    let set_config = fun(config, prop : config_map * config_update) -> Map.update prop.0 (Some prop.1) config in
    let new_config = List.fold_left set_config store.store.config config_updates in
    let new_storage = { store.store with config = new_config } in
    
    { store with store = new_storage }
