#include "../common.mligo"

let new_owner = ("KT1BzeXvLtPR83aj5FHemXmia6DmdXkeV3Uk" : address)

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (store : tld_registrar_main_storage) -> (
        let new_storage = { store with store = {
            store.store with
                data = Big_map.update treasury_address_key (Some (Bytes.pack new_owner)) store.store.data;
                owner = new_owner
        } } in
        ([] : operation list), new_storage
    )

let lambda_executed_in_name_registry : name_registry_admin_update_param =
    fun (store : name_registry_main_storage) -> (
        let new_storage = { store with store = { store.store with owner = new_owner } } in
        ([] : operation list), new_storage
    )

let lambda_executed_in_proxy : proxy_admin_update_param =
    fun (store : proxy_storage) -> (
        let new_storage = { store with owner = new_owner } in
        ([] : operation list), new_storage
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (_x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        let name_registry_contract = (match (Tezos.get_entrypoint_opt "%admin_update" name_registry_addr : name_registry_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : name_registry_admin_update_param contract)
            | Some c -> c) in
        let get_proxy (addr : address) = (match (Tezos.get_entrypoint_opt "%proxy_admin_update" addr : proxy_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : proxy_admin_update_param contract)
            | Some c -> c) in
        let nameregistry_checkaddress_contract = get_proxy nameregistry_checkaddress_addr in
        let nameregistry_setchildrecord_contract = get_proxy nameregistry_setchildrecord_addr in
        let nameregistry_updaterecord_contract = get_proxy nameregistry_updaterecord_addr in
        let nameregistry_claimreverserecord_contract = get_proxy nameregistry_claimreverserecord_addr in
        let nameregistry_updatereverserecord_contract = get_proxy nameregistry_updatereverserecord_addr in
        let tldregistrar_buy_contract = get_proxy tldregistrar_buy_addr in
        let tldregistrar_renew_contract = get_proxy tldregistrar_renew_addr in
        let tldregistrar_commit_contract = get_proxy tldregistrar_commit_addr in
        let tldregistrar_bid_contract = get_proxy tldregistrar_bid_addr in
        let tldregistrar_withdraw_contract = get_proxy tldregistrar_withdraw_addr in
        let tldregistrar_settle_contract = get_proxy tldregistrar_settle_addr in

        let new_owner_contract = (match (Tezos.get_contract_opt new_owner : unit contract option) with
            None -> (failwith "INVALID_CONTRACT" : unit contract)
            | Some c -> c) in
        [
            (Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract);
            (Tezos.transaction lambda_executed_in_name_registry 0mutez name_registry_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez nameregistry_checkaddress_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez nameregistry_setchildrecord_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez nameregistry_updaterecord_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez nameregistry_claimreverserecord_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez nameregistry_updatereverserecord_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_buy_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_renew_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_commit_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_bid_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_withdraw_contract);
            (Tezos.transaction lambda_executed_in_proxy 0mutez tldregistrar_settle_contract);
            (Tezos.transaction () Tezos.balance new_owner_contract);
        ]
    )

let multisig_payload : multisig_payload = {
    counter = 10n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload
