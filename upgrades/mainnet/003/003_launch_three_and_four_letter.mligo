#include "../common.mligo"

// TODO: update launch dates and prices
let four_letter_launch_date = 1633089600n
let three_letter_launch_date = 1635771600n
let four_letter_price = 68_493_150_685n
let three_letter_price = 273_972_602_739n

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (s : tld_registrar_main_storage) -> (
        let store = upgrade_tld_registrar_config([(1003n, three_letter_launch_date); (1004n, four_letter_launch_date); (2003n, three_letter_price); (2004n, four_letter_price)], s) in
        let store = upgrade_tld_registrar_actions([("Bid", main_bid); ("Buy", main_buy); ("Renew", main_renew)], store) in

        let new_metadata = Big_map.remove "contents" store.store.metadata in

        // metadata url as bytes: ipfs://bafybeihkbwzrr3m3eqtjkm2zmbqzd6lj4psbr7yldnibvyjc7k4pjkbohq
        let new_metadata = Big_map.update "" (Some 0x697066733a2f2f62616679626569686b62777a7272336d336571746a6b6d327a6d62717a64366c6a347073627237796c646e696276796a63376b34706a6b626f6871) new_metadata in

        let new_inner_store = { store.store with metadata = new_metadata } in

        ([] : operation list), { store with store = new_inner_store }
    )

let lambda_executed_in_name_registry : name_registry_admin_update_param =
    fun (s : name_registry_main_storage) -> (
        // domain name as bytes: usdtz.tez
        let usdtz_key = 0x757364747a2e74657a in
        let usdtz = match Big_map.find_opt usdtz_key s.store.records with
            Some r -> { r with owner = ("tz1MkwtFgjrMAHK5BAfLzKGhinrNHnB1ftHT": address) }
            | None -> (failwith "RECORD_NOT_FOUND": record) in
        let new_records = Big_map.update usdtz_key (Some usdtz) s.store.records in

        let new_inner_store = { s.store with records = new_records } in

        ([] : operation list), { s with store = new_inner_store }
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        let name_registry_contract = (match (Tezos.get_entrypoint_opt "%admin_update" name_registry_addr : name_registry_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : name_registry_admin_update_param contract)
            | Some c -> c) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract; Tezos.transaction lambda_executed_in_name_registry 0mutez name_registry_contract]
    )

let multisig_payload : multisig_payload = {
    counter = 3n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload