# Upgrade 003: Launch of 3 and 4 letter domains

## Upgrade summary

-   Launch date is updated to `Friday, Oct 1 2021 at 14:00` for 4 letter domains.
-   Launch date is updated to `Monday, Nov 1 2021 at 14:00` for 3 letter domains.
-   Price per day is updated to `25 tez per year` for 4 letter domains.
-   Price per day is updated to `100 tez per year` for 3 letter domains.
-   Based on a request from the owner of `usdtz.tez`, the domain is transferred back to the last owner address to undo this operation: https://better-call.dev/mainnet/opg/ooLQtGLUi5bkz1GAo7EL9MwStuU46czqLY3rVLsVHUsBJgpiFqH/contents
    The owner of the domain contacted us and confirmed that the operation was made in error. We have verified the owner's identity.

## Verifying that Michelson content matches Ligo source (optional)

(requires Ligo 0.22 installed)

```
ligo compile-expression --init-file=003_launch_three_and_four_letter.mligo cameligo signed_payload 2>/dev/null \
    | diff -bB 003_launch_three_and_four_letter.tz -
```

## Signing with tezos-client

First, we need to generate a packed representation:

```
tezos-client hash data "$(cat 003_launch_three_and_four_letter.tz)" of type "$(cat ../multisig_signed_type.tz)"
```

This should give output that looks like:

```
Raw packed data: <long byte sequence>
...
Ledger Blake2b hash: YdbkQ1mrk1UdyYr5WbDxAqttxx29ho4zXc7WtvjLUv6
...
```

To sign:

1. Verify that the hash above matches yours.
2. Copy the byte sequence on the first line and run:

```
tezos-client sign bytes <long byte sequence> for <your account>
```

During this step, you can verify the hash also matches the hash displayed on your Ledger device.
