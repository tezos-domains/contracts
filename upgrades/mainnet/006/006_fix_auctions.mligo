#include "../common.mligo"

let lambda_executed_in_registrar : tld_registrar_admin_update_param =
    fun (store : tld_registrar_main_storage) -> (
        let new_store = upgrade_tld_registrar_actions([("Bid", main_bid); ("Buy", main_buy); ("Settle", main_settle)], store) in

        ([] : operation list), new_store
    )

let lambda_executed_in_multisig : unit -> operation list =
    fun (_x : unit) -> (
        let tld_registrar_contract = (match (Tezos.get_entrypoint_opt "%admin_update" tld_registrar_addr : tld_registrar_admin_update_param contract option) with
            None -> (failwith "INVALID_CONTRACT" : tld_registrar_admin_update_param contract)
            | Some c -> c) in
        [Tezos.transaction lambda_executed_in_registrar 0mutez tld_registrar_contract]
    )

let multisig_payload : multisig_payload = {
    counter = 8n;
    action = Operation lambda_executed_in_multisig;
}

let replay_protection = target_chain_id, multisig_addr

let signed_payload = replay_protection, multisig_payload
