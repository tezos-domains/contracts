import { PathLike, readdirSync, stat, statSync, writeFileSync } from 'fs';
import { basename, dirname, extname, join } from 'path';
import { compileExpression } from '../common/compile';

async function buildRecursively(directory: string): Promise<any> {
    const ligoExtension = '.mligo';
    for (let entry of readdirSync(directory, { withFileTypes: true })) {
        let fullName = join(directory, entry.name);
        if (entry.isDirectory()) {
            await buildRecursively(fullName);
        } else if (entry.isFile() && /^\d\d\d_/.test(entry.name) && extname(entry.name) === ligoExtension) {
            console.log(`Compiling ${fullName}...`);
            let source = await compileExpression(fullName, 'signed_payload', true);
            writeFileSync(`${join(dirname(fullName), basename(fullName, ligoExtension))}.tz`, source);
        }
    }
}

(async () => {
    await buildRecursively('upgrades');
    console.log(`All successful.`);
})().catch(e => console.error(e));
