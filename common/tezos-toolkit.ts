import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { Operation } from '@taquito/taquito/dist/types/operations/operations';
import { Tzip16Module } from '@taquito/tzip16';
import { delay } from '../scripts/utils';
import { currentProfile } from './profiles';

export function tezosToolkit(): TezosToolkit {
    const profile = currentProfile();
    let rpc = process.env.RPC_URL || profile.rpc;
    let toolkit = new TezosToolkit(rpc);
    toolkit.setProvider({
        signer: new InMemorySigner(profile.secretKey),
        config: {
            confirmationPollingIntervalSecond: 1,
            confirmationPollingTimeoutSecond: 120,
        },
    });
    toolkit.addExtension(new Tzip16Module());
    return toolkit;
}

export async function runOperation<T extends Operation>(what: () => Promise<T>): Promise<T> {
    let op = await what();
    await op.confirmation();
    return op;
}
