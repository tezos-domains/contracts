const configKeys = {
    maxCommitmentAge: 0,
    minCommitmentAge: 1,
    minDuration: 3,
    minBidIncreaseRatio: 4,
    minAuctionPeriod: 5,
    bidAdditionalPeriod: 6,

    launchDate(len?: number) {
        return 1000 + (len || 0);
    },
    standardPricePerDay(len?: number) {
        return len ? 2000 + len : 2;
    },
} as const;

export default configKeys;
