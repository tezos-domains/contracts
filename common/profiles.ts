import accounts from './sandbox-accounts';

const alice = accounts.alice;

function profiles(): Record<string, object> {
    return {
        default: {
            // used for local tests
            rpc: 'http://localhost:8732',
            ownerAddress: alice.pkh,
            secretKey: alice.sk,
        },
        edonet: {
            rpc: 'https://edonet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'edo',
            minCommitmentAge: 30,
            deployed: require('../deployed/edonet.json'),
        },
        florencenet: {
            rpc: 'https://florencenet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'flo',
            minCommitmentAge: 30,
            deployed: require('../deployed/florencenet.json'),
        },
        granadanet: {
            rpc: 'https://granadanet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'gra',
            minCommitmentAge: 30,
            deployed: require('../deployed/granadanet.json'),
        },
        hangzhounet: {
            rpc: 'https://hangzhounet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'han',
            minCommitmentAge: 15,
            deployed: require('../deployed/hangzhounet.json'),
        },
        ithacanet: {
            rpc: 'https://ithacanet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'ith',
            minCommitmentAge: 15,
            deployed: require('../deployed/ithacanet.json'),
            treasury: process.env.OWNER_ADDRESS,
        },
        jakartanet: {
            rpc: 'https://jakartanet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'jak',
            minCommitmentAge: 15,
            deployed: require('../deployed/jakartanet.json'),
            treasury: process.env.OWNER_ADDRESS,
        },
        kathmandunet: {
            rpc: 'https://rpc.kathmandunet.teztnets.xyz',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'kat',
            minCommitmentAge: 15,
            deployed: require('../deployed/kathmandunet.json'),
            treasury: process.env.OWNER_ADDRESS,
        },
        limanet: {
            rpc: 'https://rpc.limanet.teztnets.xyz',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'lim',
            minCommitmentAge: 15,
            deployed: require('../deployed/limanet.json'),
            treasury: process.env.OWNER_ADDRESS,
        },
        ghostnet: {
            rpc: 'https://ghostnet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            tld: 'gho',
            minCommitmentAge: 15,
            deployed: require('../deployed/ghostnet.json'),
            treasury: process.env.OWNER_ADDRESS,
        },
        fakenet: {
            rpc: 'https://florencenet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // mainnet specific settings
            tld: 'tez',
            minCommitmentAge: 60,
            minAuctionPeriod: 2 * 24 * 60 * 60,
            bidAdditionalPeriod: 4 * 60 * 60,
            launchDates: [null, null, null, null, null],
            deployed: require('../deployed/fakenet.json'),
        },
        mainnet: {
            rpc: 'https://mainnet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // mainnet specific settings
            tld: 'tez',
            minCommitmentAge: 60,
            minAuctionPeriod: 21 * 24 * 60 * 60,
            bidAdditionalPeriod: 2 * 24 * 60 * 60,
            launchDates: [null, null, null, null, null],
            deployed: require('../deployed/mainnet.json'),
        },
    };
}

export function currentProfile(): any {
    let name = process.env.PROFILE || 'default';
    let profile = profiles()[name];
    if (!profile) {
        throw new Error(`No such profile: ${name}`);
    }
    return profile;
}
