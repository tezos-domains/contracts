import { compileExpression } from '../compile';
import { buildCommonMetadata } from './common.metadata';

export default async function createMetadata() {
    let resolvedType = {
        prim: 'option',
        args: [
            {
                prim: 'pair',
                args: [
                    { prim: 'bytes', args: [], annots: ['%name'] },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'option', args: [{ prim: 'address' }], annots: ['%address'] },
                            {
                                prim: 'pair',
                                args: [
                                    { prim: 'map', args: [{ prim: 'string' }, { prim: 'bytes' }], annots: ['%data'] },
                                    { prim: 'option', args: [{ prim: 'timestamp' }], annots: ['%expiry'] },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    };

    return {
        ...buildCommonMetadata('NameRegistry'),
        interfaces: ['TZIP-012-11e599f9'],
        permissions: {
            operator: 'owner-or-operator-transfer',
            receiver: 'owner-no-hook',
            sender: 'owner-no-hook',
        },
        views: [
            {
                name: 'resolve-name',
                description: 'Resolves a name to an address, and optionally other data.',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: {
                                prim: 'bytes',
                                args: [],
                                annots: [],
                            },
                            returnType: resolvedType,
                            code: await compileExpression('src/views/NameRegistry/ResolveName.mligo', 'main'),
                        },
                    },
                ],
            },
            {
                name: 'resolve-address',
                description: 'Resolves an address to a name.',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: {
                                prim: 'address',
                                args: [],
                                annots: [],
                            },
                            returnType: resolvedType,
                            code: await compileExpression('src/views/NameRegistry/ResolveAddress.mligo', 'main'),
                        },
                    },
                ],
            },

            // tzip-12
            {
                name: 'get_balance',
                description: 'Returns the balance of a given token_id for an owner',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: {
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%owner'] },
                                    { prim: 'nat', annots: ['%token_id'] },
                                ],
                            },
                            returnType: { prim: 'nat' },
                            code: await compileExpression('src/views/NameRegistry/GetBalance.mligo', 'main'),
                        },
                    },
                ],
            },
            {
                name: 'total_supply',
                description: 'Returns the total supply of a given token',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: { prim: 'nat' },
                            returnType: { prim: 'nat' },
                            code: await compileExpression('src/views/NameRegistry/TotalSupply.mligo', 'main'),
                        },
                    },
                ],
            },
            {
                name: 'is_operator',
                description: 'Returns whether the specified address is an operator of a given token',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: {
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%owner'] },
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'address', annots: ['%operator'] },
                                            { prim: 'nat', annots: ['%token_id'] },
                                        ],
                                    },
                                ],
                            },
                            returnType: { prim: 'bool' },
                            code: await compileExpression('src/views/NameRegistry/IsOperator.mligo', 'main'),
                        },
                    },
                ],
            },
            {
                name: 'token_metadata',
                description: 'Returns the TZIP-12 metadata of a given token',
                implementations: [
                    {
                        michelsonStorageView: {
                            parameter: { prim: 'nat' },
                            returnType: {
                                prim: 'pair',
                                args: [
                                    { prim: 'nat', annots: ['%token_id'] },
                                    {
                                        prim: 'map',
                                        args: [{ prim: 'string' }, { prim: 'bytes' }],
                                        annots: ['%token_info'],
                                    },
                                ],
                            },
                            code: await compileExpression('src/views/NameRegistry/TokenMetadata.mligo', 'main'),
                        },
                    },
                ],
            },
        ],
    };
}
