import { MichelsonMap, MichelsonMapKey } from '@taquito/michelson-encoder';
import { encodeString } from '../convert';

export function buildCommonMetadata(name: string): Record<string, any> {
    return {
        description: `Tezos Domains ${name}`,
        version: `${name} v${require('../../package.json').version}`,
        license: {
            name: 'Apache-2.0',
            details: 'Apache License 2.0',
        },
        homepage: 'https://tezos.domains/',
    };
}

export function generateCommonMetadataMap(name: string): MichelsonMap<MichelsonMapKey, unknown> {
    const metadata = buildCommonMetadata(name);
    return MichelsonMap.fromLiteral({
        '': encodeString('tezos-storage:contents'),
        contents: encodeString(JSON.stringify(metadata)),
    });
}
