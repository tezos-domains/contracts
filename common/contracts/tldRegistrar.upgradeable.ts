import { packDataBytes } from '@taquito/michel-codec';
import { Contract, MichelsonMap } from '@taquito/taquito';
import { dateToTimestamp, encodeString } from '../convert';
import nameRegistry_addTrustedSenders from '../lambdas/nameRegistry_addTrustedSenders';
import tldRegistrar_addTrustedSenders from '../lambdas/tldRegistrar_addTrustedSenders';
import tldRegistrar_setAction from '../lambdas/tldRegistrar_setAction';
import { currentProfile } from '../profiles';
import { runOperation, tezosToolkit } from '../tezos-toolkit';
import configKeys from '../tldRegistrar.configKeys';
import { Upgradeable } from '../upgradeables';
import { generateCommonMetadataMap } from './common.metadata';
import { nameRegistryUpgradeable } from './nameRegistry.upgradeable';

async function nameRegistryContract(): Promise<Contract> {
    return nameRegistryUpgradeable.deployed
        ? nameRegistryUpgradeable.lastDeployment.contract
        : await tezosToolkit().contract.at(currentProfile().deployed['NameRegistry']);
}

const dataKeys = {
    treasuryAddress: 'ff00',
};

export const tldRegistrarUpgradeable = new Upgradeable(
    'TLDRegistrar',
    async (owner, config) => {
        let nameRegistry = await nameRegistryContract();

        // set up the config
        let tldConfig = new MichelsonMap({ prim: 'map', args: [{ prim: 'nat' }, { prim: 'nat' }] });
        tldConfig.set(configKeys.maxCommitmentAge, 24 * 60 * 60);
        tldConfig.set(configKeys.minCommitmentAge, config.minCommitmentAge || 0);
        tldConfig.set(configKeys.standardPricePerDay(), 2739726027); // 1tez per year
        tldConfig.set(configKeys.minDuration, config.minDuration ?? 365);
        tldConfig.set(configKeys.minBidIncreaseRatio, config.minBidIncreaseRatio ?? 10);
        tldConfig.set(configKeys.minAuctionPeriod, config.minAuctionPeriod ?? 14 * 24 * 60 * 60);
        tldConfig.set(configKeys.bidAdditionalPeriod, config.bidAdditionalPeriod ?? 24 * 60 * 60);
        tldConfig.set(configKeys.launchDate(), dateToTimestamp(new Date(2020, 1, 1))); // set a default

        // fill the launch dates per label length
        if (config.launchDates) {
            for (let i = 0; i < config.launchDates.length; i++) {
                tldConfig.set(configKeys.launchDate(i), config.launchDates[i] ? dateToTimestamp(config.launchDates[i]) : 0);
            }
        }

        // fill the standard prices per label length
        if (config.standardPrices) {
            for (let i = 0; i < config.standardPrices.length; i++) {
                if (config.standardPrices[i] != null) {
                    tldConfig.set(configKeys.standardPricePerDay(i), config.standardPrices[i]);
                }
            }
        }

        // setup the data
        const data = new MichelsonMap();
        if (config.treasury) {
            data.set(dataKeys.treasuryAddress, packDataBytes({ string: config.treasury }, { prim: 'address' }).bytes);
        }

        return {
            tld: encodeString(config.tld || 'tez'),
            records: new MichelsonMap(),
            commitments: new MichelsonMap(),
            auctions: new MichelsonMap(),
            bidder_balances: new MichelsonMap(),
            owner,
            config: tldConfig,
            name_registry: nameRegistry.address,
            data,
            metadata: generateCommonMetadataMap('TLDRegistrar'),
        };
    },
    tldRegistrar_setAction,
    tldRegistrar_addTrustedSenders,
    async (contract, statusReporter = () => {}) => {
        // we need to be trusted by NameRegistrar because of inter-contract calls
        let nameRegistry = await nameRegistryContract();

        statusReporter(`Adding ${contract.address} as trusted caller of ${nameRegistry.address}`);
        await runOperation(async () => await nameRegistry.methods.admin_update(await nameRegistry_addTrustedSenders(contract.address)).send());
    },

    [
        // TLDRegistrar actions
        { name: 'Commit', proxy: true },
        { name: 'Buy', proxy: true },
        { name: 'Renew', proxy: true },
        { name: 'Bid', proxy: true },
        { name: 'Withdraw', proxy: true },
        { name: 'Settle', proxy: true },
    ]
);
