import { Contract, MichelsonMap } from '@taquito/taquito';
import { encodeString } from '../convert';
import nameRegistry_addTrustedSenders from '../lambdas/nameRegistry_addTrustedSenders';
import nameRegistry_appendMetadata from '../lambdas/nameRegistry_appendMetadata';
import nameRegistry_setAction from '../lambdas/nameRegistry_setAction';
import { runOperation } from '../tezos-toolkit';
import { Upgradeable } from '../upgradeables';
import createMetadata from './nameRegistry.metadata';

export const nameRegistryUpgradeable = new Upgradeable(
    'NameRegistry',
    async (owner, config) => {
        return {
            records: new MichelsonMap(),
            reverse_records: new MichelsonMap(),
            expiry_map: new MichelsonMap(),
            owner,
            data: new MichelsonMap(),
            metadata: MichelsonMap.fromLiteral({
                '': encodeString('tezos-storage:contents'),
                // ... JSON contents filled in separately later
            }),
            tzip12_tokens: new MichelsonMap(),
            next_tzip12_token_id: 1,
        };
    },
    nameRegistry_setAction,
    nameRegistry_addTrustedSenders,
    async (contract, statusReporter = () => {}) => {
        // the contract is so big we need to fill the metadata contents in chunks, otherwise we are hitting 16kbytes per operation limit
        let metadataStr = JSON.stringify(await createMetadata());
        const chunkSize = 8000;
        for (let i = 0; i * chunkSize < metadataStr.length; i++) {
            statusReporter(`Initializing metadata (${i + 1} out of ${Math.ceil(metadataStr.length / chunkSize)})...`);
            let chunk = metadataStr.substring(i * chunkSize, (i + 1) * chunkSize);
            await runOperation(async () => await contract.methods.admin_update(await nameRegistry_appendMetadata(chunk)).send());
        }
    },

    [
        // NameRegistry actions
        { name: 'SetChildRecord', proxy: true },
        { name: 'UpdateRecord', proxy: true },
        { name: 'ClaimReverseRecord', proxy: true },
        { name: 'UpdateReverseRecord', proxy: true },
        { name: 'CheckAddress', proxy: true },
        { name: 'SetExpiry', proxy: false },

        // tzip-12 specific actions
        { name: 'BalanceOf', proxy: false },
        { name: 'Transfer', proxy: false },
        { name: 'UpdateOperators', proxy: false },
    ]
);
