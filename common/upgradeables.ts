import accounts from '../common/sandbox-accounts';
import { MichelsonMap, Schema } from '@taquito/michelson-encoder';
import { InMemorySigner } from '@taquito/signer';
import { Contract } from '@taquito/taquito';
import { compileContract, compileExpression } from './compile';
import { tezosToolkit, runOperation } from './tezos-toolkit';
import { currentProfile } from './profiles';
import { generateCommonMetadataMap } from './contracts/common.metadata';

const alice = accounts.alice;

type UpgradeableDeploymentAction = {
    proxyContract: Contract;
    parameterType: any;
};

function entrypointName(s: string) {
    return s
        .replace(/\.?([A-Z]+)/g, '_$1')
        .toLowerCase()
        .replace(/^_/, '');
}

export class UpgradeableDeployment {
    constructor(public contract: Contract, public proxies: Record<string, UpgradeableDeploymentAction>) {}

    encodeParameter(parameter: any, parameterType: any) {
        const schema = new Schema(parameterType);
        return schema.Encode(parameter);
    }

    async callAction(actionName: string, parameter: any, parameterType: any, senderPkh: string, amount = 0) {
        senderPkh = senderPkh || alice.pkh;

        const value = this.encodeParameter(parameter, parameterType);
        let bytesParam = (await tezosToolkit().rpc.packData({ data: value, type: parameterType })).packed;

        return await runOperation(() => this.contract.methods.execute(actionName, bytesParam, senderPkh).send({ amount }));
    }

    async callProxy(actionName: string, parameter: any, senderSk: string, amount = 0) {
        let tezos = tezosToolkit();
        if (senderSk) {
            tezos.setSignerProvider(new InMemorySigner(senderSk));
        }

        let encodedParam = this.encodeParameter(parameter, this.proxies[actionName].parameterType);

        let op = await tezos.contract.transfer({
            to: this.proxies[actionName].proxyContract.address,
            amount: amount,
            parameter: {
                entrypoint: entrypointName(actionName),
                value: encodedParam,
            },
        });
        await op.confirmation();
        return op;
    }
}

type UpgradeableAction = {
    name: string;
    proxy: boolean;
};

export class Upgradeable {
    #lastDeployment?: UpgradeableDeployment = undefined;

    get deployed() {
        return !!this.#lastDeployment;
    }

    get lastDeployment(): UpgradeableDeployment {
        if (!this.#lastDeployment) {
            throw new Error(`No ${this.name} deployment yet`);
        }
        return this.#lastDeployment;
    }

    constructor(
        public name: string,
        private defaultInnerStorageFactory: (owner: string, config: any) => any,
        private setActionFactory: (name: string, michelson: any) => Promise<void>,
        private addTrustedSenderFactory: (newTrustedSender: string | Array<string>) => Promise<any>,
        private afterDeploy: (contract: Contract, statusReporter: (msg: string) => void) => Promise<void>,
        public actions: Array<UpgradeableAction>
    ) {}

    async deploy(config: any = {}, innerStorage: any = {}, addresses: Record<string, string> = {}, statusReporter: (msg: string) => void = () => {}) {
        // prepare storage & deploy
        let tezos = tezosToolkit();
        let owner = currentProfile().ownerAddress;
        let storage = {
            store: {
                ...(await this.defaultInnerStorageFactory(owner, config)),
                ...(innerStorage || {}),
            },
            actions: new MichelsonMap(),
            trusted_senders: [],
        };

        const nameWithTld = this.name + (config.tld ? `:${config.tld}` : '');
        statusReporter(`Originating ${nameWithTld}...`);
        let originationOp = await runOperation(
            async () =>
                await tezos.contract.originate({
                    code: await compileContract(`src/contracts/${this.name}/${this.name}.mligo`),
                    storage,
                    balance: config.balance,
                })
        );
        statusReporter(`Originated ${originationOp.contractAddress}.`);

        let contract = await tezos.contract.at(originationOp.contractAddress!);
        addresses[nameWithTld] = contract.address;

        // deploy all actions and proxies
        let proxies: Record<string, UpgradeableDeploymentAction> = {};
        for (let action of this.actions) {
            if (config.actionNames && !config.actionNames.includes(action.name)) {
                continue;
            }

            // store the action in the contract's actions bigmap
            let actionCode = await compileExpression(`src/actions/${this.name}/${action.name}.mligo`, `main_${action.name.toLowerCase()}`);

            statusReporter(`Updating action ${action.name}...`);
            await runOperation(async () => await contract.methods.admin_update(await this.setActionFactory(action.name, actionCode)).send());

            // deploy proxy
            if (action.proxy) {
                let proxyName = `${this.name}.${action.name}`;
                let code = await compileContract(`src/contracts/${this.name}/proxies/${proxyName}.mligo`);

                statusReporter(`Originating proxy ${action.name}...`);
                let proxyOriginationOp = await runOperation(
                    async () =>
                        await tezos.contract.originate({
                            code,
                            storage: {
                                contract: contract.address,
                                owner,
                                metadata: generateCommonMetadataMap(proxyName),
                            },
                        })
                );
                statusReporter(`Originated ${proxyOriginationOp.contractAddress}.`);
                let proxyContract = await tezos.contract.at(proxyOriginationOp.contractAddress!);

                let entrypoint = entrypointName(action.name);
                let parameterType = code[0].args[0].args.filter((a: any) => a.annots[0] == `%${entrypoint}`)[0];
                delete parameterType.annots;
                proxies[action.name] = {
                    proxyContract,
                    parameterType,
                };
                addresses[proxyName + (config.tld ? `:${config.tld}` : '')] = proxyContract.address;
            }
        }

        // trust the proxies
        statusReporter(`Adding all proxies as trusted senders...`);
        await runOperation(
            async () => await contract.methods.admin_update(await this.addTrustedSenderFactory(Object.values(proxies).map(p => p.proxyContract.address))).send()
        );

        await this.afterDeploy(contract, statusReporter);

        statusReporter(`${this.name} deployed succesfully.`);

        this.#lastDeployment = new UpgradeableDeployment(contract, proxies);
        return this.#lastDeployment;
    }
}

module.exports = {
    Upgradeable,
    UpgradeableDeployment,
};
