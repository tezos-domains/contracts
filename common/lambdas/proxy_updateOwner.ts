export default function proxy_updateOwner(newOwner: string): any {
    return [
        { prim: 'CAR' },
        { prim: 'PUSH', args: [{ prim: 'address' }, { string: newOwner }] },
        { prim: 'SWAP' },
        { prim: 'PAIR' },
        { prim: 'NIL', args: [{ prim: 'operation' }] },
        { prim: 'PAIR' },
    ];
}
