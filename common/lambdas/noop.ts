const noop = [{ prim: 'NIL', args: [{ prim: 'operation' }] }, { prim: 'PAIR' }];

export default noop;
