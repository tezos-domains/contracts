import compileLambda from './compileLambda';

export default async function tldRegistrar_setOwner(owner: string): Promise<any> {
    return await compileLambda(`src/lambdas/TLDRegistrar/SetOwner.mligo`, [{ search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: owner }]);
}
