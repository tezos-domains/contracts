import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function (address: string, name: string | undefined, owner: string): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/SetReverseRecord.mligo`, [
        { search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: address },
        { search: '7a7a7a7a7a', replacement: name ? encodeString(name) : null, type: 'bytes' },
        { search: 'tz1aoQSwjDU4pxSwT5AsBiK5Xk15FWgBJoYr', replacement: owner },
    ]);
}
