import { dateToTimestamp, encodeString } from '../convert';
import configKeys from '../tldRegistrar.configKeys';
import compileLambda from './compileLambda';

interface TldRegistrarConfigOptions {
    maxCommitmentAge: number;
    minCommitmentAge: number;
    minDuration: number;
    minBidIncreaseRatio: number;
    minAuctionPeriod: number;
    launchDates: (Date | null)[];
    standardPrices: (number | null)[];
    bidAdditionalPeriod: number;
    tld: string;
    nameRegistryAddress: string;
}

export default async function tldRegistrar_updateConfig(options: TldRegistrarConfigOptions): Promise<any> {
    let config: Record<number, number> = {};
    config[configKeys.maxCommitmentAge] = options.maxCommitmentAge;
    config[configKeys.minCommitmentAge] = options.minCommitmentAge;
    config[configKeys.minDuration] = options.minDuration;
    config[configKeys.minBidIncreaseRatio] = options.minBidIncreaseRatio;
    config[configKeys.minAuctionPeriod] = options.minAuctionPeriod;
    config[configKeys.bidAdditionalPeriod] = options.bidAdditionalPeriod;
    for (let i = 0; i < options.launchDates.length; i++) {
        config[configKeys.launchDate(i)] = options.launchDates[i] ? dateToTimestamp(options.launchDates[i]!) : 0;
    }
    for (let i = 0; i < options.standardPrices.length; i++) {
        const pricePerDay = options.standardPrices[i];
        if (pricePerDay != null) {
            config[configKeys.standardPricePerDay(i)] = pricePerDay;
        }
    }
    return await compileLambda(`src/lambdas/TLDRegistrar/UpdateConfig.mligo`, [
        { search: '74657a', replacement: encodeString(options.tld) },
        { search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: options.nameRegistryAddress },
        {
            searchJson: [
                { prim: 'PUSH', args: [{ prim: 'nat' }, { int: '999' }] },
                { prim: 'SOME' },
                { prim: 'PUSH', args: [{ prim: 'nat' }, { int: '9' }] },
                { prim: 'UPDATE' },
            ],
            replacementJson: Object.entries(config).flatMap(e => [
                { prim: 'PUSH', args: [{ prim: 'nat' }, { int: e[1].toString() }] },
                { prim: 'SOME' },
                { prim: 'PUSH', args: [{ prim: 'nat' }, { int: e[0] }] },
                { prim: 'UPDATE' },
            ]),
        },
    ]);
}
