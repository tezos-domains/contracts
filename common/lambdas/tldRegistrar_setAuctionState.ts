import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function tldRegistrar_setAuctionState(
    label: string,
    lastBid: number,
    lastBidder: string,
    endsAt: Date,
    registrationDuration: number
): Promise<any> {
    return await compileLambda(`src/lambdas/TLDRegistrar/SetAuctionState.mligo`, [
        { search: '74657a', replacement: encodeString(label) },
        { search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: lastBidder },
        { search: '123000000', replacement: lastBid },
        { search: '555', replacement: registrationDuration },
        { search: '2000-01-01T10:10:10Z', replacement: endsAt.toISOString() },
    ]);
}
