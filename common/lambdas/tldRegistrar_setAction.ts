import compileLambda from './compileLambda';

export default async function tldRegistrar_setAction(name: string, michelson: any): Promise<any> {
    return await compileLambda(`src/lambdas/TLDRegistrar/SetAction.mligo`, [
        { search: 'ActionName', replacement: name },
        {
            searchJson: [{ prim: 'CDR' }, { prim: 'NIL', args: [{ prim: 'operation' }] }, { prim: 'PAIR' }],
            replacementJson: michelson,
        },
    ]);
}
