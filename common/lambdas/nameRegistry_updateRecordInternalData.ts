import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function nameRegistry_updateRecordInternalData(name: string, dataKey: string, valueBytes: any): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/UpdateRecordInternalData.mligo`, [
        { search: '74657a', replacement: encodeString(name), type: 'bytes' },
        { search: 'what', replacement: dataKey, type: 'string' },
        { search: '7a7a7a7a7a', replacement: valueBytes, type: 'bytes' },
    ]);
}
