import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function nameRegistry_setRecord(
    name: string,
    owner: string,
    level: number | undefined,
    expiryKey?: string,
    address?: string
): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/SetRecord.mligo`, [
        { search: '74657a', replacement: encodeString(name) },
        { search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: owner },
        { search: '999', replacement: level || 1 },
        { search: '7a7a7a7a7a', replacement: expiryKey ? encodeString(expiryKey) : null, type: 'bytes' },
        { search: 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb', replacement: address, type: 'address' },
    ]);
}
