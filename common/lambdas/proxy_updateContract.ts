export default function proxy_updateContract(newAddress: string): any {
    return [
        { prim: 'DUP' },
        { prim: 'CDR' },
        { prim: 'SWAP' },
        { prim: 'CAR' },
        { prim: 'CDR' },
        { prim: 'PUSH', args: [{ prim: 'address' }, { string: newAddress }] },
        { prim: 'PAIR' },
        { prim: 'PAIR' },
        { prim: 'NIL', args: [{ prim: 'operation' }] },
        { prim: 'PAIR' },
    ];
}
