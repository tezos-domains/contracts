import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function nameRegistry_appendMetadata(metadataStr: string): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/AppendMetadata.mligo`, [{ search: '1234', replacement: encodeString(metadataStr) }]);
}
