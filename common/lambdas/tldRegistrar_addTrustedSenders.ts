import compileLambda from './compileLambda';

export default async function tldRegistrar_addTrustedSenders(newTrustedSender: string | Array<string>): Promise<any> {
    let senders = Array.isArray(newTrustedSender) ? newTrustedSender : [newTrustedSender];
    return await compileLambda(`src/lambdas/NameRegistry/AddTrustedSenders.mligo`, [
        {
            searchJson: [{ prim: 'PUSH', args: [{ prim: 'address' }, { string: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj' }] }, { prim: 'CONS' }],
            replacementJson: senders.flatMap(a => [{ prim: 'PUSH', args: [{ prim: 'address' }, { string: a }] }, { prim: 'CONS' }]),
        },
    ]);
}
