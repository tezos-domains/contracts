import compileLambda from './compileLambda';

export default async function nameRegistry_setOwner(owner: string): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/SetOwner.mligo`, [{ search: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj', replacement: owner }]);
}
