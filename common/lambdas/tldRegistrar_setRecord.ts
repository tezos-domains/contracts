import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function tldRegistrar_setRecord(label: string, expirationDate: Date): Promise<any> {
    return await compileLambda(`src/lambdas/TLDRegistrar/SetRecord.mligo`, [
        { search: '74657a', replacement: encodeString(label) },
        { search: '2000-01-01T10:10:10Z', replacement: expirationDate.toISOString() },
    ]);
}
