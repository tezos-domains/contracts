import compileLambda from './compileLambda';

export default async function nameRegistry_setAction(name: string, michelson: any): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/SetAction.mligo`, [
        { search: 'ActionName', replacement: name },
        {
            searchJson: [{ prim: 'CDR' }, { prim: 'NIL', args: [{ prim: 'operation' }] }, { prim: 'PAIR' }],
            replacementJson: michelson,
        },
    ]);
}
