import assert from 'assert';
import { compileExpression } from '../compile';

type CompileLambdaParam = {
    replacementJson?: any;
    replacement?: any;
    searchJson?: any;
    search?: any;
    type?: string;
};

function normalizeParameter(param: CompileLambdaParam) {
    if (!param.replacementJson && !param.replacement) {
        return {
            searchJson: [{ prim: 'PUSH', args: [{ prim: param.type }, { [michelsonTypeToPropertyName(param.type!)]: param.search }] }, { prim: 'SOME' }],
            replacementJson: [{ prim: 'NONE', args: [{ prim: param.type }] }],
        };
    }
    return {
        searchJson: param.searchJson || param.search,
        replacementJson: param.replacementJson || param.replacement.toString(),
    };
}

function replace(json: any, searchJson: any, replacementJson: any): any {
    if (deepEqual(json, searchJson)) {
        return replacementJson;
    }
    if (Array.isArray(json)) {
        if (Array.isArray(searchJson)) {
            for (let i = 0; i < json.length - searchJson.length + 1; i++) {
                if (deepEqual(json.slice(i, i + searchJson.length), searchJson)) {
                    return json
                        .slice(0, i)
                        .concat(replacementJson)
                        .concat(json.slice(i + searchJson.length));
                }
            }
        }
        let found = false;
        var mapped = json.map(x => {
            let result = replace(x, searchJson, replacementJson);
            if (result !== null) {
                found = true;
                return result;
            }
            return x;
        });
        return found ? mapped : null;
    }
    if (typeof json === 'object') {
        let found = false;
        let mapped = Object.fromEntries(
            Object.entries(json).map(([property, oldValue]) => {
                let newValue: any = replace(oldValue, searchJson, replacementJson);
                if (newValue !== null) {
                    found = true;
                    return [property, newValue];
                }
                return [property, oldValue];
            })
        );
        return found ? mapped : null;
    }
    return null;
}

function michelsonTypeToPropertyName(type: string) {
    switch (type) {
        case 'nat':
            return 'int';
        case 'address':
            return 'string';
        default:
            return type;
    }
}

function deepEqual(json1: any, json2: any) {
    try {
        assert.deepStrictEqual(json1, json2);
        return true;
    } catch {
        return false;
    }
}

export default async function compileLambda(path: string, parameters: CompileLambdaParam[]): Promise<any> {
    let jsonObj = await compileExpression(path, 'lambda');
    for (let param of parameters.map(normalizeParameter)) {
        let newObj = replace(jsonObj, param.searchJson, param.replacementJson);
        if (newObj === null) {
            throw new Error(`parameter not found: ${JSON.stringify(param.searchJson)}`);
        }
        jsonObj = newObj;
    }
    return jsonObj;
}
