import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function nameRegistry_generateTzip12Ids(names: Array<string>): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/GenerateTzip12Ids.mligo`, [
        {
            searchJson: [{ prim: 'PUSH', args: [{ prim: 'bytes' }, { bytes: '7a7a7a7a7a' }] }, { prim: 'CONS' }],
            replacementJson: names.flatMap(a => [{ prim: 'PUSH', args: [{ prim: 'bytes' }, { bytes: encodeString(a) }] }, { prim: 'CONS' }]),
        },
    ]);
}
