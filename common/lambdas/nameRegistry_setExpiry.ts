import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export async function nameRegistry_setExpiry(name: string, expiry: Date): Promise<any> {
    return await compileLambda(`src/lambdas/NameRegistry/SetExpiry.mligo`, [
        { search: '74657a', replacement: encodeString(name) },
        { search: '2000-01-01T10:10:10Z', replacement: expiry.toISOString() },
    ]);
}
