import { encodeString } from '../convert';
import compileLambda from './compileLambda';

export default async function nameRegistry_updateRecordData(name: string, dataKey: string, dataValue: any): Promise<any> {
    const json = JSON.stringify(dataValue);
    return await compileLambda(`src/lambdas/NameRegistry/UpdateRecordData.mligo`, [
        { search: '74657a', replacement: encodeString(name), type: 'bytes' },
        { search: 'what', replacement: dataKey, type: 'string' },
        { search: '7a7a7a7a7a', replacement: encodeString(json), type: 'bytes' },
    ]);
}
