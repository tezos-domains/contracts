import { spawn } from 'child_process';

function unixPath(path: string) {
    if (process.platform == 'win32') {
        return path.replace(/\\/g, '/');
    }
    return path;
}

function execLigo(args: Array<string>): Promise<string> {
    return new Promise((resolve, reject) => {
        const ligoImage = 'ligolang/ligo:0.42.0';

        // Use spawn() instead of exec() here so that the OS can take care of escaping args.
        let docker = spawn('docker', ['run', '-v', `${process.cwd()}:/project`, '-w', '/project', '--rm', '-i', ligoImage].concat(args));

        let stdout = '';
        let stderr = '';

        docker.stdout.on('data', data => {
            stdout += data;
        });

        docker.stderr.on('data', data => {
            stderr += data;
        });

        docker.on('close', code => {
            if (code != 0) {
                reject(stderr || stdout);
                return;
            }

            const jsonContractOutput = stdout.trim();

            resolve(jsonContractOutput);
        });
    });
}

export async function compileExpression(sourcePath: string, expression: string, textOutput: boolean = false): Promise<any> {
    let output = await execLigo([
        'compile',
        'expression',
        '--michelson-format',
        textOutput ? 'text' : 'json',
        '--protocol',
        'ithaca',
        '--init-file',
        unixPath(sourcePath),
        'cameligo',
        expression,
    ]);
    if (textOutput) {
        return output;
    } else {
        return JSON.parse(output);
    }
}

export async function compileContract(sourcePath: string): Promise<any> {
    return JSON.parse(await execLigo(['compile', 'contract', '--michelson-format', 'json', '-e', 'main', unixPath(sourcePath)]));
}
