export const operators = {
    key: 'operators',
} as const;

export const childrenAreTzip12Tokens = {
    key: 'tzip12_children',
    values: {
        direct: '00',
    } as const,
} as const;
