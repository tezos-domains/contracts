import { sha512 } from 'js-sha512';
import { encodeString, hexToArray } from './convert';
import { tezosToolkit } from './tezos-toolkit';

const buyParam = (label: string, owner: string, nonce: number) => ({
    prim: 'Pair',
    args: [
        {
            prim: 'Pair',
            args: [
                {
                    bytes: encodeString(label),
                },
                {
                    string: owner,
                },
            ],
        },
        {
            int: nonce.toString(),
        },
    ],
});

const buyType = {
    prim: 'pair',
    args: [
        {
            prim: 'pair',
            args: [
                {
                    prim: 'bytes',
                },
                {
                    prim: 'address',
                },
            ],
        },
        {
            prim: 'nat',
        },
    ],
};

export default async function createCommitment(label: string, owner: string, nonce: number): Promise<string> {
    const data = buyParam(label, owner, nonce);
    let packedParam = (await tezosToolkit().rpc.packData({ data, type: buyType })).packed;
    return sha512(hexToArray(packedParam));
}
