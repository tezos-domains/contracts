import nameRegistry_setRecord from '../common/lambdas/nameRegistry_setRecord';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { currentProfile } from '../common/profiles';
import { run } from './utils';
import { runOperation } from '../common/tezos-toolkit';
import nameRegistry_updateRecordInternalData from '../common/lambdas/nameRegistry_updateRecordInternalData';
import { OpKind } from '@taquito/rpc';
import { childrenAreTzip12Tokens } from '../common/nameRegistry.internalData';

run(async tezos => {
    const profile = currentProfile();
    let addresses = {};

    // create registry
    let nameRegistry = await nameRegistryUpgradeable.deploy({}, {}, addresses, console.log);

    // create TLD registrar
    let defaultTld = profile.tld || 'tez';
    let tldRegistrar = await tldRegistrarUpgradeable.deploy(
        {
            tld: defaultTld,
            minCommitmentAge: profile.minCommitmentAge,
            minAuctionPeriod: profile.minAuctionPeriod,
            bidAdditionalPeriod: profile.bidAdditionalPeriod,
            launchDates: profile.launchDates,
            standardPrices: profile.standardPrices,
            treasury: profile.treasury,
        },
        {},
        addresses,
        console.log
    );

    // create the tez domain
    console.log('Bootstrapping the top-level domain...');
    const params = [
        nameRegistry.contract.methods.admin_update(await nameRegistry_setRecord(defaultTld, tldRegistrar.contract.address, 1)).toTransferParams(),
        nameRegistry.contract.methods
            .admin_update(await nameRegistry_updateRecordInternalData(defaultTld, childrenAreTzip12Tokens.key, childrenAreTzip12Tokens.values.direct))
            .toTransferParams(),
    ];
    await runOperation(async () => await tezos.contract.batch(params.map(p => ({ kind: OpKind.TRANSACTION, ...p }))).send());

    console.log(`All successful.`);
    console.log(`Deployed addresses:`);
    console.log(JSON.stringify(addresses, null, 2));
});
