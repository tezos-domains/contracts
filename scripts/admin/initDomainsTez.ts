import { run } from '../utils';
import tldRegistrar_setRecord from '../../common/lambdas/tldRegistrar_setRecord';
import { currentProfile } from '../../common/profiles';
import { runOperation } from '../../common/tezos-toolkit';
import nameRegistry_updateRecordData from '../../common/lambdas/nameRegistry_updateRecordData';
import nameRegistry_setRecord from '../../common/lambdas/nameRegistry_setRecord';
import nameRegistry_setReverseRecord from '../../common/lambdas/nameRegistry_setReverseRecord';

run(async tezos => {
    let profile = currentProfile();
    let registry = await tezos.contract.at(profile.deployed['NameRegistry']);
    let tldRegistrar = await tezos.contract.at(profile.deployed[`TLDRegistrar:${profile.tld}`]);

    let op;

    console.log(`creating 2LD record`);
    let d = new Date();
    d.setFullYear(d.getFullYear() + 1000);
    op = await tldRegistrar.methods.admin_update(await tldRegistrar_setRecord('domains', d)).send();
    await op.confirmation();

    async function setContractAddress(owner: string, address: string, label: string, parent: string, level: number): Promise<string> {
        let name = `${label}.${parent}`;
        console.log(`creating registry record ${name} <-> ${address} (owned by ${owner})`);

        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord(name, owner, level, undefined, address)).send());
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setReverseRecord(address, name, owner)).send());
        await runOperation(
            async () => await registry.methods.admin_update(await nameRegistry_updateRecordData(name, 'openid:email', 'hello@tezos.domains')).send()
        );
        return name;
    }

    let parent = await setContractAddress(tldRegistrar.address, tldRegistrar.address, 'domains', profile.tld, 2);

    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Buy:${profile.tld}`], 'buy', parent, 3);
    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Commit:${profile.tld}`], 'commit', parent, 3);
    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Renew:${profile.tld}`], 'renew', parent, 3);
    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Bid:${profile.tld}`], 'bid', parent, 3);
    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Settle:${profile.tld}`], 'settle', parent, 3);
    await setContractAddress(tldRegistrar.address, profile.deployed[`TLDRegistrar.Withdraw:${profile.tld}`], 'withdraw', parent, 3);

    let registryName = await setContractAddress(registry.address, registry.address, 'registry', parent, 3);
    await setContractAddress(registry.address, profile.deployed['NameRegistry.SetChildRecord'], 'set-child-record', registryName, 4);
    await setContractAddress(registry.address, profile.deployed['NameRegistry.UpdateRecord'], 'update-record', registryName, 4);
    await setContractAddress(registry.address, profile.deployed['NameRegistry.ClaimReverseRecord'], 'claim-reverse-record', registryName, 4);
    await setContractAddress(registry.address, profile.deployed['NameRegistry.UpdateReverseRecord'], 'update-reverse-record', registryName, 4);
    await setContractAddress(registry.address, profile.deployed['NameRegistry.CheckAddress'], 'check-address', registryName, 4);
});
