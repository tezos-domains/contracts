import { run } from '../utils';
import { currentProfile } from '../../common/profiles';
import { runOperation } from '../../common/tezos-toolkit';
import nameRegistry_setOwner from '../../common/lambdas/nameRegistry_setOwner';
import tldRegistrar_setOwner from '../../common/lambdas/tldRegistrar_setOwner';
import { Upgradeable } from '../../common/upgradeables';
import proxy_updateOwner from '../../common/lambdas/proxy_updateOwner';
import { nameRegistryUpgradeable } from '../../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../../common/contracts/tldRegistrar.upgradeable';

run(async tezos => {
    const profile = currentProfile();
    const newOwner = profile.deployed['admin_multisig'];

    async function updateProxies(upgradeable: Upgradeable, setOwnerFactory: (newOwner: string) => Promise<any>, tld?: string) {
        const tldSuffix = tld ? `:${tld}` : '';
        const name = upgradeable.name + tldSuffix;
        console.log(`Setting ${name} owner to ${newOwner}`);
        let contract = await tezos.contract.at(profile.deployed[name]);
        await runOperation(async () => await contract.methods.admin_update(await setOwnerFactory(newOwner)).send());

        for (let action of upgradeable.actions.filter(a => a.proxy)) {
            const name = `${upgradeable.name}.${action.name}${tldSuffix}`;
            const proxy = await tezos.contract.at(profile.deployed[name]);
            console.log(`Setting ${name} owner to ${newOwner}`);
            await runOperation(() => proxy.methods.proxy_admin_update(proxy_updateOwner(newOwner)).send());
        }
    }

    await updateProxies(nameRegistryUpgradeable, nameRegistry_setOwner);
    await updateProxies(tldRegistrarUpgradeable, tldRegistrar_setOwner, profile.tld);
});
