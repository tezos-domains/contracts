import * as fs from 'fs-extra';
import { OpKind } from '@taquito/rpc';

import { run, getLevel } from '../utils';
import nameRegistry_setRecord from '../../common/lambdas/nameRegistry_setRecord';
import tldRegistrar_setRecord from '../../common/lambdas/tldRegistrar_setRecord';
import { currentProfile } from '../../common/profiles';
import { runOperation } from '../../common/tezos-toolkit';
import { nameRegistry_setExpiry } from '../../common/lambdas/nameRegistry_setExpiry';
import nameRegistry_generateTzip12Ids from '../../common/lambdas/nameRegistry_generateTzip12Ids';

interface InputSunriseInfo {
    label: string;
    owner: string;
}

/**
 * Input file is required in evn variable `FILE` in following format
 *
 * ```
 * [
 *     {
 *         "label": "sunrise-domain-1",
 *         "owner": "<tz_address>"
 *     },
 *     {
 *         "label": "sunrise-domain-2",
 *         "owner": "<tz_address>"
 *     }
 * ]
 * ```
 *
 * `EXPIRY_DATE` env variable can be set to force expiry date for all domains (otherwise it's now + 1 year).
 */
run(async tezos => {
    const profile = currentProfile();
    const registry = await tezos.contract.at(profile.deployed['NameRegistry']);
    const tldRegistrar = await tezos.contract.at(profile.deployed[`TLDRegistrar:${profile.tld}`]);
    const inputFile = process.env.FILE;

    if (!inputFile) {
        throw new Error('Env variable file is required.');
    }

    console.log('Executing sunrise...');

    const expiryDate = process.env.EXPIRY_DATE;
    const expiry = expiryDate ? new Date(new Date(expiryDate).getTime()) : new Date(new Date().getTime() + 365 * 24 * 60 * 60 * 1000);

    console.log(`Expiry date for all domains is ${expiry.toISOString()}.`);

    const input = (await fs.readJSON(inputFile)) as InputSunriseInfo[];

    for (const info of input) {
        await createDomain(info.label, info.owner);
    }

    console.log('Sunrise domains created.');

    async function createDomain(label: string, owner: string): Promise<void> {
        const name = `${label}.${profile.tld}`;
        const level = getLevel(name);

        if (level != 2) {
            console.warn(`Skipping ${name}. Only registering 2nd level domains.`);
            return;
        }

        console.log(`Creating records for ${name} (owned by ${owner})`);

        const params = [
            registry.methods.admin_update(await nameRegistry_setRecord(name, owner, level, name)).toTransferParams(),
            registry.methods.admin_update(await nameRegistry_setExpiry(name, expiry)).toTransferParams(),
            tldRegistrar.methods.admin_update(await tldRegistrar_setRecord(label, expiry)).toTransferParams(),
            registry.methods.admin_update(await nameRegistry_generateTzip12Ids([name])).toTransferParams(),
        ];

        await runOperation(async () => await tezos.contract.batch(params.map(p => ({ kind: OpKind.TRANSACTION, ...p }))).send());
    }
});
