import * as fs from 'fs-extra';
import { buildCommonMetadata } from '../../common/contracts/common.metadata';

import createMetadata from '../../common/contracts/nameRegistry.metadata';
import { run } from '../utils';

run(async tezos => {
    const nameRegistryMetadata = await createMetadata();
    const tldRegistrarMetadata = buildCommonMetadata('TLDRegistrar');

    await fs.writeJSON('temp/name-registry.metadata.json', nameRegistryMetadata);
    await fs.writeJSON('temp/tld-registrar.metadata.json', tldRegistrarMetadata);
});
