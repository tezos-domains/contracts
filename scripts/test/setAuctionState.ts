import { run } from '../utils';
import tldRegistrar_setAuctionState from '../../common/lambdas/tldRegistrar_setAuctionState';
import { currentProfile } from '../../common/profiles';

run(async tezos => {
    let profile = currentProfile();

    let c = await tezos.contract.at(profile.deployed['TLDRegistrar:a1']);
    let op = await c.methods
        .admin_update(await tldRegistrar_setAuctionState('kekw', 2000000, 'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n', new Date('2021-03-10T16:00:00Z'), 1))
        .send();
    await op.confirmation();
});
