import nameRegistry_setRecord from '../../common/lambdas/nameRegistry_setRecord';
import { tldRegistrarUpgradeable } from '../../common/contracts/tldRegistrar.upgradeable';
import { currentProfile } from '../../common/profiles';
import { run } from '../utils';
import { runOperation } from '../../common/tezos-toolkit';

run(async tezos => {
    const profile = currentProfile();
    let addresses = {};
    let nameRegistry = await tezos.contract.at(profile.deployed['NameRegistry']);

    for (let i = 0; i <= 2; i++) {
        let tld = `a${i + 1}`;

        // launchDate = now, now + 1d, now + 2d, etc.
        let launchDate = new Date(Date.now() + i * 24 * 60 * 60 * 1000);

        // one day bidding periods followed by one day settlements with minimum bid of 1tez
        let auctionRegistrar = await tldRegistrarUpgradeable.deploy(
            {
                tld,
                minCommitmentAge: profile.minCommitmentAge,
                standardPricePerDay: 1_000_000_000_000,
                minDuration: 1,
                minAuctionPeriod: 24 * 60 * 60,
                bidAdditionalPeriod: 30 * 60,
                launchDates: [launchDate],
            },
            {},
            addresses,
            console.log
        );

        console.log(`Bootstrapping ${tld} top-level domain...`);
        await runOperation(async () => await nameRegistry.methods.admin_update(await nameRegistry_setRecord(tld, auctionRegistrar.contract.address, 1)).send());
    }

    console.log(`All successful.`);
    console.log(`Deployed addresses:`);
    console.log(JSON.stringify(addresses, null, 2));
});
