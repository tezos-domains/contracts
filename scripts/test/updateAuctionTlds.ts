import { run } from '../utils';
import tldRegistrar_updateConfig from '../../common/lambdas/tldRegistrar_updateConfig';
import { currentProfile } from '../../common/profiles';

// Helper script that updates the a1-3 auction domains for testing.
// The purpose is to run this once a day on testnets to keep the testing "auction cycle" going.

run(async tezos => {
    let profile = currentProfile();
    const addresses = [profile.deployed[`TLDRegistrar:a1`], profile.deployed[`TLDRegistrar:a2`], profile.deployed[`TLDRegistrar:a3`]];
    const secondsInDay = 24 * 60 * 60;
    const standardPrice = 1_000_000_000_000;
    let index = Math.floor(Date.now() / (secondsInDay * 1000)) % 3;

    console.log(`Updating a${index + 1}: ${addresses[index]}`);
    let c = await tezos.contract.at(addresses[index]);
    let op = await c.methods
        .admin_update(
            await tldRegistrar_updateConfig({
                tld: `a${index + 1}`,
                nameRegistryAddress: profile.deployed[`NameRegistry`],
                minDuration: 1,
                minAuctionPeriod: secondsInDay,
                bidAdditionalPeriod: 30 * 60,
                minBidIncreaseRatio: 10,
                launchDates: [new Date(), null, null, null],
                standardPrices: [standardPrice, null, null, null, standardPrice * 4, standardPrice * 2],
                maxCommitmentAge: secondsInDay,
                minCommitmentAge: 15,
            })
        )
        .send();
    await op.confirmation();
});
