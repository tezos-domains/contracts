import { Contract, MichelsonMap } from '@taquito/taquito';
import { run } from '../utils';
import { encodeString } from '../../common/convert';
import createCommitment from '../../common/createCommitment';
import nameRegistry_setRecord from '../../common/lambdas/nameRegistry_setRecord';
import { currentProfile } from '../../common/profiles';

async function buyDomain(buy: Contract, commit: Contract, label: string, owner: string): Promise<void> {
    console.log(`Buying ${label}`);

    // commit
    let commitment = await createCommitment(label, owner, 1234);
    var commitOp = await commit.methods.commit(commitment).send();
    await commitOp.confirmation(1);

    // buy
    var buyOp = await buy.methods.buy(encodeString(label), 365, owner, owner, new MichelsonMap(), 1234).send({ amount: 1 });
    await buyOp.confirmation(1);
}

run(async tezos => {
    let profile = currentProfile();
    let registry = await tezos.contract.at(profile.deployed['NameRegistry']);
    let buy = await tezos.contract.at(profile.deployed[`TLDRegistrar.Buy:${profile.tld}`]);
    let commit = await tezos.contract.at(profile.deployed[`TLDRegistrar.Commit:${profile.tld}`]);
    let setChildRecord = await tezos.contract.at(profile.deployed[`NameRegistry.SetChildRecord`]);
    let updateRecord = await tezos.contract.at(profile.deployed[`NameRegistry.UpdateRecord`]);

    let op = await registry.methods.admin_update(await nameRegistry_setRecord('test', profile.ownerAddress, 1)).send();
    await op.confirmation();

    await buyDomain(buy, commit, 'alice', profile.ownerAddress);

    await buyDomain(buy, commit, 'necroskillz', profile.ownerAddress);

    op = await setChildRecord.methods
        .set_child_record(
            encodeString('test'),
            encodeString(`necroskillz.${profile.tld}`),
            'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
            'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
            new MichelsonMap()
        )
        .send();
    await op.confirmation();

    for (let i = 1; i <= 80; i++) {
        console.log(`Setting ${i}`);

        let op = await setChildRecord.methods
            .set_child_record(
                encodeString(i.toString()),
                encodeString(`necroskillz.${profile.tld}`),
                'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
                'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
                new MichelsonMap()
            )
            .send();
        await op.confirmation();
    }

    op = await updateRecord.methods
        .update_record(
            encodeString(`necroskillz.${profile.tld}`),
            'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
            'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n',
            new MichelsonMap()
        )
        .send();
    await op.confirmation();
});
