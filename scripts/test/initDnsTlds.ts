import { run } from '../utils';
import nameRegistry_setRecord from '../../common/lambdas/nameRegistry_setRecord';
import { currentProfile } from '../../common/profiles';

run(async tezos => {
    let profile = currentProfile();
    let registry = await tezos.contract.at(profile.deployed['NameRegistry']);
    let op = await registry.methods.admin_update(await nameRegistry_setRecord('com', profile.deployed['OracleRegistrar'], 1)).send();
    await op.confirmation();
    op = await registry.methods.admin_update(await nameRegistry_setRecord('dev', profile.deployed['OracleRegistrar'], 1)).send();
    await op.confirmation();
});
