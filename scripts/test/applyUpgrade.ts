import { run } from '../utils';
import { currentProfile } from '../../common/profiles';
import { compileExpression } from '../../common/compile';
import { runOperation } from '../../common/tezos-toolkit';

run(async tezos => {
    let profile = currentProfile();

    let tldRegistrar = await tezos.contract.at(profile.deployed['TLDRegistrar:gra']);
    let nameRegistry = await tezos.contract.at(profile.deployed['NameRegistry']);

    let tldCode = await compileExpression('upgrades/mainnet/003/003_launch_three_and_four_letter.mligo', 'lambda_executed_in_registrar');
    let nrCode = await compileExpression('upgrades/mainnet/003/003_launch_three_and_four_letter.mligo', 'lambda_executed_in_name_registry');

    await runOperation(async () => await tldRegistrar.methods.admin_update(tldCode).send());
    await runOperation(async () => await nameRegistry.methods.admin_update(nrCode).send());
});
