## Prerequisites

-   [Node 14](https://nodejs.org/)
-   [Yarn](https://yarnpkg.com/)
-   [Docker](https://docs.docker.com/get-docker/)
-   [Python 3.8+](https://www.python.org/)

## Initial setup

1. `yarn install`
2. Copy `default.env` to `.env` and update the sign key (only needed for interactions with testnets/mainnet)

## Running tests

1. `yarn start-sandbox >/dev/null &`
2. (let Ganache spin up)
3. `yarn test`

## Deployment

After configuring `.env`, you can run the deploy script with:

`yarn deploy`

## Running admin scripts

`yarn run-script scripts/admin/initDomainsTez.ts` etc.
