#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

// Right-combed return type to token_metadata.
// see https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
type token_metadata_view_result =
[@layout:comb]
{
    token_id: nat;
    token_info: (string, bytes) map;
}

let main (param, store : nat * name_registry_main_storage) : token_metadata_view_result =
    let store = store.store in
    let name = find_tzip12_token (param, store) in
    let result : token_metadata_view_result = {
        token_id = param;
        token_info = Map.literal [
            ("name", name);
            ("decimals", 0x30); // the string "0" in UTF-8
            ("symbol", 0x444f4d41494e) // the string "DOMAIN" in UTF-8
        ]
    } in
    result