#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main (name, store : bytes * name_registry_main_storage) : resolved_domain option =
    resolve_name (name, store)
