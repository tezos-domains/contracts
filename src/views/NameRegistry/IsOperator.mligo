#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

// Right-combed parameter type to is_operator.
// see https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
type is_operator_view_param =
[@layout:comb]
{
    owner: address;
    operator: address;
    token_id: nat;
}

let main (param, store : is_operator_view_param * name_registry_main_storage) : bool =
    let name = find_tzip12_token(param.token_id, store.store) in

    match resolve_record(name, store.store) with
        None -> (failwith "RECORD_NOT_FOUND" : bool)
        | Some record -> record.owner = param.owner && is_tzip12_operator(param.operator, record)
