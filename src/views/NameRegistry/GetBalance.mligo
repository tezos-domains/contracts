#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

// Right-combed parameter type to get_balance.
// see https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
type balance_of_view_param =
[@layout:comb]
{
    owner: address;
    token_id: nat;
}

let main (param, store : balance_of_view_param * name_registry_main_storage) : nat =
    tzip12_get_balance (param.owner, param.token_id, store.store)