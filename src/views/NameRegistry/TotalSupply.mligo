#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main (param, store : nat * name_registry_main_storage) : nat =
    let store = store.store in
    let name = find_tzip12_token (param, store) in
    match Big_map.find_opt name store.records with
        | Some record ->
            // currently valid
            if is_valid_record (record.expiry_key, store) then 1n
            // has expired
            else 0n
        // no record exists
        | None -> 0n
