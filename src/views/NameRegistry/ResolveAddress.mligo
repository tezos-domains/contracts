#include "common.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main (addr, store : address * name_registry_main_storage) : resolved_domain option =
    let name_opt = (match Big_map.find_opt addr store.store.reverse_records with
        // found a reverse record
        | Some reverse_record -> reverse_record.name
        // no record found
        | None -> (None : bytes option)
    ) in
    match name_opt with
        // resolve fully
        | Some name -> resolve_name (name, store)
        // no name set
        | None -> (None : resolved_domain option)
