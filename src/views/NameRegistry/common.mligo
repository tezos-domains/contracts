#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/common.mligo"

// Right-combed return type of resolve-address and resolve-name.
type resolved_domain = [@layout:comb] {
    // The name of the resolved domain
    name: bytes;

    // The address of the resolved domain, if any
    address: address option;

    // A map of any additional data users wish to store with the domain
    data: data_map;

    // The expiration date of the domain, if any (the domain is only valid until this time)
    expiry: timestamp option;
}

// resolves a name and returns the full resolved domain information
let resolve_name (name, store : bytes * name_registry_main_storage) : resolved_domain option =
    match Big_map.find_opt name store.store.records with
        // a record exists
        | Some record ->
            let expiry_opt = get_expiry (record.expiry_key, store.store) in
            if is_valid_expiry expiry_opt then (Some {
                name = name;
                address = record.address;
                data = record.data;
                expiry = expiry_opt;
            } : resolved_domain option)
            else (None : resolved_domain option)
        // no record exists
        | None -> (None : resolved_domain option)
