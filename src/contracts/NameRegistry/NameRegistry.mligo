#include "./types.mligo"

type name_registry_parameter =
    Execute of action_param

    // tzip-12
    | Balance_of of tzip12_balance_of_param
    | Transfer of tzip12_transfer_param
    | Update_operators of tzip12_update_operator list

    // Administrative update.
    | Admin_update of name_registry_admin_update_param

// the 'admin_update' entrypoint
let admin_update (p, store : name_registry_admin_update_param * name_registry_main_storage) : name_registry_main_return =
    // if valid owner, execute
    if Tezos.sender = store.store.owner then p store

    // fail for everyone else
    else (failwith "NOT_AUTHORIZED" : name_registry_main_return)

[@inline]
let execute_action (action_name, payload, sender_, main_store : string * bytes * address * name_registry_main_storage) =
    match Big_map.find_opt action_name main_store.actions with
        Some action ->
            let ops, new_store = action ((payload, sender_), main_store.store) in
            (ops, { main_store with store = new_store })
        | None -> (failwith "UNKNOWN_ACTION" : name_registry_main_return)

let main (action, main_store : name_registry_parameter * name_registry_main_storage) : name_registry_main_return =
    match action with
        | Execute p ->
            let assert_trusted_sender = require_trusted_sender main_store.trusted_senders in
            execute_action (p.action_name, p.payload, p.original_sender, main_store)
        | Balance_of p -> (
            let param : tzip12_balance_of_param_internal = {
                requests = p.requests;
                callback = Tezos.address p.callback;
            } in
            execute_action ("BalanceOf", Bytes.pack param, Tezos.sender, main_store)
        )
        | Transfer p -> execute_action ("Transfer", Bytes.pack p, Tezos.sender, main_store)
        | Update_operators p -> execute_action ("UpdateOperators", Bytes.pack p, Tezos.sender, main_store)
        | Admin_update p -> admin_update (p, main_store)
    
