#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

type name_registry_main_return = operation list * name_registry_main_storage

type name_registry_admin_update_param = name_registry_main_storage -> name_registry_main_return
