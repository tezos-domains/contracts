#include "../../../shared/proxy.mligo"
#include "../NameRegistry.mligo"

type proxy_parameter =
    Check_address of check_address_param
    | Proxy_admin_update of proxy_admin_update_param

// Checks that there is a domain record with the specified name and the address.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Check_address p -> action_call ("CheckAddress", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
