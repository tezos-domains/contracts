#include "../../../shared/proxy.mligo"
#include "../NameRegistry.mligo"

type proxy_parameter =
    Update_record of update_record_param
    | Proxy_admin_update of proxy_admin_update_param

// Updates an existing domain record. The current sender must be its owner.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =
    match action with
        Update_record p -> action_call ("UpdateRecord", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
