#include "../../../shared/proxy.mligo"
#include "../NameRegistry.mligo"

type proxy_parameter =
    Claim_reverse_record of claim_reverse_record_param
    | Proxy_admin_update of proxy_admin_update_param

// Claims a reverse record corresponding to a domain (a forward record). The claimed reverse record will map the sender's address to the specified domain name.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Claim_reverse_record p -> action_call ("ClaimReverseRecord", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
