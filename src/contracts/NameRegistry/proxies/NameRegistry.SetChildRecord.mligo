#include "../../../shared/proxy.mligo"
#include "../NameRegistry.mligo"

type proxy_parameter =
    Set_child_record of set_child_record_param
    | Proxy_admin_update of proxy_admin_update_param

// Creates or overwrites an existing domain record. The current sender must be the owner of the parent record.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Set_child_record p -> action_call ("SetChildRecord", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
