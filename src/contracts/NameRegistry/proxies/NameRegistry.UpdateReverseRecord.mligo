#include "../../../shared/proxy.mligo"
#include "../NameRegistry.mligo"

type proxy_parameter =
    Update_reverse_record of update_reverse_record_param
    | Proxy_admin_update of proxy_admin_update_param

// Updates an existing reverse record. The current sender must be its owner. There must be a corresponding domain record.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =
    match action with
        Update_reverse_record p -> action_call ("UpdateReverseRecord", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
