#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Settle of settle_param
    | Proxy_admin_update of proxy_admin_update_param

// Settles an auction that has been won by the sender account.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Settle p -> action_call ("Settle", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
