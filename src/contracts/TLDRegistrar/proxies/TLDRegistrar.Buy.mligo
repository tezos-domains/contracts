#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Buy of buy_param
    | Proxy_admin_update of proxy_admin_update_param

// Buys a second-level domain based on previous commitment (see TLDRegistrar.Commit).
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Buy p -> action_call ("Buy", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
