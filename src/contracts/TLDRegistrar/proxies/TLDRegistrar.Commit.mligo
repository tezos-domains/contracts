#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Commit of commit_param
    | Proxy_admin_update of proxy_admin_update_param

// Creates a commitment to buy a second-level domain without disclosing the actual name. This is implemented according to the Commitment scheme.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Commit p -> action_call ("Commit", p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
