#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Withdraw of address
    | Proxy_admin_update of proxy_admin_update_param

// Withdraws all balance to a given account
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Withdraw p -> action_call ("Withdraw", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
