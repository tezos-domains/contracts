#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Renew of renew_param
    | Proxy_admin_update of proxy_admin_update_param

// Renews second-level domain for requested duration.
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Renew p -> action_call ("Renew", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
