#include "../../../shared/proxy.mligo"
#include "../TLDRegistrar.mligo"

type proxy_parameter =
    Bid of bid_param
    | Proxy_admin_update of proxy_admin_update_param

// Bids on a second-level domain
let main (action, store : proxy_parameter * proxy_storage) : proxy_return =    
    match action with
        Bid p -> action_call ("Bid", Bytes.pack p, store.contract), store
        | Proxy_admin_update p -> proxy_admin_update (p, store)
