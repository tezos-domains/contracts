#include "./types.mligo"

type tld_registrar_parameter =
    Execute of action_param

    // Administrative update.
    | Admin_update of tld_registrar_admin_update_param


// the 'admin_update' entrypoint
let admin_update (p, store : tld_registrar_admin_update_param * tld_registrar_main_storage) : tld_registrar_main_return =
    // if valid owner, execute
    if Tezos.sender = store.store.owner then p store

    // fail for everyone else
    else (failwith "NOT_AUTHORIZED" : tld_registrar_main_return)

let main (action, main_store : tld_registrar_parameter * tld_registrar_main_storage) : tld_registrar_main_return =
    match action with
        Execute p ->
            (let assert_trusted_sender = require_trusted_sender main_store.trusted_senders in
            match Big_map.find_opt p.action_name main_store.actions with
                Some action ->
                    let lambda_param : action_lambda_param = {
                        payload = p.payload;
                        original_sender = p.original_sender;
                    } in
                    let ops, new_store = action (lambda_param, main_store.store) in
                    (ops, { main_store with store = new_store })
                | None -> (failwith "UNKNOWN_ACTION" : tld_registrar_main_return))
        | Admin_update p -> admin_update (p, main_store)
    
