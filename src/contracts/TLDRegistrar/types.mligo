#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

type tld_registrar_main_storage = {
    store: tld_registrar_storage;
    actions: tld_registrar_action_map;
    trusted_senders: address set
}

type tld_registrar_main_return = operation list * tld_registrar_main_storage

// Admin lambda for changing tld_registrar_storage and invoking transactions.
type tld_registrar_admin_update_param = tld_registrar_main_storage -> tld_registrar_main_return
