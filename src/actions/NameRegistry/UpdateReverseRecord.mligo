#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main_updatereverserecord (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : update_reverse_record_param = (match (Bytes.unpack payload : update_reverse_record_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:update_reverse_record_param" : update_reverse_record_param)) in

    let assert_zero_amount = require_zero_amount () in

    match Big_map.find_opt p.address store.reverse_records with
        None -> (failwith "RECORD_NOT_FOUND" : name_registry_return) // no such record
        | Some existing_record ->
            // fail if not authorized
            if existing_record.owner <> original_sender then (failwith "NOT_AUTHORIZED" : name_registry_return)
            else
                // check the corresponding forward record exists
                let assert_name = match p.name with
                    Some name -> check_address (name, p.address, store)
                    | None -> () in

                // update the value
                let updated_record = {
                    existing_record with
                    name = p.name;
                    owner = p.owner;
                } in

                // store the reverse record
                ([] : operation list), { store with reverse_records = Big_map.update p.address (Some updated_record) store.reverse_records }

