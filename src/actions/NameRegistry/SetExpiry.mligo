#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main_setexpiry (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : set_expiry_param = (match (Bytes.unpack payload : set_expiry_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:set_expiry_param" : set_expiry_param)) in

    let assert_zero_amount = require_zero_amount () in

    // check parent ownership
    let parent = require_parent_ownership (p.parent, store, original_sender) in

    // check that this is a second-level domain
    let assert_parent_tld : unit = if parent.level <> 1n then (failwith "PARENT_NOT_TLD" : unit) else () in

    // name = label + '.' + parent
    let name = p.label ^ 0x2e ^ p.parent in

    // update expiry with the new value
    ([] : operation list), { store with expiry_map = Big_map.update name p.expiry store.expiry_map }
