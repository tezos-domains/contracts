#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let tzip12_balance_of (param, store : tzip12_balance_of_param_internal * name_registry_storage) : operation list =
    let get_response (request : tzip12_balance_of_request) : tzip12_balance_of_response =
        {
            request = request;
            balance = tzip12_get_balance (request.owner, request.token_id, store);
        } in
    let responses : tzip12_balance_of_response list = List.map get_response param.requests in
    let callback_contract = (match (Tezos.get_contract_opt param.callback : ((tzip12_balance_of_response list) contract) option) with
        | Some contract -> contract
        | None -> (failwith "INVALID_CONTRACT" : (tzip12_balance_of_response list) contract)
    ) in
    [Tezos.transaction responses 0tez callback_contract]

let main_balanceof (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : tzip12_balance_of_param_internal = (match (Bytes.unpack payload : tzip12_balance_of_param_internal option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:balance_of" : tzip12_balance_of_param_internal)) in

    let assert_zero_amount = require_zero_amount () in

    tzip12_balance_of (p, store), store
