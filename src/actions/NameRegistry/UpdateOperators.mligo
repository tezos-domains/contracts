#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

type tzip12_operator_action = Add | Remove

[@inline]
let update_tzip12_operator (param, action, original_sender, store: tzip12_operator_param * tzip12_operator_action * address * name_registry_storage): name_registry_storage = (
    let name = find_tzip12_token(param.token_id, store) in

    match resolve_record(name, store) with
        None -> (failwith "RECORD_NOT_FOUND" : name_registry_storage)
        | Some record -> 
            let assert_owner = if original_sender <> record.owner then (failwith "FA2_NOT_OWNER" : unit) else () in
            let assert_owner_match = if param.owner <> record.owner then (failwith "OWNER_PARAMETER_MISMATCH" : unit) else () in
            let operators = get_tzip12_operators(record) in

            let new_operators = match action with
                Add -> Set.add param.operator operators
                | Remove -> Set.remove param.operator operators in
            let new_record = { record with internal_data = Map.update tzip12_operators_key (Some (Bytes.pack new_operators)) record.internal_data } in
            
            { store with records = Big_map.update name (Some new_record) store.records }
)

let update_operators (param, original_sender, store : (tzip12_update_operator_param * address * name_registry_storage)): name_registry_storage = (
    let update_operator (store, update : name_registry_storage * tzip12_update_operator) : name_registry_storage = (
        match update with
            Add_operator p -> update_tzip12_operator(p, Add, original_sender, store)
            | Remove_operator p -> update_tzip12_operator(p, Remove, original_sender, store)
    ) in

    List.fold_left update_operator store param
)

let main_updateoperators (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : tzip12_update_operator_param = (match (Bytes.unpack payload : tzip12_update_operator_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:update_operators" : tzip12_update_operator_param)) in

    let assert_zero_amount = require_zero_amount () in

    (([] : operation list), update_operators (p, original_sender, store))
