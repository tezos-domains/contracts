#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main_checkaddress (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : check_address_param = (match (Bytes.unpack payload : check_address_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:check_address_param" : check_address_param)) in

    let assert_zero_amount = require_zero_amount () in
    let assert_address = check_address (p.name, p.address, store) in
    ([] : operation list), store
