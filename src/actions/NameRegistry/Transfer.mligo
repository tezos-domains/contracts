#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let update_owner (name, new_owner, from, original_sender, store : bytes * address * address * address * name_registry_storage) : name_registry_storage =
    match Big_map.find_opt name store.records with
        // no such record (this should never happen)
        None -> (failwith "RECORD_NOT_FOUND" : name_registry_storage)
        // record exists
        | Some existing ->
            let has_permissions = from = original_sender || is_tzip12_operator(original_sender, existing) in
            let assert_permissions = if not has_permissions then (failwith "FA2_NOT_OPERATOR" : unit) else () in
            // not being the owner is interpreted as having zero balance
            if existing.owner <> from || not is_valid_record (existing.expiry_key, store) then (failwith "FA2_INSUFFICIENT_BALANCE" : name_registry_storage)
            else
                // update the value
                let value = { existing with
                    owner = new_owner;
                    internal_data = Map.remove tzip12_operators_key existing.internal_data;
                } in
                { store with records = Big_map.update name (Some value) store.records }

let tzip12_transfer (param, original_sender, store : tzip12_transfer_param * address * name_registry_storage) : name_registry_storage =
    let transfer_item (store, transfer : name_registry_storage * tzip12_transfer) : name_registry_storage = (
        let transfer_dest_item (store, destination : name_registry_storage * tzip12_transfer_destination) : name_registry_storage = (
            if destination.amount = 0n then store else
            if destination.amount = 1n then update_owner (find_tzip12_token (destination.token_id, store), destination.to_, transfer.from_, original_sender, store) else
            (failwith "FA2_INSUFFICIENT_BALANCE" : name_registry_storage)
        ) in
        List.fold transfer_dest_item transfer.txs store
    ) in
    List.fold transfer_item param store

let main_transfer (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : tzip12_transfer_param = (match (Bytes.unpack payload : tzip12_transfer_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:transfer" : tzip12_transfer_param)) in

    let assert_zero_amount = require_zero_amount () in

    (([] : operation list), tzip12_transfer (p, original_sender, store))
