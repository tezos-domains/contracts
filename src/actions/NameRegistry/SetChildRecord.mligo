#include "../../shared/global.mligo"
#include "../../shared/validate_label.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let children_are_tzip12_tokens_key = "tzip12_children"

// only direct children are TZIP-12 tokens (this value is not passed to children)
let children_are_tzip12_tokens_direct = 0x00

let create_tzip12_token_id (parent, name, existing_record_opt, store : record * bytes * record option * name_registry_storage) : (nat option * name_registry_storage) =
    let children_are_tzip12_tokens = Map.find_opt children_are_tzip12_tokens_key parent.internal_data in
    if children_are_tzip12_tokens = Some children_are_tzip12_tokens_direct then
        (match existing_record_opt with
            | Some existing -> (existing.tzip12_token_id, store)
            | None -> (Some store.next_tzip12_token_id, { store with
                next_tzip12_token_id = store.next_tzip12_token_id + 1n;
                tzip12_tokens = Big_map.update store.next_tzip12_token_id (Some name) store.tzip12_tokens
            }
        ))
    else ((None : nat option), store)

let main_setchildrecord (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : set_child_record_param = (match (Bytes.unpack payload : set_child_record_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:set_child_record_param" : set_child_record_param)) in

    let assert_zero_amount = require_zero_amount () in
    
    // validate label
    let assert_valid_label = validate_label p.label in

    // name = label + '.' + parent
    let name = p.label ^ 0x2e ^ p.parent in

    // validate max name length
    let assert_valid_name = if (Bytes.length name) > 400n then (failwith "NAME_TOO_LONG" : unit) else () in

    // check parent ownership
    let parent = require_parent_ownership (p.parent, store, original_sender) in

    // fetch an existing record
    let existing_opt = Big_map.find_opt name store.records in

    // expiry key is the parent's expiry key, or the name itself for second-level domains
    let expiry_key_opt = if parent.level = 1n then Some name else parent.expiry_key in

    // create and store a new TZIP-12 token_id if needed
    let tzip12_token_id, store = create_tzip12_token_id (parent, name, existing_opt, store) in

    // new record
    let new_record : record = {
        address = p.address;
        owner = p.owner;
        data = p.data;
        level = parent.level + 1n;
        expiry_key = expiry_key_opt;
        internal_data = (Map.empty : data_map);
        tzip12_token_id = tzip12_token_id;
    } in

    // update reverse record
    let store = sync_reverse_record (name, existing_opt, p.address, store) in

    // update the expiry map (only for second-level domains)
    let store = if parent.level = 1n then
        // second-level domain
        (match expiry_key_opt with
            // expiry key exists, there is something to update
            Some expiry_key -> { store with expiry_map = Big_map.update expiry_key p.expiry store.expiry_map }
            // no expiry key, nothing to update
            | None -> store)
        // not a second-level domain
        else store in

    (([] : operation list), { store with records = Big_map.update name (Some new_record) store.records })
