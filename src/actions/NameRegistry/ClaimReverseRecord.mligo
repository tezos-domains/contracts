#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main_claimreverserecord (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : claim_reverse_record_param = (match (Bytes.unpack payload : claim_reverse_record_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:claim_reverse_record_param" : claim_reverse_record_param)) in

    let assert_zero_amount = require_zero_amount () in

    // check the corresponding forward record exists
    let assert_name = match p.name with
        Some name -> check_address (name, original_sender, store)
        | None -> () in

    let new_record : reverse_record = {
        name = p.name;
        owner = p.owner;
        internal_data = (Map.empty : (string, data_value) map);
    } in
    
    // store the reverse record
    ([] : operation list), { store with reverse_records = Big_map.update original_sender (Some new_record) store.reverse_records }
