#include "../../shared/global.mligo"
#include "../../shared/NameRegistry/params.mligo"
#include "../../shared/NameRegistry/common.mligo"

let main_updaterecord (param, store : (bytes * address) * name_registry_storage) : name_registry_return =
    let (payload, original_sender) = param in
    let p : update_record_param = (match (Bytes.unpack payload : update_record_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:update_record_param" : update_record_param)) in

    let assert_zero_amount = require_zero_amount () in

    match resolve_record (p.name, store) with
        // no such record
        None -> (failwith "RECORD_NOT_FOUND" : name_registry_return)
        // record exists
        | Some existing ->
            // fail if not authorized
            if existing.owner <> original_sender && not is_tzip12_operator(original_sender, existing) then (failwith "NOT_AUTHORIZED" : name_registry_return)
            else
                // clear tzip12 operators if owner changes
                let new_internal_data = if existing.owner <> p.owner then Map.remove tzip12_operators_key existing.internal_data else existing.internal_data in
                // update the value
                let value = { existing with
                    address = p.address;
                    owner = p.owner;
                    data = p.data;
                    internal_data = new_internal_data
                } in

                // sync the reverse record if needed
                let store = sync_reverse_record (p.name, Some existing, p.address, store) in

                // store the forward record
                ([] : operation list), { store with records = Big_map.update p.name (Some value) store.records }
