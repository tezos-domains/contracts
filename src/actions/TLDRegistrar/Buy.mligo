#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

// spends given commitment or fails if invalid
let spend_commitment (commitment, store : bytes * tld_registrar_storage) : tld_registrar_storage =
    let now = Tezos.now in

    match Big_map.find_opt commitment store.commitments with
        // commitment has to exist
        Some age ->
            // commitment has to be no older than max_commitment_age
            if age + int(get_max_commitment_age (store)) >= now then
                // commitment has to be no younger than min_commitment_age
                if age + int(get_min_commitment_age (store)) <= now
                    // consume the commitment
                    then {store with commitments = Big_map.remove commitment store.commitments}
                else (failwith "COMMITMENT_TOO_RECENT" : tld_registrar_storage)
            else (failwith "COMMITMENT_TOO_OLD" : tld_registrar_storage)
        | None -> (failwith "COMMITMENT_NOT_FOUND" : tld_registrar_storage)

// checks that given domain is available for FIFS registration or fails
let require_buyable (label, store : bytes * tld_registrar_storage) : unit =
    let info = require_available (label, store) in
    match info.state with
        Some a -> (failwith "LABEL_IN_AUCTION" : unit)
        | None -> if info.ends_at > Tezos.now then (failwith "LABEL_IN_AUCTION" : unit) else ()

let main_buy (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let p : buy_param = (match (Bytes.unpack param.payload : buy_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:buy_param" : buy_param)) in

    // generate expected commitment and consume it
    let commitment = Crypto.sha512 (Bytes.pack (p.label, p.owner, p.nonce)) in
    let store = spend_commitment (commitment, store) in

    // check availability
    let assert_available = require_buyable (p.label, store) in

    // check the amount equals to actual price
    let correct_amount = require_correct_amount (p.label, p.duration, param.original_sender, store) in

    let expiry = Tezos.now + int(p.duration * seconds_per_day) in

    set_record_and_store_proceeds (p.label, expiry, p.address, p.owner, p.data, correct_amount, store)
