#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

let main_withdraw (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let assert_zero_amount = require_zero_amount () in

    let recipient : address = (match (Bytes.unpack param.payload : address option) with
        Some a -> a
        | None -> (failwith "INVALID_PAYLOAD:address" : address)) in
    
    // get the balance
    let current_balance = get_bidder_balance (param.original_sender, store.bidder_balances) in

    if current_balance > 0tez then (
        // transaction withdrawing the full amount
        let transaction = match (Tezos.get_contract_opt recipient : unit contract option) with
            None -> (failwith "INVALID_RECIPIENT" : operation)
            | Some contract -> Tezos.transaction () current_balance contract in
        // remove the balance
        let store = {store with bidder_balances = Big_map.remove param.original_sender store.bidder_balances} in
        
        [transaction], store
    )
    // do nothing when no balance
    else ([] : operation list), store
