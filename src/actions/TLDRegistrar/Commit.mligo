#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

let main_commit (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let commitment = param.payload in
    let assert_zero_amount = require_zero_amount () in

    let assert_commitment_does_not_exist = (match Big_map.find_opt commitment store.commitments with
        | Some existing -> (failwith "COMMITMENT_EXISTS" : unit)
        | None -> ()) in

    // store the given commitment and the current timestamp
    ([] : operation list), {store with commitments = Big_map.update commitment (Some Tezos.now) store.commitments}
