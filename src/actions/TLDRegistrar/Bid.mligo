#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/validate_label.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

// Requires that a label can be bid on (fails otherwise). Returns the auction information for the given label.
let require_biddable (label, store : bytes * tld_registrar_storage) : auction_info =
    let info = require_available (label, store) in

    // fail if the bidding window is over
    if info.ends_at <= Tezos.now then (failwith "AUCTION_ENDED" : auction_info) else info

// Rounds up an amount to the closest tenth of a tez
let round_to_nearest_tenth (a : tez) : tez =
    (a + 50_000mutez) / 100_000n * 100_000n

// Requires that a new highest bid passes the requirements or fails. Returns the new balance
let require_correct_bid_amount (auction_state_opt, label, bid, duration, current_balance, store: auction_state option * bytes * tez * nat * tez * tld_registrar_storage) : tez =
    // minimum_bid = round_to_nearest_tenth(last_bid * (1 + get_min_bid_increase_ratio / 100))
    // (in real numbers arithmetic; unit is tez)
    let current_minimum_bid = match auction_state_opt with
        Some auction -> round_to_nearest_tenth (auction.last_bid * (100n + get_min_bid_increase_ratio store) / 100n)
        | None -> domain_price (duration, get_standard_price_per_day (Bytes.length label, store)) in
    
    // check minimum bid
    let assert_min_bid = if bid < current_minimum_bid then (failwith "BID_TOO_LOW" : unit) else () in

    // consider both the sent amount and the sender's balance
    let amount_available = Tezos.amount + current_balance in

    // check that the amount is enough
    Option.unopt_with_error (amount_available - bid) "AMOUNT_TOO_LOW"

[@inline]
let update_balance (bidder_balances, addr, new_balance: (address, tez) big_map * address * tez) : (address, tez) big_map =
    let new_value = if new_balance <> 0tez then Some new_balance else (None : tez option) in
    Big_map.update addr new_value bidder_balances

let main_bid (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let p : bid_param = (match (Bytes.unpack param.payload : bid_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:bid_param" : bid_param)) in

    // validate label
    let assert_valid_label = validate_label p.label in

    // check availability
    let info = require_biddable (p.label, store) in

    // ownership_period is set to min_duration if no bids yet
    let ownership_period = match info.state with
        Some auction -> auction.ownership_period
        | None -> get_min_duration store in

    // check the amount equals to actual price
    let current_balance = get_bidder_balance (param.original_sender, store.bidder_balances) in
    let new_balance = require_correct_bid_amount (info.state, p.label, p.bid, ownership_period, current_balance, store) in

    // update sender's balance if needed
    let bidder_balances = if new_balance <> current_balance
        then update_balance (store.bidder_balances, param.original_sender, new_balance)
        else store.bidder_balances in

    // credit previous bid back to the bidder
    let bidder_balances = match info.state with
        // there is a previous bidder
        Some auction -> (
            let last_bidder_balance = get_bidder_balance (auction.last_bidder, bidder_balances) in
            update_balance (bidder_balances, auction.last_bidder, last_bidder_balance + auction.last_bid)
        )
        // no previous bidder
        | None -> bidder_balances in

    // new_auction_end = max(current_auction_end, now + get_bid_additional_period)
    let prolonged_period = Tezos.now + int (get_bid_additional_period store) in
    let new_auction_end = if prolonged_period > info.ends_at then prolonged_period else info.ends_at in

    let new_auction_state : auction_state = {
        last_bid = p.bid;
        last_bidder = param.original_sender;
        ends_at = new_auction_end;
        ownership_period = ownership_period
    } in

    let store = { store with
        auctions = Big_map.update p.label (Some new_auction_state) store.auctions;
        bidder_balances = bidder_balances
    } in

    ([] : operation list), store
