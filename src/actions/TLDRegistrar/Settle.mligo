#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

// Requires that a label auction is settleable. Returns the auction state.
let require_settleable (label, store : bytes * tld_registrar_storage) : auction_state =
    let info = require_available (label, store) in
    match info.state with
        // an auction with bids exists
        Some auction ->
            let now = Tezos.now in
            // check that we are in the settlement window
            if auction.ends_at <= now && now < auction.ends_at + int (auction.ownership_period * seconds_per_day) then auction
            // not in the settlement window
            else (failwith "NOT_SETTLEABLE" : auction_state)
        // no auction with bids
        | None -> (failwith "NOT_SETTLEABLE" : auction_state)

let main_settle (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let assert_zero_amount = require_zero_amount () in

    let p : settle_param = (match (Bytes.unpack param.payload : settle_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:settle_param" : settle_param)) in

    // check settleability
    let auction_state = require_settleable (p.label, store) in

    // check the sender's identity
    let assert_bidder = if auction_state.last_bidder <> param.original_sender then (failwith "NOT_AUTHORIZED" : unit) else () in

    // finalize by removing the auction
    let store = {store with auctions = Big_map.remove p.label store.auctions } in

    // expires "ownership_period" days after the auction ended
    let expiry = auction_state.ends_at + int(auction_state.ownership_period * seconds_per_day) in

    set_record_and_store_proceeds (p.label, expiry, p.address, p.owner, p.data, auction_state.last_bid, store)
