#include "../../shared/action.mligo"
#include "../../shared/global.mligo"
#include "../../shared/TLDRegistrar/params.mligo"
#include "../../shared/TLDRegistrar/common.mligo"

// require that the given label is currently valid (exists and not expired)
let require_valid (label, store : bytes * tld_registrar_storage) : tld_record =
    match Big_map.find_opt label store.records with
        // tld_record exists
        Some tld_record ->
            // valid tld_record exists
            if Tezos.now < tld_record.expiry then tld_record
            // tld_record is expired
            else (failwith "LABEL_EXPIRED" : tld_record)
        // tld_record doesn't exist
        | None -> (failwith "LABEL_NOT_FOUND" : tld_record)

let main_renew (param, store : action_lambda_param * tld_registrar_storage) : tld_registrar_return =
    let p : renew_param = (match (Bytes.unpack param.payload : renew_param option) with
        Some p -> p
        | None -> (failwith "INVALID_PAYLOAD:renew_param" : renew_param)) in

    // require a tld_record exists and is not expired
    let tld_record = require_valid (p.label, store) in

    // check the amount equals to actual price
    let correct_amount = require_correct_amount (p.label, p.duration, param.original_sender, store) in

    // generate a new tld_record with a new expiration date
    let expiry = tld_record.expiry + int(p.duration * 24n * 60n * 60n) in
    let new_record = { tld_record with expiry = expiry } in

    // send SetExpiry to NameRegistry
    let expiry_param : set_expiry_param = {
        label = p.label;
        parent = store.tld;
        expiry = Some expiry
    } in
    let set_expiry_param_bytes = Bytes.pack expiry_param in
    let execute_param : action_param = {
        action_name = "SetExpiry";
        payload = set_expiry_param_bytes;
        original_sender = Tezos.self_address;
    } in
    let set_expiry_transaction = match (Tezos.get_entrypoint_opt "%execute" store.name_registry : action_param contract option) with
        None -> (failwith "INVALID_NAME_REGISTRY_CONTRACT" : operation)
        | Some entrypoint -> Tezos.transaction execute_param 0mutez entrypoint in

    // transaction that redirects the proceeds of this renewal to the treasury
    let send_proceeds = Tezos.transaction () correct_amount (get_treasury store) in

    // update store with the new tld_record
    let store = {store with records = Big_map.update p.label (Some new_record) store.records} in
    
    [set_expiry_transaction; send_proceeds], store
