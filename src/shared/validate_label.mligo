// Recursively validate a substring of a given label starting at 'startIndex' and 'length' long
let rec validate_label_substring (label, startIndex, length : bytes * nat * nat) : unit =
    if startIndex < length then
        // slice one byte
        let c = Bytes.sub startIndex 1n label in

        // check [a-z0-9-] with no hyphens at the start or the end
        if ((c >= 0x30 && c <= 0x39) // 0-9
            || (c = 0x2d && startIndex + 1n < length && startIndex > 0n) // hyphen (not allowed at the start or the end)
            || (c >= 0x61 && c <= 0x7a)) // a-z
        then
            // validate the rest
            validate_label_substring (label, startIndex + 1n, length)
        // not valid
        else (failwith "INVALID_LABEL" : unit)
    else ()

// Validates given label
let validate_label (label : bytes) : unit =
    let length : nat = Bytes.length label in
    
    // check max length
    let assert_max_length : unit = if length > 100n then (failwith "LABEL_TOO_LONG" : unit) else () in

    // check min length
    let assert_min_length : unit = if length = 0n then (failwith "LABEL_EMPTY" : unit) else () in
    
    validate_label_substring (label, 0n, length)

