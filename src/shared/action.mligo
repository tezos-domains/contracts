// Right-combed parameter type to admin_update entrypoints.
type action_param = [@layout:comb] {
    action_name: string;
    payload: bytes;
    original_sender: address
}

type action_lambda_param = {
    payload: bytes;
    original_sender: address;
}
