#include "action.mligo"
#include "global.mligo"

// Generic proxy storage
type proxy_storage = {
    // The contract the proxy is wrapping
    contract: address;

    // The owner of this contract allowed to make administrative updates
    owner: address;

    // TZIP-16 metadata
    metadata: tzip16_metadata;
}

type proxy_return = operation list * proxy_storage

// Admin lambda for changing storage and invoking transactions.
type proxy_admin_update_param = proxy_storage -> proxy_return

// Generic entrypoint for updating the storage by an administrator
let proxy_admin_update (p, store : proxy_admin_update_param * proxy_storage) : proxy_return =
    // if valid owner, execute
    if Tezos.sender = store.owner then p store

    // fail for everyone else
    else (failwith "NOT_AUTHORIZED" : proxy_return)

// Generates failure when fetching a contract
let proxy_invalid_contract (_ : unit) : operation list =
    (failwith "INVALID_CONTRACT" : operation list)

// Creates a transaction calling an action on a given contract.
let action_call (action_name, payload, contract : string * bytes * address) : operation list =
    match (Tezos.get_entrypoint_opt "%execute" contract : action_param contract option) with
        None -> proxy_invalid_contract ()
        | Some c ->
            let param : action_param = {
                action_name = action_name;
                payload = payload;
                original_sender = Tezos.sender
            } in
            [Tezos.transaction param Tezos.amount c]
