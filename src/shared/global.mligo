// all possible types that can be stored in the data map of an entity
type data_value = bytes

// data map with all possible types that can be stored with an entity
type data_map = (string, data_value) map

// TZIP-16 metadata
type tzip16_metadata = (string, bytes) big_map

// Requires zero funds transferred (fails if the current transaction contains a non-zero tezos amount).
// Useful as a correctness check for most entrypoints that don't accept funds.
let require_zero_amount (_ : unit) : unit = 
    if Tezos.amount > 0tez then
        (failwith "AMOUNT_NOT_ZERO" : unit)
    else ()

// Requires that the current transaction sender is in the given set of trusted senders.
let require_trusted_sender (trusted_senders : address set) : unit =
    if Set.mem Tezos.sender trusted_senders then ()
    else (failwith "NOT_TRUSTED_SENDER" : unit)

let seconds_per_day = 86_400n // 24 * 60 * 60

[@inline]
let max_timestamp (a, b : timestamp * timestamp) : timestamp =
    if a > b then a else b
