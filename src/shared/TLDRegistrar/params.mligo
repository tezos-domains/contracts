#include "../global.mligo"

type commit_param = bytes

// Right-combed parameter type to Buy.
type buy_param = [@layout:comb] {
    // The UTF-8 encoded label of the second-level domain to buy.
    label: bytes;

    // Ownership duration represented in days.
    duration: nat;

    // The new owner of the given domain.
    owner: address;

    // The optional address the given domain resolves to.
    address: address option;

    // A map of any additional data clients wish to store with the given domain.
    data: data_map;

    // Chosen commitment nonce preventing dictionary attacks on published commitments
    nonce: nat;
}

// Right-combed parameter type to Renew.
type renew_param = [@layout:comb] {
    // The UTF-8 encoded label of the second-level domain to buy.
    label: bytes;

    // The renewal duration represented in days.
    duration: nat;
}

// Right-combed parameter type to Bid.
type bid_param = [@layout:comb] {
    // Label the auction bid relates to
    label: bytes;

    // The new highest bid
    bid: tez;
}

// Right-combed parameter type to Settle.
type settle_param = [@layout:comb] {
    // The UTF-8 encoded label of the second-level domain for which the auction was running.
    label: bytes;

    // The new owner of the given domain.
    owner: address;

    // The optional address the given domain resolves to.
    address: address option;

    // A map of any additional data clients wish to store with the given domain.
    data: data_map;
}
