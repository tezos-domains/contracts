#include "../NameRegistry/params.mligo"
#include "../action.mligo"

type tld_record = {
    // The expiry timestamp. Domain is considered expired starting with this second.
    expiry: timestamp;

    // A map of internal dynamic fields for extendability that contracts store with the domain.
    internal_data: data_map;
}

type auction_state = {
    // Last (= highest) bid of the auction
    last_bid: tez;

    // Last bidder of the auction (who placed the highest bid).
    last_bidder: address;

    // The timestamp of when the auction ends.
    ends_at: timestamp;

    // The ownership period. This includes whatever time the auction spends in the settlement period.
    // This can be different from configured min_duration if min_duration is changed during an active auction.
    // Storing the ownership period here will ensure that auction winners will acquire their domains with the expected expiry time.
    ownership_period: nat;
}

type tld_registrar_storage = {
    // The TLD this contract manages.
    tld: bytes;

    // Map of labels to domain records.
    records: (bytes, tld_record) big_map;

    // Map of commitments containing when each commitment has been added.
    commitments: (bytes, timestamp) big_map;

    // Map of labels and their auction states (auctions with at least one bid).
    auctions: (bytes, auction_state) big_map;

    // Balances of bidders that have been outbid on an auction.
    bidder_balances: (address, tez) big_map;

    // The owner of this contract allowed to make administrative updates.
    owner: address;

    // Map for numeric configurable fields used for calculations.
    config: (nat, nat) map;

    // The address of the NameRegistry contract.
    name_registry: address;

    // Dynamic data store for extendability.
    data: (bytes, data_value) big_map;

    // TZIP-16 metadata
    metadata: tzip16_metadata;
}

type tld_registrar_return = operation list * tld_registrar_storage

type tld_registrar_action = (action_lambda_param * tld_registrar_storage) -> tld_registrar_return

type tld_registrar_action_map = (string, tld_registrar_action) big_map

// Gets a value for the given key from the tld_registrar_storage config.
[@inline]
let get_config (key, tld_registrar_storage : nat * tld_registrar_storage) : nat option =
    Map.find_opt key tld_registrar_storage.config

// Requires a value for the given key from the tld_registrar_storage config.
[@inline]
let require_config (key, tld_registrar_storage : nat * tld_registrar_storage) : nat =
    match get_config (key, tld_registrar_storage) with
        Some v -> v
        | None -> (failwith "INTERNAL_CONFIG_MISSING" : nat)

// Requires a non-zero value for the given key from the storage config.
[@inline]
let require_nonzero_config (key, storage : nat * tld_registrar_storage) : nat =
    let value = require_config (key, storage) in
    if value > 0n then value else (failwith "INTERNAL_CONFIG_INVALID" : nat)

let max_commitment_age_key = 0n

// Ges the maximum allowed age of a commitment to be still spendable (in seconds).
[@inline]
let get_max_commitment_age (store : tld_registrar_storage) : nat =
    require_config (max_commitment_age_key, store)

let min_commitment_age_key = 1n

// Gets the minimum allowed age of a commitment to be spendable (in seconds).
[@inline]
let get_min_commitment_age (store : tld_registrar_storage) : nat =
    require_config (min_commitment_age_key, store)

let standard_price_per_day_key = 2n

// Gets the standard price (which is also the minimum starting bid) for a domain per one day.
// Represented in picotezos (10^-12 tez) to avoid rounding error when registering "usual" periods.
// Value is always higher than 0n.
[@inline]
let get_standard_price_per_day (length, store : nat * tld_registrar_storage) : nat =
    match get_config (standard_price_per_day_key * 1000n + length, store) with
        Some v -> v
        // use default
        | None -> require_nonzero_config (standard_price_per_day_key, store)

let min_duration_key = 3n

// Gets the Minimum duration that a domain can be registered or renewed for.
[@inline]
let get_min_duration (store : tld_registrar_storage) : nat =
    require_config (min_duration_key, store)

let min_bid_increase_ratio_key = 4n

// The minimum increase for a bid, represented as a percentage of the last bid (i.e. hundreths of the last bid).
// Value is always higher than 0n.
[@inline]
let get_min_bid_increase_ratio (store : tld_registrar_storage) : nat =
    require_nonzero_config (min_bid_increase_ratio_key, store)

let min_auction_period_key = 5n

// the minimum period of an auction (the value of 0n means that no auctions take place)
[@inline]
let get_min_auction_period (store : tld_registrar_storage) : nat =
    require_config (min_auction_period_key, store)

let bid_additional_period_key = 6n

// period of time added to the auction end when a bid is placed
// (the value of 0n means that auctions are never prolonged by bids)
[@inline]
let get_bid_additional_period (store : tld_registrar_storage) : nat =
    require_config (bid_additional_period_key, store)

let launch_date_key_start = 1000n

// the launch date of the TLD for the given domain length (or None if domains with this length are not availble)
[@inline]
let get_launch_date (length, store : nat * tld_registrar_storage) : timestamp option =
    let nat_timestamp = (match get_config (launch_date_key_start + length, store) with
        Some v -> v
        // use default
        | None -> require_config (launch_date_key_start, store)) in
    
    if nat_timestamp = 0n then (None : timestamp option)
    else Some ((0 : timestamp) + int nat_timestamp)

let treasury_address_key : bytes = 0xff00

// the contract that should receive all proceeds of the registrar
[@inline]
let get_treasury (store : tld_registrar_storage) : unit contract =
    let treasury_address_bytes = match (Big_map.find_opt treasury_address_key store.data : bytes option) with
        Some b -> b
        | None -> (failwith "INTERNAL_CONFIG_MISSING" : bytes) in
    let treasury_address = match (Bytes.unpack treasury_address_bytes : address option) with
        Some a -> a
        | None -> (failwith "INTERNAL_CONFIG_INVALID" : address) in
    (Tezos.get_contract_with_error treasury_address "INTERNAL_CONFIG_INVALID" : unit contract)

// return the domain price given a duration and a price-per-day
[@inline]
let domain_price (duration, price_per_day : nat * nat) : tez = 
    ((duration * price_per_day + 500_000n) / 1_000_000n) * 1mutez

// Calculates the price from the given duration.
// Fails if the transferred amount is lower and refunds if the transferred amount is higher.
[@inline]
let require_correct_amount (label, duration, original_sender, store : bytes * nat * address * tld_registrar_storage) : tez =
    // check minimum duration
    let assert_min_duration = if duration < get_min_duration (store) then (failwith "DURATION_TOO_LOW" : unit) else () in

    // calculate price and convert from picotezos
    let price = domain_price (duration, get_standard_price_per_day (Bytes.length label, store)) in

    // fail if amount is not equal to actual price
    if Tezos.amount < price then
        (failwith "AMOUNT_TOO_LOW" : tez)
    else if Tezos.amount > price then
        (failwith "AMOUNT_TOO_HIGH" : tez)
    else Tezos.amount

// Auction information for a given label (return type for require_available)
type auction_info = {
    // Returns the auction state of the last auction (None if case no bids have been placed)
    state: auction_state option;

    // Current (expected) end of the auction.
    ends_at: timestamp;
}

// Validates that a label is available for auction or buy and returns its auction information.
[@inline]
let require_available (label, store : bytes * tld_registrar_storage) : auction_info =
    // fail if not launched for this label
    let launch_date = (match get_launch_date (Bytes.length label, store) with
        Some d -> if Tezos.now >= d then d else (failwith "LABEL_NOT_AVAILABLE" : timestamp)
        | None -> (failwith "LABEL_NOT_AVAILABLE" : timestamp)) in

    let record_opt = Big_map.find_opt label store.records in

    // fail if taken
    let assert_available : unit = match record_opt with
        // if a domain exists, it has to be past expiration
        Some tld_record -> if Tezos.now < tld_record.expiry then (failwith "LABEL_TAKEN" : unit) else ()
        // if a domain hasn't been registered, it's automatically available
        | None -> () in

    let record_expiry = (match record_opt with
        Some tld_record -> tld_record.expiry
        | None -> launch_date) in
    let auction_state_opt = Big_map.find_opt label store.auctions in
    match auction_state_opt with
        Some auction ->
            let settlement_period = int (auction.ownership_period * seconds_per_day) in
            // not an auction with expired settlement -> standard ends_at
            if Tezos.now < auction.ends_at + settlement_period then { state = auction_state_opt; ends_at = auction.ends_at }
            // auction with expired settlement -> new auction
            else { state = (None : auction_state option); ends_at = max_timestamp (auction.ends_at + settlement_period, record_expiry) + int (get_min_auction_period store) }
        | None -> {
            state = auction_state_opt;
            ends_at = record_expiry + int (get_min_auction_period store)
        }
    
// Updates the balance of a given account (removes the balance entry if the new balance is 0tez)
let update_balance (bidder_balances, addr, new_balance: (address, tez) big_map * address * tez) : (address, tez) big_map =
    let new_value = if new_balance <> 0tez then Some new_balance else (None : tez option) in
    Big_map.update addr new_value bidder_balances

// Balance of a given account or 0tez in case of no balance
[@inline]
let get_bidder_balance (bidder_address, bidder_balances : address * (address, tez) big_map) : tez =
    match Big_map.find_opt bidder_address bidder_balances with
        Some found_balance -> found_balance
        | None -> 0tez

// Updates or creates a tld_record with given values. Sends proceeds to the configured treasury address.
let set_record_and_store_proceeds (label, expiry, addr, owner, data, proceeds, store : bytes * timestamp * address option * address * data_map * tez * tld_registrar_storage) : tld_registrar_return =
    // generate a new tld_record with a future expiration date
    let new_record : tld_record = {
        expiry = expiry;
        internal_data = (Map.empty : data_map);
    } in

    // send SetChildRecord to NameRegistry
    let child_record_param : set_child_record_param = {
        label = label;
        parent = store.tld;
        address = addr;
        owner = owner;
        data = data;
        expiry = Some expiry;
    } in
    let set_child_record_bytes = Bytes.pack child_record_param in
    let execute_param : action_param = {
        action_name = "SetChildRecord";
        payload = set_child_record_bytes;
        original_sender = Tezos.self_address;
    } in
    let set_child_record_transaction = match (Tezos.get_entrypoint_opt "%execute" store.name_registry : action_param contract option) with
        None -> (failwith "INVALID_NAME_REGISTRY_CONTRACT" : operation)
        | Some entrypoint -> Tezos.transaction execute_param 0mutez entrypoint in

    // transaction that redirects the proceeds of this buy/settle to the treasury
    let send_proceeds = Tezos.transaction () proceeds (get_treasury store) in
    
    // update store with the new tld_record
    let store = {store with records = Big_map.update label (Some new_record) store.records} in
    
    [set_child_record_transaction; send_proceeds], store
