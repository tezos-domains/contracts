#if !NAME_REGISTRY_PARAMS
#define NAME_REGISTRY_PARAMS

#include "../global.mligo"

// Right-combed parameter type to SetChildRecord.
type set_child_record_param = [@layout:comb] {
    // The UTF-8 encoded label.
    label: bytes;

    // The UTF-8 encoded parent domain.
    parent: bytes;

    // The optional address the record resolves to.
    address: address option;

    // The owner of the record allowed to make changes.
    owner: address;

    // A map of any additional data clients wish to store with the domain.
    data: data_map;

    // The expiry of this record. Only applicable to second-level domains as all higher-level domains share the expiry of their ancestor 2LD.
    expiry: timestamp option
}

// Right-combed parameter type to SetExpiry.
type set_expiry_param = [@layout:comb] {
    // The UTF-8 encoded label
    label: bytes;

    // The UTF-8 encoded parent
    parent: bytes;

    // The new expiry of the 2LD
    expiry: timestamp option
}

// Right-combed parameter type to ClaimReverseRecord.
type claim_reverse_record_param = [@layout:comb] {
    // The UTF-8 encoded name to claim.
    name: bytes option;

    // The owner of the record allowed to make changes.
    owner: address;
}

// Right-combed parameter type to UpdateReverseRecord.
type update_reverse_record_param = [@layout:comb] {
    // The address of the reverse record to update.
    address: address;

    // The new UTF-8 encoded name the record resolves to.
    name: bytes option;

    // The owner of the record allowed to make changes.
    owner: address;
}

// Right-combed parameter type to CheckAddress.
type check_address_param = [@layout:comb] {
    // The UTF-8 encoded name to check.
    name: bytes;

    // The expected address.
    address: address
}

// Right-combed parameter type to UpdateRecord.
type update_record_param = [@layout:comb] {
    // The UTF-8 encoded name of the domain to update.
    name: bytes;

    // The optional new address the record resolves to.
    address: address option;

    // The new owner of the record allowed to make changes.
    owner: address;

    // The new map of any additional data that clients wish to store with the domain.
    data: data_map;
}

// TZIP-12 implementation types
// see https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md

type tzip12_balance_of_request =
[@layout:comb]
{
    owner: address;
    token_id: nat;
}

type tzip12_balance_of_response =
[@layout:comb]
{
    request: tzip12_balance_of_request;
    balance: nat;
}

type tzip12_balance_of_param_internal =
[@layout:comb]
{
    requests: tzip12_balance_of_request list;
    callback: address;
}

type tzip12_balance_of_param =
[@layout:comb]
{
    requests : tzip12_balance_of_request list;
    callback : (tzip12_balance_of_response list) contract;
}

type tzip12_transfer_destination =
[@layout:comb]
{
  to_: address;
  token_id: nat;
  amount: nat;
}

type tzip12_transfer =
[@layout:comb]
{
  from_: address;
  txs: tzip12_transfer_destination list;
}

type tzip12_transfer_param = tzip12_transfer list

type tzip12_operator_param =
[@layout:comb]
{
  owner: address;
  operator: address;
  token_id: nat;
}

type tzip12_update_operator =
  [@layout:comb]
  | Add_operator of tzip12_operator_param
  | Remove_operator of tzip12_operator_param

type tzip12_update_operator_param = tzip12_update_operator list

#endif
