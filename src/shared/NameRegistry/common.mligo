// Forward record
type record = {
    // The optional address the record resolves to
    address: address option;

    // The owner of the record allowed to make changes
    owner: address;

    // A map of any additional data clients wish to store with the domain
    data: data_map;

    // A map of internal dynamic fields for extendability that contracts store with the domain.
    internal_data: data_map;

    // The computed level of this record
    level: nat;

    // Key to the expiry map containing the expiry of this record
    expiry_key: bytes option;

    // TZIP-12 token ID (only has a value for 2nd level domains)
    tzip12_token_id: nat option;
}

type reverse_record = {
    // UTF-8 encoded name
    name: bytes option;

    // The owner of the record allowed to make changes
    owner: address;

    // A map of internal dynamic fields for extendability that contracts store with the domain.
    internal_data: data_map;
}

type name_registry_storage = {
    // Map of UTF-8 encoded names to forward records
    records: (bytes, record) big_map;

    // Map of addresses to reverse records
    reverse_records: (address, reverse_record) big_map;

    // Map containing expiry for every second-level domain
    expiry_map: (bytes, timestamp) big_map;

    // The owner of this contract allowed to make administrative updates.
    owner: address;

    // Dynamic data store for extendability.
    data: (bytes, bytes) big_map;

    // TZIP-16 metadata
    metadata: tzip16_metadata;

    // TZIP-12 tokens
    tzip12_tokens: (nat, bytes) big_map;

    // The next token ID to assign
    next_tzip12_token_id: nat;
}

type tzip12_record_operators = address set

type name_registry_return = operation list * name_registry_storage

type name_registry_action = ((bytes * address) * name_registry_storage) -> name_registry_return

type name_registry_action_map = (string, name_registry_action) big_map

type name_registry_main_storage = {
    store: name_registry_storage;
    actions: name_registry_action_map;
    trusted_senders: address set
}

let tzip12_operators_key = "operators"

[@inline]
let get_tzip12_operators (record: record) : tzip12_record_operators =
    match Map.find_opt tzip12_operators_key record.internal_data with
        Some operators ->
            (match (Bytes.unpack operators : tzip12_record_operators option) with
                Some o -> o
                // this should never happen
                | None -> (failwith "UNABLE_TO_PARSE_OPERATORS" : tzip12_record_operators))
        | None -> (Set.empty : tzip12_record_operators)

[@inline]
let is_tzip12_operator (addr, record: address * record) : bool =
    let operators = get_tzip12_operators(record) in

    Set.mem addr operators

// sets empty name to a reverse record with given address if it exists
let invalidate_reverse_record (name_to_invalidate, address_to_invalidate, store : bytes * address * name_registry_storage) : name_registry_storage =
    // find corresponding reverse record
    match Big_map.find_opt address_to_invalidate store.reverse_records with
        // reverse record doesn't exist -> nothing to do
        None -> store
        // reverse record found
        | Some existing_reverse ->
            (match existing_reverse.name with
                // no name assigned on reverse record -> nothing to do
                None -> store
                // name assigned on reverse record
                | Some existing_name ->
                    // it's the one to invalidate -> set its name to empty
                    (if existing_name = name_to_invalidate then
                        let updated_reverse = { existing_reverse with name = (None : bytes option) } in
                        // recreate name_registry_storage with updated reverse record
                        { store with reverse_records = Big_map.update address_to_invalidate (Some updated_reverse) store.reverse_records }
                    // it's different domain -> nothing to do
                    else store))

// returns whether a new record address means the corresponding reverse record needs to be invalidated
let needs_invalidating (new_address_opt, existing_address : address option * address) =
    match new_address_opt with 
        // the new address is None -> remove the reverse record
        None -> true
        // a new address exists -> remove the reverse record if the new address is different from the old address
        | Some new_address -> new_address <> existing_address

// sync's the reverse record (if any) to be consistent with a given new_address for a given existing record
let sync_reverse_record (name, existing_record_option, new_address_option, store : bytes * record option * address option * name_registry_storage) : name_registry_storage =
    match existing_record_option with
        None -> store // nothing to do
        | Some existing_record -> (match existing_record.address with
            // record has no address, nothing to do
            None -> store
            // record has an existing address
            | Some existing_address -> if needs_invalidating (new_address_option, existing_address)
                then invalidate_reverse_record (name, existing_address, store)
                else store)

// Returns an expiry based on an expiry_key.
let get_expiry (expiry_key_opt, store : (bytes option) * name_registry_storage) : timestamp option =
    match expiry_key_opt with
        // has a expiry key
        | Some expiry_key -> Big_map.find_opt expiry_key store.expiry_map
        // no expiry key -> implicitly valid
        | None -> (None : timestamp option)

// Returns whether a given record's expiry timestamp is valid.
let is_valid_expiry (expiry : timestamp option) : bool =
    match expiry with
        // has a expiry entry
        | Some expiry_timestamp -> Tezos.now < expiry_timestamp
        // no expiry entry -> implicitly valid
        | None -> true

// Returns a record with the given expiry timestamp is valid.
[@inline]
let is_valid_record (expiry_key_opt, store : (bytes option) * name_registry_storage) : bool =
    is_valid_expiry (get_expiry (expiry_key_opt, store))

// name_registry_return a record of given name after checking the record owner is the current sender
let require_parent_ownership (name, store, sender_address : bytes * name_registry_storage * address) : record =
    match Big_map.find_opt name store.records with
        | Some parent ->
            // currently valid
            if is_valid_record (parent.expiry_key, store) then
                // check the owner
                if parent.owner = sender_address || is_tzip12_operator(sender_address, parent) then parent
                else (failwith "NOT_AUTHORIZED" : record)
            // has expired
            else (failwith "PARENT_NOT_FOUND" : record)
        // no record exists
        | None -> (failwith "PARENT_NOT_FOUND" : record)

// checks that the given forward record exists and fails otherwise
let check_address (name, address_to_check, store : bytes * address * name_registry_storage) : unit =
    let checked = (match Big_map.find_opt name store.records with
        // record exists
        | Some record ->
            (match record.address with
                // record currently valid and address matches?
                | Some addr -> is_valid_record (record.expiry_key, store) && address_to_check = addr
                // no address
                | None -> false)
        // no record
        | None -> false) in
    if checked then () else (failwith "NAME_ADDRESS_MISMATCH" : unit)

// returns the domain name given a TZIP-12 token_id, or fails
[@inline]
let find_tzip12_token (token_id, store : nat * name_registry_storage) : bytes =
    match Big_map.find_opt token_id store.tzip12_tokens with
        | Some name -> name
        | None -> (failwith "FA2_TOKEN_UNDEFINED" : bytes)

// returns the TZIP-12 token balance for an owner and a token_id
let tzip12_get_balance (owner, token_id, store : address * nat * name_registry_storage) : nat =
    let name = find_tzip12_token (token_id, store) in
    match Big_map.find_opt name store.records with
        | Some record ->
            // currently valid
            if is_valid_record (record.expiry_key, store) && record.owner = owner then 1n
            // has expired
            else 0n
        // no record exists
        | None -> 0n

// Returns the record a given name resolves to or None if it doesn't resolve to any record.
let resolve_record (name, store : bytes * name_registry_storage) : record option =
    match Big_map.find_opt name store.records with
        | Some record ->
            // currently valid
            if is_valid_record (record.expiry_key, store) then Some record
            // has expired
            else (None : record option)
        // no record exists
        | None -> (None : record option)
