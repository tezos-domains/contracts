#include "../../contracts/TLDRegistrar/TLDRegistrar.mligo"

let lambda : tld_registrar_main_storage -> tld_registrar_main_return =
    fun (s : tld_registrar_main_storage) -> (let value : tld_record = {
        expiry = ("2000-01-01T10:10:10Z" : timestamp);
        internal_data = (Map.empty : (string, bytes) map);
    } in
    ([] : operation list), { s with store = { s.store with records = Big_map.update 0x74657a (Some value) s.store.records } })
