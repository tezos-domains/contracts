#include "../../contracts/TLDRegistrar/TLDRegistrar.mligo"

let lambda : tld_registrar_main_storage -> tld_registrar_main_return =
    fun (s : tld_registrar_main_storage) -> (
        ([] : operation list), { s with store = { s.store with owner = ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address) } }
    )
