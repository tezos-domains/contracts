#include "../../contracts/TLDRegistrar/TLDRegistrar.mligo"

let lambda : tld_registrar_main_storage -> tld_registrar_main_return =
    fun (s : tld_registrar_main_storage) -> (let value = { s with
        store = { s.store with
            tld = 0x74657a;
            config = Map.literal [
                (9n, 999n);
            ];
            name_registry = ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address);
        }
    } in ([] : operation list), value)
