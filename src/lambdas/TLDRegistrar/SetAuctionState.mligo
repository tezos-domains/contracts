#include "../../contracts/TLDRegistrar/TLDRegistrar.mligo"

let lambda : tld_registrar_main_storage -> tld_registrar_main_return =
    fun (s : tld_registrar_main_storage) -> (let value : auction_state = {
        last_bid = 123tez;
        last_bidder = ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address);
        ends_at = ("2000-01-01T10:10:10Z" : timestamp);
        ownership_period = 555n;
    } in
    ([] : operation list), { s with store = { s.store with auctions = Big_map.update 0x74657a (Some value) s.store.auctions } })
