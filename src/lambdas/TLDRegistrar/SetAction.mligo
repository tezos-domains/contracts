#include "../../contracts/TLDRegistrar/TLDRegistrar.mligo"

let lambda : tld_registrar_main_storage -> tld_registrar_main_return =
    fun (s : tld_registrar_main_storage) -> (
        let value : (action_lambda_param * tld_registrar_storage) -> tld_registrar_return = 
            fun (param, store : action_lambda_param * tld_registrar_storage) -> (([] : operation list), store)
        in ([] : operation list), { s with actions = Big_map.update "ActionName" (Some value) s.actions }
    )
