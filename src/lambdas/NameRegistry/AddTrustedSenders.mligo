#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (store : name_registry_main_storage) ->
        let add = (fun (acc, item : address set * address) -> Set.add item acc) in
        let additions : address list = [("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address)] in
        let new_store = {
            store with trusted_senders = List.fold add additions store.trusted_senders
        } in
    ([] : operation list), new_store
