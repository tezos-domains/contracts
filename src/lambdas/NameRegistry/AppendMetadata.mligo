#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (
        let value : bytes = 0x1234 in
        let old_value : bytes = (match Big_map.find_opt "contents" s.store.metadata with
            | Some v -> v
            | None -> 0x) in
        ([] : operation list), { s with store = { s.store with metadata = Big_map.update "contents" (Some (old_value ^ value)) s.store.metadata } }
    )
