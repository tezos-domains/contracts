#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s :name_registry_main_storage) -> (
        ([] : operation list), { s with store = { s.store with owner = ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address) } }
    )
