#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (
        let value : ((bytes * address) * name_registry_storage) -> name_registry_return = 
            fun (param, store : (bytes * address) * name_registry_storage) -> (([] : operation list), store)
        in ([] : operation list), { s with actions = Big_map.update "ActionName" (Some value) s.actions }
    )
