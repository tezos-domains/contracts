#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) ->
        let names : bytes list = [0x7a7a7a7a7a] in
        let generate_id (store, name : name_registry_storage * bytes) : name_registry_storage = (
            let record = (match Big_map.find_opt name store.records with
                | Some record -> record
                | None -> (failwith "NOT_FOUND" : record)) in
            (match record.tzip12_token_id with
                | None -> (
                    let new_token_id = store.next_tzip12_token_id in
                    let store = { store with
                        next_tzip12_token_id = store.next_tzip12_token_id + 1n;
                        tzip12_tokens = Big_map.update new_token_id (Some name) store.tzip12_tokens
                    } in
                    let record = { record with tzip12_token_id = Some new_token_id } in
                    { store with records = Big_map.update name (Some record) store.records }
                )
                | Some id -> store)
        ) in
        let store = List.fold generate_id names s.store in
        ([] : operation list), { s with store = store }
