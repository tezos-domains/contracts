#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (let value : record = {
        address = Some ("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" : address);
        owner = ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address);
        data = (Map.empty : data_map);
        internal_data = (Map.empty : data_map);
        expiry_key = Some 0x7a7a7a7a7a;
        level = 999n;
        tzip12_token_id = (None : nat option);
    } in ([] : operation list), { s with store = { s.store with records = Big_map.update 0x74657a (Some value) s.store.records } })
