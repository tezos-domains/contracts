#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (let value : reverse_record = {
        name = Some 0x7a7a7a7a7a;
        owner = ("tz1aoQSwjDU4pxSwT5AsBiK5Xk15FWgBJoYr" : address);
        internal_data = (Map.empty : data_map);
    } in ([] : operation list), { s with store = { s.store with reverse_records = Big_map.update ("tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj" : address) (Some value) s.store.reverse_records } })
