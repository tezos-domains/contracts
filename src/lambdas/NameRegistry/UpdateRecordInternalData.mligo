#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (
        let name = 0x74657a in
        let existing = (match Big_map.find_opt name s.store.records with Some existing -> existing | None -> (failwith "not found" : record)) in
        let value : record = { existing with internal_data = (Map.update "what" (Some 0x7a7a7a7a7a) existing.internal_data); } in
        ([] : operation list), { s with store = { s.store with records = Big_map.update name (Some value) s.store.records } }
    )
