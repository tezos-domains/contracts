#include "../../contracts/NameRegistry/NameRegistry.mligo"

let lambda : name_registry_main_storage -> name_registry_main_return =
    fun (s : name_registry_main_storage) -> (
        ([] : operation list), { s with store = { s.store with expiry_map = Big_map.update 0x74657a (Some ("2000-01-01T10:10:10Z" : timestamp)) s.store.expiry_map } }
    )
