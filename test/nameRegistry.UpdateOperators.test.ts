import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { MichelsonType, unpackDataBytes } from '@taquito/michel-codec';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { itShouldFail } from './common';
import { setDomain, unpackOperators } from './nameRegistry.common';

describe('NameRegistry.UpdateOperators', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registry: Contract;
    let tezos: TezosToolkit;
    let store: any;

    before(async () => {
        tezos = tezosToolkit();
        const records = new MichelsonMap<string, any>();
        setDomain(records, 'tez', alice.pkh, alice.pkh, {}, 1);

        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, {}, 2, encodeString('alice.tez'));
        setDomain(records, 'bob.tez', alice.pkh, bob.pkh, {}, 2, encodeString('bob.tez'));
        setDomain(records, 'charlie.tez', alice.pkh, alice.pkh, {}, 2, encodeString('charlie.tez'));
        setDomain(records, 'david.tez', alice.pkh, alice.pkh, {}, 2, encodeString('david.tez'));
        setDomain(records, 'eve.tez', alice.pkh, eve.pkh, {}, 2, encodeString('eve.tez'));
        setDomain(records, 'fiona.tez', alice.pkh, eve.pkh, {}, 2, encodeString('fiona.tez'));
        setDomain(records, 'expired.tez', alice.pkh, eve.pkh, {}, 2, encodeString('expired.tez'));

        const expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let tokens = new MichelsonMap();
        tokens.set(1, encodeString('alice.tez'));
        tokens.set(2, encodeString('bob.tez'));
        tokens.set(3, encodeString('charlie.tez'));
        tokens.set(4, encodeString('david.tez'));
        tokens.set(5, encodeString('eve.tez'));
        tokens.set(6, encodeString('fiona.tez'));
        tokens.set(100, encodeString('expired.tez'));

        const registryDeployment = await nameRegistryUpgradeable.deploy(
            { actionNames: ['UpdateOperators'] },
            {
                records,
                expiry_map: expiryMap,
                tzip12_tokens: tokens,
            }
        );
        registry = registryDeployment.contract;

        registry = await tezos.contract.at(registryDeployment.contract.address);
        let mainStore: any = await registry.storage();
        store = mainStore.store;
    });

    itShouldFail(updateOperators, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(updateOperators, 'FA2_NOT_OWNER', { operators: [{ add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 1 } }], sender: bob });
    itShouldFail(updateOperators, 'FA2_NOT_OWNER', { operators: [{ remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 1 } }], sender: bob });
    itShouldFail(updateOperators, 'OWNER_PARAMETER_MISMATCH', { operators: [{ add_operator: { owner: bob.pkh, operator: eve.pkh, token_id: 1 } }] });
    itShouldFail(updateOperators, 'OWNER_PARAMETER_MISMATCH', { operators: [{ remove_operator: { owner: bob.pkh, operator: eve.pkh, token_id: 1 } }] });
    itShouldFail(updateOperators, 'FA2_TOKEN_UNDEFINED', { operators: [{ add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 20 } }] });
    itShouldFail(updateOperators, 'FA2_TOKEN_UNDEFINED', { operators: [{ remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 20 } }] });
    itShouldFail(updateOperators, 'RECORD_NOT_FOUND', {
        when: 'expired',
        operators: [{ add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 100 } }],
    });
    itShouldFail(updateOperators, 'RECORD_NOT_FOUND', {
        when: 'expired',
        operators: [{ remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 100 } }],
    });

    it('should add operators', async () => {
        await updateOperators({
            operators: [
                { add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 1 } },
                { add_operator: { owner: alice.pkh, operator: bob.pkh, token_id: 1 } },
                { add_operator: { owner: alice.pkh, operator: alice.pkh, token_id: 3 } },
            ],
        });

        const aliceOperators = await getOperators('alice.tez');
        const charlieOperators = await getOperators('charlie.tez');

        assert.equal(aliceOperators.size, 2);
        assert.isTrue(aliceOperators.has(eve.pkh));
        assert.isTrue(aliceOperators.has(bob.pkh));

        assert.equal(charlieOperators.size, 1);
        assert.isTrue(charlieOperators.has(alice.pkh));
    });

    it('should append operators', async () => {
        await updateOperators({
            operators: [{ add_operator: { owner: alice.pkh, operator: bob.pkh, token_id: 4 } }],
        });

        await updateOperators({
            operators: [{ add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 4 } }],
        });

        const operators = await getOperators('david.tez');

        assert.equal(operators.size, 2);
        assert.isTrue(operators.has(bob.pkh));
        assert.isTrue(operators.has(eve.pkh));
    });

    it('should remove operators', async () => {
        await updateOperators({
            operators: [
                { add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 2 } },
                { add_operator: { owner: alice.pkh, operator: alice.pkh, token_id: 2 } },
                { add_operator: { owner: alice.pkh, operator: alice.pkh, token_id: 5 } },
            ],
        });

        await updateOperators({
            operators: [
                { remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 2 } },
                { remove_operator: { owner: alice.pkh, operator: alice.pkh, token_id: 5 } },
            ],
        });

        const bobOperators = await getOperators('bob.tez');
        const eveOperators = await getOperators('eve.tez');

        assert.equal(bobOperators.size, 1);
        assert.isTrue(bobOperators.has(alice.pkh));

        assert.equal(eveOperators.size, 0);
    });

    it('should execute commands in order', async () => {
        await updateOperators({
            operators: [
                { add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 6 } },
                { remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 6 } },
            ],
        });

        let operators = await getOperators('fiona.tez');

        assert.equal(operators.size, 0);

        await updateOperators({
            operators: [
                { remove_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 6 } },
                { add_operator: { owner: alice.pkh, operator: eve.pkh, token_id: 6 } },
            ],
        });

        operators = await getOperators('fiona.tez');

        assert.equal(operators.size, 1);
    });

    async function updateOperators(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.update_operators(options.operators || []).send({ amount: options.amount || 0 }));
    }

    async function getOperators(name: string) {
        const record = await store.records.get(encodeString(name));

        return unpackOperators(record);
    }
});
