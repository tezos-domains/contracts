import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { itShouldFail } from './common';
import { setDomain } from './nameRegistry.common';

describe('NameRegistry.Transfer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registry: Contract;
    let registryStorage: any;
    let tezos: TezosToolkit;

    before(async () => {
        tezos = tezosToolkit();

        // deploy new
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, { test: '1234' });
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh, {}, 2, null);
        setDomain(records, 'bananas.tez', eve.pkh, eve.pkh);
        setDomain(records, 'charlie.tez', eve.pkh, eve.pkh);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh);

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('alice.tez'), new Date(2500, 0, 0));
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let tokens = new MichelsonMap();
        tokens.set(1, encodeString('alice.tez'));
        tokens.set(2, encodeString('bob.tez'));
        tokens.set(3, encodeString('bananas.tez'));
        tokens.set(4, encodeString('expired.tez'));
        tokens.set(5, encodeString('charlie.tez'));

        let registryUpgradeable = await nameRegistryUpgradeable.deploy(
            { actionNames: ['Transfer', 'UpdateOperators'] },
            {
                records,
                expiry_map: expiryMap,
                tzip12_tokens: tokens,
            }
        );

        // we need use Taquito directly to be able to change signers
        registry = await tezos.contract.at(registryUpgradeable.contract.address);
        let mainStore: any = await registry.storage();
        registryStorage = mainStore.store;
    });

    it('should transfer nothing on empty transfers', async () => {
        await transfer({ transfers: [] });
    });

    it('should transfer nothing on empty destination list', async () => {
        await transfer({ transfers: [{ from_: alice.pkh, txs: [] }] });
    });

    it('should transfer nothing on zero amount', async () => {
        await transfer({ transfers: [{ from_: alice.pkh, txs: [{ to_: bob.pkh, token_id: 1, amount: 0 }] }] });

        assert.equal((await registryStorage.records.get(encodeString('alice.tez'))).owner, alice.pkh);
    });

    it('should transfer one', async () => {
        await transfer({ transfers: [{ from_: alice.pkh, txs: [{ to_: bob.pkh, token_id: 1, amount: 1 }] }] });

        assert.equal((await registryStorage.records.get(encodeString('alice.tez'))).owner, bob.pkh);
    });

    it('should transfer on behalf of eve when alice is an operator', async () => {
        await updateOperators({ operators: [{ add_operator: { owner: eve.pkh, operator: alice.pkh, token_id: 5 } }], sender: eve });

        await transfer({ transfers: [{ from_: eve.pkh, txs: [{ to_: bob.pkh, token_id: 5, amount: 1 }] }] });

        assert.equal((await registryStorage.records.get(encodeString('charlie.tez'))).owner, bob.pkh);
    });

    it('should transfer many in one transfer', async () => {
        await transfer({
            transfers: [
                {
                    from_: bob.pkh,
                    txs: [
                        { to_: alice.pkh, token_id: 1, amount: 1 },
                        { to_: alice.pkh, token_id: 2, amount: 1 },
                    ],
                },
            ],
            sender: bob,
        });

        assert.equal((await registryStorage.records.get(encodeString('alice.tez'))).owner, alice.pkh);
        assert.equal((await registryStorage.records.get(encodeString('bob.tez'))).owner, alice.pkh);
    });

    it('should transfer many in multiple transfers', async () => {
        await transfer({
            transfers: [
                {
                    from_: alice.pkh,
                    txs: [{ to_: bob.pkh, token_id: 1, amount: 1 }],
                },
                {
                    from_: alice.pkh,
                    txs: [{ to_: bob.pkh, token_id: 2, amount: 1 }],
                },
            ],
        });

        assert.equal((await registryStorage.records.get(encodeString('alice.tez'))).owner, bob.pkh);
        assert.equal((await registryStorage.records.get(encodeString('bob.tez'))).owner, bob.pkh);
    });

    it('should transfer same token in multiple transfers', async () => {
        await transfer({
            transfers: [
                {
                    from_: bob.pkh,
                    txs: [{ to_: bob.pkh, token_id: 1, amount: 1 }],
                },
                {
                    from_: bob.pkh,
                    txs: [{ to_: eve.pkh, token_id: 1, amount: 1 }],
                },
            ],
            sender: bob,
        });

        assert.equal((await registryStorage.records.get(encodeString('alice.tez'))).owner, eve.pkh);
    });

    itShouldFail(transfer, 'FA2_TOKEN_UNDEFINED', { transfers: [{ from_: alice.pkh, txs: [{ to_: bob.pkh, token_id: 9, amount: 1 }] }] });

    // fails when bob tries to send on behalf of eve
    itShouldFail(transfer, 'FA2_NOT_OPERATOR', { transfers: [{ from_: eve.pkh, txs: [{ to_: bob.pkh, token_id: 3, amount: 1 }] }], sender: bob });

    // fails when alice tries to transfer bananas.tez, but it belongs to eve
    itShouldFail(transfer, 'FA2_INSUFFICIENT_BALANCE', {
        when: 'domain has a different owner',
        transfers: [{ from_: alice.pkh, txs: [{ to_: bob.pkh, token_id: 3, amount: 1 }] }],
    });

    // fails when alice tries to transfer bananas.tez, but it has expired (the total balance is 0)
    itShouldFail(transfer, 'FA2_INSUFFICIENT_BALANCE', {
        when: 'domain has expired',
        transfers: [{ from_: eve.pkh, txs: [{ to_: alice.pkh, token_id: 4, amount: 1 }] }],
        sender: eve,
    });

    itShouldFail(transfer, 'AMOUNT_NOT_ZERO', { amount: 123 });

    async function updateOperators(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.update_operators(options.operators || []).send({ amount: options.amount || 0 }));
    }

    async function transfer(options: any) {
        let sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.transfer(options.transfers || []).send({ amount: options.amount || 0 }));
    }
});
