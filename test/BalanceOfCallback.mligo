type balance_of_request =
[@layout:comb]
{
    owner: address;
    token_id: nat;
}

type balance_of_response =
[@layout:comb]
{
    request: balance_of_request;
    balance: nat;
}

type response_list = balance_of_response list

let main (param, store : response_list * response_list option) : (operation list * response_list option) =
    // always store the last param
    ([] : operation list), Some param
