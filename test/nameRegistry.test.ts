import { InMemorySigner } from '@taquito/signer';
import { Contract } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import noop from '../common/lambdas/noop';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { assertThrowsAsync } from './common';

describe('NameRegistry', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;

    let tezos = tezosToolkit();
    let registry: Contract;

    before(async () => {
        let registryDeployment = await nameRegistryUpgradeable.deploy({ actionNames: [] });

        // we need use Taquito directly to be able to change signers
        registry = await tezos.contract.at(registryDeployment.contract.address);
    });

    it('execute should fail with NOT_TRUSTED_SENDER', async () => {
        tezos.setSignerProvider(new InMemorySigner(bob.sk));
        await assertThrowsAsync(async () => await registry.methods.execute('Foo', '', alice.pkh).send(), 'NOT_TRUSTED_SENDER');
    });

    it('admin_update should succeed', async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        let op = await registry.methods.admin_update(noop).send();
        await op.confirmation();
    });

    it('admin_update should fail with NOT_AUTHORIZED', async () => {
        tezos.setSignerProvider(new InMemorySigner(bob.sk));
        await assertThrowsAsync(async () => await registry.methods.admin_update(noop).send(), 'NOT_AUTHORIZED');
    });
});
