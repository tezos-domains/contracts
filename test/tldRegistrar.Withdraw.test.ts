import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { compileContract } from '../common/compile';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import accounts from '../common/sandbox-accounts';
import { tezosToolkit } from '../common/tezos-toolkit';
import { UpgradeableDeployment } from '../common/upgradeables';

describe('TLDRegistrar.Withdraw', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;

    let registrarDeployment: UpgradeableDeployment;
    let registrar: Contract;
    let registrarStore: any;
    let tezos = tezosToolkit();
    let withdrawRecipientContract: Contract;

    before(async () => {
        await nameRegistryUpgradeable.deploy({ actionNames: [] });

        let balances = new MichelsonMap<string, number>();
        balances.set(alice.pkh, 3_500_000);
        balances.set(bob.pkh, 1_500_000);
        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                balance: 5,
                actionNames: ['Withdraw'],
            },
            {
                bidder_balances: balances,
            }
        );
        registrar = registrarDeployment.contract;
        let mainRegistrarStore: any = await registrar.storage();
        registrarStore = mainRegistrarStore.store;

        // deploy WithdrawRecipient
        let code = await compileContract('test/WithdrawRecipient.mligo');
        let op = await tezos.contract.originate({
            code: code,
            storage: null,
        });
        withdrawRecipientContract = await op.contract();
    });

    it('should withdraw', async () => {
        let actor = alice;
        let actorBalanceBefore = await tezos.tz.getBalance(actor.pkh);

        await withdraw({ actor });

        // check balance in contract is empty
        let balanceInContract = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balanceInContract);

        // check new balance of the actor
        let actorBalanceAfter = await tezos.tz.getBalance(actor.pkh);
        assert.approximately(actorBalanceAfter.toNumber(), actorBalanceBefore.toNumber() + 3_500_000, 10_000);

        // check new balance of the contract
        assert.equal(1_500_000, (await tezos.tz.getBalance(registrar.address)).toNumber());
    });

    it('should do nothing when withdrawing empty balance', async () => {
        let actor = alice;
        let actorBalanceBefore = await tezos.tz.getBalance(actor.pkh);

        await withdraw({ actor });

        // check balance in contract is empty
        let balanceInContract = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balanceInContract);

        // check new balance of the actor
        let actorBalanceAfter = await tezos.tz.getBalance(actor.pkh);
        assert.approximately(actorBalanceAfter.toNumber(), actorBalanceBefore.toNumber(), 10_000);

        // check balance of the contract
        assert.equal(1_500_000, (await tezos.tz.getBalance(registrar.address)).toNumber());
    });

    it('should withdraw calling an entrypoint', async () => {
        let actor = bob;
        let recipientBalanceBefore = await tezos.tz.getBalance(withdrawRecipientContract.address);

        await withdraw({ actor, recipient: withdrawRecipientContract.address + '%foo' });

        // check balance in contract is empty
        let balanceInContract = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balanceInContract);

        // check new balance of the actor
        let recipientBalanceAfter = await tezos.tz.getBalance(withdrawRecipientContract.address);
        assert.approximately(recipientBalanceAfter.toNumber(), recipientBalanceBefore.toNumber() + 1_500_000, 10_000);

        // check new balance of the contract
        assert.equal(0, (await tezos.tz.getBalance(registrar.address)).toNumber());
    });

    async function withdraw(options: any = {}) {
        let actor = options.actor || alice;
        let param = options.recipient || actor.pkh;
        await registrarDeployment.callProxy('Withdraw', param, actor.sk, 0);
    }
});
