import { MichelsonV1ExpressionBase } from '@taquito/rpc';
import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, ViewSimulationError } from '@taquito/taquito';
import { tzip16 } from '@taquito/tzip16';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { UpgradeableDeployment } from '../common/upgradeables';
import { setDomain } from './nameRegistry.common';

describe('NameRegistry', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registryUpgradeable: UpgradeableDeployment;
    let registry: Contract;
    let views: any;
    let tezos = tezosToolkit();

    before(async () => {
        // deploy new
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, { test: '1234' });
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh, {}, 2, null);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh);

        let expiryMap = new MichelsonMap<string, Date>();
        expiryMap.set(encodeString('alice.tez'), new Date(2500, 0, 0));
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let reverseRecords = new MichelsonMap();
        function setReverseRecord(address: string, name: string | null) {
            reverseRecords.set(address, {
                owner: address,
                name: name != null ? encodeString(name) : null,
                internal_data: new MichelsonMap(),
            });
        }
        setReverseRecord(alice.pkh, 'alice.tez');
        setReverseRecord(bob.pkh, 'bob.tez');
        setReverseRecord(eve.pkh, 'expired.tez');
        setReverseRecord('tz1Vap5NCJmT32jzArdFRu6JTZp8zySyXE8D', null);

        let tokens = new MichelsonMap();
        tokens.set(1, encodeString('alice.tez'));
        tokens.set(2, encodeString('bob.tez'));
        tokens.set(3, encodeString('expired.tez'));

        registryUpgradeable = await nameRegistryUpgradeable.deploy(
            { actionNames: ['UpdateOperators'] },
            {
                records,
                reverse_records: reverseRecords,
                expiry_map: expiryMap,
                tzip12_tokens: tokens,
            }
        );
        registry = registryUpgradeable.contract;

        // get TZIP-16 views
        let registryContract = await tezos.contract.at(registry.address, tzip16);
        views = await registryContract.tzip16().metadataViews();
    });

    async function updateOperators(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.update_operators(options.operators || []).send({ amount: options.amount || 0 }));
    }

    it('should return an address from resolve-name', async () => {
        let result = await views['resolve-name']().executeView(encodeString('alice.tez'));
        assert.isNotNull(result);
        assert.equal(result.name, encodeString('alice.tez'));
        assert.equal(result.address, alice.pkh);
        assert.equal(result.data.size, 1);
        assert.equal(result.data.get('test'), '1234');
        assert.equal(Date.parse(result.expiry), new Date(2500, 0, 0).getTime());
    });

    it('should return an address from resolve-name when no expiry', async () => {
        let result = await views['resolve-name']().executeView(encodeString('bob.tez'));
        assert.isNotNull(result);
        assert.equal(result.name, encodeString('bob.tez'));
        assert.equal(result.address, bob.pkh);
        assert.equal(result.data.size, 0);
        assert.equal(result.expiry, null);
    });

    it('should return no address from resolve-name when not found', async () => {
        let result = await views['resolve-name']().executeView(encodeString('unknown.tez'));
        assert.isNull(result);
    });

    it('should return no address from resolve-name when expired', async () => {
        let result = await views['resolve-name']().executeView(encodeString('expired.tez'));
        assert.isNull(result);
    });

    it('should return a name from resolve-address', async () => {
        let result = await views['resolve-address']().executeView(alice.pkh);
        assert.isNotNull(result);
        assert.equal(result.name, encodeString('alice.tez'));
        assert.equal(result.address, alice.pkh);
        assert.equal(result.data.size, 1);
        assert.equal(result.data.get('test'), '1234');
        assert.equal(Date.parse(result.expiry), new Date(2500, 0, 0).getTime());
    });

    it('should return a name from resolve-address when no expiry', async () => {
        let result = await views['resolve-address']().executeView(bob.pkh);
        assert.isNotNull(result);
        assert.equal(result.name, encodeString('bob.tez'));
        assert.equal(result.address, bob.pkh);
        assert.equal(result.data.size, 0);
        assert.equal(result.expiry, null);
    });

    it('should return no name from resolve-address when expired', async () => {
        let result = await views['resolve-address']().executeView(eve.pkh);
        assert.isNull(result);
    });

    it('should return no name from resolve-address when null', async () => {
        let result = await views['resolve-address']().executeView('tz1Vap5NCJmT32jzArdFRu6JTZp8zySyXE8D');
        assert.isNull(result);
    });

    it('should return no name from resolve-address when unknown', async () => {
        let result = await views['resolve-address']().executeView('tz1QMNiDxZ1fWGBJT2YDrwc4XtJSP6LWpQeD');
        assert.isNull(result);
    });

    it('should return balance of 1 from get_balance on domain with expiry', async () => {
        let result = await views['get_balance']().executeView(alice.pkh, 1);
        assert.equal(result, 1);
    });

    it('should return balance of 1 from get_balance on domain with no expiry', async () => {
        let result = await views['get_balance']().executeView(bob.pkh, 2);
        assert.equal(result, 1);
    });

    it('should return balance of 0 from get_balance on domain owned by another account', async () => {
        let result = await views['get_balance']().executeView(alice.pkh, 2);
        assert.equal(result, 0);
    });

    it('should return balance of 0 from get_balance on expired domain', async () => {
        let result = await views['get_balance']().executeView(eve.pkh, 3);
        assert.equal(result, 0);
    });

    it('should return balance of 0 from get_balance on expired domain owned by another account', async () => {
        let result = await views['get_balance']().executeView(alice.pkh, 3);
        assert.equal(result, 0);
    });

    it('should fail with FA2_TOKEN_UNDEFINED in get_balance on invalid token_id', async () => {
        await assertViewThrowsAsync(async () => await views['get_balance']().executeView(alice.pkh, 4), 'FA2_TOKEN_UNDEFINED');
    });

    it('should return supply of 1 from total_supply on domain with expiry', async () => {
        let result = await views['total_supply']().executeView(1);
        assert.equal(result, 1);
    });

    it('should return supply of 1 from total_supply on domain with no expiry', async () => {
        let result = await views['total_supply']().executeView(2);
        assert.equal(result, 1);
    });

    it('should return supply of 0 from total_supply on an expired domain', async () => {
        let result = await views['total_supply']().executeView(3);
        assert.equal(result, 0);
    });

    it('should fail with FA2_TOKEN_UNDEFINED in total_supply on invalid token_id', async () => {
        await assertViewThrowsAsync(async () => await views['total_supply']().executeView(4), 'FA2_TOKEN_UNDEFINED');
    });

    it('should return true from is_operator if specified address is an operator and owner matches', async () => {
        await updateOperators({ operators: [{ add_operator: { token_id: 1, operator: eve.pkh, owner: alice.pkh } }] });

        assert.isTrue(await views['is_operator']().executeView(alice.pkh, eve.pkh, 1));
        assert.isFalse(await views['is_operator']().executeView(bob.pkh, eve.pkh, 1));
        assert.isFalse(await views['is_operator']().executeView(alice.pkh, bob.pkh, 1));
    });

    it('should fail with FA2_TOKEN_UNDEFINED in is_operator on invalid token_id', async () => {
        await assertViewThrowsAsync(async () => await views['is_operator']().executeView(alice.pkh, bob.pkh, 4), 'FA2_TOKEN_UNDEFINED');
    });

    it('should return metadata from token_metadata', async () => {
        let result = await views['token_metadata']().executeView(1);
        assert.isNotNull(result);
        assert.equal(result.token_id, 1);
        assert.equal(result.token_info.get('name'), encodeString('alice.tez'));
        assert.equal(result.token_info.get('decimals'), encodeString('0'));
        assert.equal(result.token_info.get('symbol'), encodeString('DOMAIN'));
    });

    it('should fail with FA2_TOKEN_UNDEFINED in token_metadata on invalid token_id', async () => {
        await assertViewThrowsAsync(async () => await views['token_metadata']().executeView(4), 'FA2_TOKEN_UNDEFINED');
    });

    async function assertViewThrowsAsync(fn: () => Promise<any>, expectedError: string) {
        try {
            await fn();
        } catch (e) {
            assert.isTrue(e instanceof ViewSimulationError);

            let failWith = (e as ViewSimulationError).failWith as MichelsonV1ExpressionBase;
            assert.isObject(failWith);
            assert.equal(failWith.string, expectedError);
            return;
        }
        assert.fail('did not fail');
    }
});
