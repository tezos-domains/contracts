import { MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { UpgradeableDeployment } from '../common/upgradeables';
import { assertThrowsAsync, getTezosTime, invalidLabels, itShouldFail, validLabels } from './common';
import { setRecord, setAuction } from './tldRegistrar.common';

describe('TLDRegistrar.Bid', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let time: number;
    let registrarDeployment: UpgradeableDeployment;
    let registrarStore: any;

    before(async () => {
        time = await getTezosTime();

        await nameRegistryUpgradeable.deploy({ actionNames: [] });

        let records = new MichelsonMap<string, any>();
        setRecord(records, 'taken', new Date(2100, 1, 1));
        setRecord(records, 'recently-expired', new Date(time - 24 * 60 * 60 * 1000));
        setRecord(records, 'recently-expired-with-settlement-expired', new Date(time - 24 * 60 * 60 * 1000));
        setRecord(records, 'expired', new Date(2000, 1, 1));

        let auctions = new MichelsonMap<string, Date>();
        setAuction(auctions, 'aged', 5_000_000, new Date(time - 366 * 24 * 60 * 60 * 1000));
        setAuction(auctions, 'settleable', 15_000_000, new Date(time - 60 * 60 * 1000));
        setAuction(auctions, 'open-1000000tez', 1_000_000, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'open-1100000tez', 1_100_000, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'open-1500000tez', 1_500_000, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'open-99000000tez', 99_000_000, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'open-5409090tez', 5_409_090, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'open-5409091tez', 5_409_091, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'recently-expired-with-settlement-expired', 5_000_000, new Date(time - 400 * 24 * 60 * 60 * 1000));

        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                launchDates: [
                    new Date(time), // in auction by default
                    null, // no launch for 1-letter labels
                    null, // no launch for 2-letter labels
                    new Date(2100, 1, 1), // future launch date for 3-letter labels
                    new Date(2020, 1, 1), // 4-letter labels in FIFS
                ],
                actionNames: ['Bid'],
                standardPrices: [null, null, null, null, null, null, 2739726027 * 3],
            },
            {
                records,
                auctions,
            }
        );

        let mainStorage: any = await registrarDeployment.contract.storage();
        registrarStore = mainStorage.store;
    });

    itShouldFail(bid, 'LABEL_NOT_AVAILABLE', { label: 'a', when: 'never launched' });
    itShouldFail(bid, 'LABEL_NOT_AVAILABLE', { label: 'ali', when: 'not launched yet' });
    itShouldFail(bid, 'AUCTION_ENDED', { label: 'alic', when: 'auction ended with no bids' });
    itShouldFail(bid, 'AUCTION_ENDED', { label: 'expired', when: 'auction ended with no bids on expired domain' });
    itShouldFail(bid, 'AUCTION_ENDED', { label: 'settleable', when: 'ended auction currently settleable' });
    itShouldFail(bid, 'LABEL_TAKEN', { label: 'taken' });
    itShouldFail(bid, 'BID_TOO_LOW', { bid: 500000 });
    itShouldFail(bid, 'AMOUNT_TOO_LOW', { bid: 1000000, amount: 0.5 });

    it('should store a new bid', async () => {
        let label = `alice`;
        let actor = alice;
        await bid({ actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 1000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 14 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);

        let balance = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balance);
    });

    it('should replace a bid with the next minimum bid', async () => {
        let label = `alice`;
        let actor = bob;
        await bid({ amount: 1.1, bid: 1100000, actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 1100000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 14 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);

        let balance = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balance);
    });

    itShouldFail(bid, 'BID_TOO_LOW', { label: `alice`, bid: 1199999, amount: 1.199999, when: '1mutez short' });

    it('should store a new bid and save balance', async () => {
        let label = `barbara`;
        let actor = eve;
        await bid({ amount: 2.5, bid: 2000000, actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 2000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 14 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);

        let balance = await registrarStore.bidder_balances.get(actor.pkh);
        assert.equal(balance, 500000);
    });

    it('should replace a bid and save balance', async () => {
        let label = `barbara`;
        let actor = eve;
        await bid({ amount: 4, bid: 3000000, actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 3000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 14 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);

        let balance = await registrarStore.bidder_balances.get(actor.pkh);
        assert.equal(balance, 3500000);
    });

    it('should store a new bid after settlement expired', async () => {
        let actor = alice;
        let label = `aged`;
        await bid({ actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 1000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 13 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);
    });

    it('should store a new bid on recently expired domain', async () => {
        let actor = alice;
        let label = `recently-expired`;
        await bid({ actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 1000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 13 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);
    });

    it('should store a new bid on recently expired domain with existing expired auction settlement', async () => {
        let actor = alice;
        let label = `recently-expired-with-settlement-expired`;
        await bid({ actor, label });

        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.equal(registrarAuction.last_bid, 1000000);
        assert.equal(registrarAuction.last_bidder, actor.pkh);

        let expectedAuctionEnd = time + 13 * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarAuction.ends_at), expectedAuctionEnd);

        assert.equal(registrarAuction.ownership_period, 365);
    });

    itCalculatesMinBid('sixsix', 3_000_000); // more expensive starting bid (6 letter)
    itCalculatesMinBid('open-1000000tez', 1_100_000);
    itCalculatesMinBid('open-1100000tez', 1_200_000);
    itCalculatesMinBid('open-1500000tez', 1_700_000);
    itCalculatesMinBid('open-99000000tez', 108_900_000);

    // check bounds of rounding down/up
    itCalculatesMinBid('open-5409090tez', 5_900_000);
    itCalculatesMinBid('open-5409091tez', 6_000_000);

    for (let label of validLabels) {
        it(`should succeed on valid label '${label}'`, async () => {
            await bid({ label });
        });
    }

    for (let i = 0; i < invalidLabels.length; i++) {
        let label = invalidLabels[i];
        itShouldFail(bid, 'INVALID_LABEL', { label, when: `called with label ${i}: "${label}"` });
    }
    itShouldFail(bid, 'LABEL_EMPTY', { label: '' });

    it(`should fail with INTERNAL_CONFIG_INVALID when min_bid_increase_ration = 0`, async () => {
        // deploy with invalid config
        let auctions = new MichelsonMap<string, Date>();
        setAuction(auctions, 'open', 1_000_000, new Date(time + 60 * 60 * 1000));
        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                actionNames: ['Bid'],
                minBidIncreaseRatio: 0,
            },
            {
                auctions,
            }
        );

        await assertThrowsAsync(() => bid({ label: 'open' }), 'INTERNAL_CONFIG_INVALID');
    });

    function itCalculatesMinBid(label: string, expectedMinimumBid: number) {
        let when = `expected minimum bid of ${label} is ${expectedMinimumBid}`;
        let incorrectBid = expectedMinimumBid - 1;

        itShouldFail(bid, 'BID_TOO_LOW', { label, bid: incorrectBid, amount: incorrectBid / 1000000, when });
        it(`should bid successfully when ${when}`, async () => {
            let actor = alice;
            await bid({ actor, label, bid: expectedMinimumBid, amount: expectedMinimumBid / 1000000 });

            let registrarAuction = await registrarStore.auctions.get(encodeString(label));
            assert.equal(registrarAuction.last_bid, expectedMinimumBid);
            assert.equal(registrarAuction.last_bidder, actor.pkh);
        });
    }

    async function bid(options: any = {}) {
        let param = {
            label: encodeString(options.label !== undefined ? options.label : `testing`),
            bid: options.bid || 1000000,
        };
        await registrarDeployment.callProxy('Bid', param, (options.actor || alice).sk, options.amount || 1);
    }
});
