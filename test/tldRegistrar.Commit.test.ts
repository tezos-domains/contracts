import { assert } from 'chai';
import accounts from '../common/sandbox-accounts';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { getTezosTime, itShouldFail } from './common';
import { UpgradeableDeployment } from '../common/upgradeables';

describe('TLDRegistrar.Commit', () => {
    const alice = accounts.alice;

    let registrarStore: any;
    let time: number;
    let registrarDeployment: UpgradeableDeployment;

    before(async () => {
        time = await getTezosTime();
        await nameRegistryUpgradeable.deploy({ actionNames: [] });

        registrarDeployment = await tldRegistrarUpgradeable.deploy({ actionNames: ['Commit'] });
        let registrar = registrarDeployment.contract;
        let mainStore: any = await registrar.storage();
        registrarStore = mainStore.store;
    });

    it('should store commitment', async () => {
        await commit({ commitment: '12345678' });

        var timestamp = await registrarStore.commitments.get('12345678');
        assert.isDefined(timestamp);
        assert.isAtLeast(Date.parse(timestamp), time);
    });

    itShouldFail(commit, 'COMMITMENT_EXISTS', { commitment: '12345678' });
    itShouldFail(commit, 'AMOUNT_NOT_ZERO', { amount: 2 });

    async function commit(options: any = {}) {
        await registrarDeployment.callProxy('Commit', options.commitment || '00', alice.sk, options.amount);
    }
});
