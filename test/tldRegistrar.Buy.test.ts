import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { encodeString } from '../common/convert';
import createCommitment from '../common/createCommitment';
import nameRegistry_setRecord from '../common/lambdas/nameRegistry_setRecord';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import configKeys from '../common/tldRegistrar.configKeys';
import { UpgradeableDeployment } from '../common/upgradeables';
import { assertThrowsAsync, getTezosTime, itShouldFail } from './common';
import { setAuction, setRecord } from './tldRegistrar.common';

describe('TLDRegistrar.Buy', () => {
    const { alice, bob } = accounts;
    const treasury = 'tz1burnburnburnburnburnburnburjAYjjX';

    let registryStore: any;
    let registrarDeployment: UpgradeableDeployment;
    let registry: Contract;
    let registrarStore: any;
    let tezos = tezosToolkit();
    let previousTreasuryBalance: number;

    before(async () => {
        let time = await getTezosTime();

        let registryDeployment = await nameRegistryUpgradeable.deploy({ actionNames: ['SetChildRecord'] });
        registry = registryDeployment.contract;
        let registryMainStore: any = await registry.storage();
        registryStore = registryMainStore.store;

        let records = new MichelsonMap<string, any>();
        setRecord(records, 'expired', new Date(2020, 1, 1));
        setRecord(records, 'recently-expired', new Date(time - 24 * 60 * 60 * 1000));
        setRecord(records, 'recently-expired-with-settlement-expired', new Date(time - 24 * 60 * 60 * 1000));
        setRecord(records, 'taken', new Date(2100, 1, 1));

        let auctions = new MichelsonMap<string, any>();
        setAuction(auctions, 'in-auction', 15_000_000, new Date(time + 60 * 60 * 1000));
        setAuction(auctions, 'settleable', 15_000_000, new Date(time - 60 * 60 * 1000));
        setAuction(auctions, 'settlement-expired', 5_000_000, new Date(time - 366 * 24 * 60 * 60 * 1000));
        setAuction(auctions, 'recently-expired-with-settlement-expired', 5_000_000, new Date(time - 400 * 24 * 60 * 60 * 1000));
        setAuction(auctions, 'expired-second-auction', 5_000_000, new Date(time - 380 * 24 * 60 * 60 * 1000));

        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                launchDates: [
                    new Date(2020, 1, 1), // labels of 5+ letters available for buying
                    null, // no launch for 1-letter labels
                    null, // no launch for 2-letter labels
                    new Date(2100, 1, 1), // future launch date for 3-letter labels
                    new Date(time - 60 * 60 * 1000), // in auction for 4-letter labels
                ],
                actionNames: ['Commit', 'Buy'],
                standardPrices: [null, null, null, null, null, null, 2739726027 * 3],
                treasury,
            },
            {
                auctions,
                records,
            }
        );
        let registrarMainStore: any = await registrarDeployment.contract.storage();
        registrarStore = registrarMainStore.store;

        await initTld();
    });

    async function initTld() {
        await runOperation(async () => registry.methods.admin_update(await nameRegistry_setRecord('tez', registrarDeployment.contract.address, 1)).send());
    }

    beforeEach(async () => {
        previousTreasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
    });

    it('should buy free domain for default amount', async () => {
        let label = 'alice';
        let duration = 366;

        // commit + reveal
        let commitment = await commit({ label });
        var op = await buy({ label, duration });

        await verifyRecord(label, await getTezosTime(op.includedInBlock), duration);
        await verifyProceedsTransfered(label, duration);
        await verifyCommitmentSpent(commitment);
    });

    it('should buy domain with amount from config (6 letter)', async () => {
        let label = 'alice1';
        let duration = 365;

        // commit + reveal
        let commitment = await commit({ label });
        var op = await buy({ label, duration });

        await verifyRecord(label, await getTezosTime(op.includedInBlock), duration);
        await verifyProceedsTransfered(label, duration);
        await verifyCommitmentSpent(commitment);
    });

    it('should buy free domain after auction settlement expired', async () => {
        let label = 'expired-second-auction';
        let duration = 365;

        // commit + reveal
        let commitment = await commit({ label });
        var op = await buy({ label, duration });

        await verifyRecord(label, await getTezosTime(op.includedInBlock), duration);
        await verifyProceedsTransfered(label, duration);
        await verifyCommitmentSpent(commitment);
    });

    it('should buy expired domain', async () => {
        let label = 'expired';
        let duration = 368;

        // commit + reveal
        let commitment = await commit({ label });
        var op = await buy({ label, duration });

        await verifyRecord(label, await getTezosTime(op.includedInBlock), duration);
        await verifyProceedsTransfered(label, duration);
        await verifyCommitmentSpent(commitment);
    });

    itShouldFail(buy, 'COMMITMENT_NOT_FOUND', { label: 'not-committed' });
    itShouldFailBuyWithCommit('LABEL_TAKEN', { label: `taken` });
    itShouldFailBuyWithCommit('LABEL_NOT_AVAILABLE', { label: `al`, when: 'not launched at all' }); // 2 letters
    itShouldFailBuyWithCommit('LABEL_NOT_AVAILABLE', { label: `ali`, when: 'not launched yet' }); // 3 letters
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', { label: `alic`, when: 'in implicit auction' }); // 4 letters
    itShouldFailBuyWithCommit('AMOUNT_TOO_LOW', { label: `alice2`, amount: 1, when: 'more expensive' }); // 6 letters
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', { label: `in-auction`, when: 'in existing auction' }); // auction is currently running
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', { label: `settleable`, when: 'in settlement period' }); // auction finished, not settled yet
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', { label: `recently-expired`, when: 'expired and in auction again' }); // recently expired, goes into auction again
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', { label: `settlement-expired`, when: 'expired settlement period and in auction again' }); // recently expired settlement period, goes into auction again
    itShouldFailBuyWithCommit('LABEL_IN_AUCTION', {
        label: `recently-expired-with-settlement-expired`,
        when: 'expired settlement period followed by bought domain that expired and is in auction again',
    }); // recently expired settlement period, goes into auction again
    itShouldFailBuyWithCommit('INVALID_LABEL', { label: 'a_li_ce' });
    itShouldFailBuyWithCommit('DURATION_TOO_LOW', { duration: 30, nonce: 5000 });
    itShouldFailBuyWithCommit('AMOUNT_TOO_LOW', { amountDiff: -0.001, nonce: 5001 });
    itShouldFailBuyWithCommit('AMOUNT_TOO_HIGH', { amountDiff: 0.001, nonce: 5002 });

    function itShouldFailBuyWithCommit(expectedError: string, options: any) {
        it(`should fail with ${expectedError}${options.when ? ` when ${options.when}` : ''}`, async () => {
            await commit({ label: options.label, nonce: options.nonce });
            await assertThrowsAsync(() => buy(options), expectedError);
        });
    }

    it(`should fail with COMMITMENT_NOT_FOUND on invalid nonce`, async () => {
        await commit({ label: 'invalid-nonce', nonce: 888 });
        await assertThrowsAsync(() => buy({ label: 'invalid-nonce' }), 'COMMITMENT_NOT_FOUND');
    });

    it(`should fail with INTERNAL_CONFIG_INVALID when default standard_price_per_day = 0`, async () => {
        // deploy with invalid config
        registrarDeployment = await tldRegistrarUpgradeable.deploy({
            actionNames: ['Commit', 'Buy'],
            standardPrices: [0],
        });
        await initTld();

        await commit();
        await assertThrowsAsync(() => buy({ amount: 0 }), 'INTERNAL_CONFIG_INVALID');
    });

    async function commit(options: any = {}) {
        let commitment = await createCommitment(options.label || 'testing', alice.pkh, options.nonce || 777);
        await registrarDeployment.callProxy('Commit', commitment, alice.sk, options.amount);
        return commitment;
    }

    async function buy(options: any = {}) {
        const label = options.label || 'testing';
        let param = {
            owner: alice.pkh,
            address: bob.pkh,
            data: new MichelsonMap(),
            nonce: options.nonce || 777,
            label: encodeString(label),
            duration: options.duration || 365,
        };
        param.data.set('foo', encodeString('bar'));
        let amount = (options.amount != undefined ? options.amount : calculatePrice(label, param.duration)) + (options.amountDiff || 0);
        let sender = options.sender || alice;
        return await registrarDeployment.callProxy('Buy', param, sender.sk, amount);
    }

    async function verifyRecord(label: string, baseTime: number, duration: number) {
        let registrarRecord = await registrarStore.records.get(encodeString(label));

        let expectedExpiry = baseTime + duration * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarRecord.expiry), expectedExpiry);

        let registryRecord = await registryStore.records.get(encodeString(`${label}.tez`));
        assert.equal(registryRecord.address, bob.pkh);
        assert.equal(registryRecord.owner, alice.pkh);
        assert.equal(registryRecord.data.get('foo'), encodeString('bar'));

        let actualExpiry = await registryStore.expiry_map.get(encodeString(`${label}.tez`));
        assert.equal(actualExpiry, registrarRecord.expiry);
    }

    async function verifyProceedsTransfered(label: string, duration: number) {
        let balance = (await tezos.tz.getBalance(registrarDeployment.contract.address)).toNumber();
        assert.equal(balance, 0);

        let treasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
        assert.equal(treasuryBalance - previousTreasuryBalance, calculatePrice(label, duration) * 1_000_000);
    }

    function calculatePrice(label: string, duration: number) {
        const key = label.length === 6 ? configKeys.standardPricePerDay(label.length) : configKeys.standardPricePerDay();
        const standardPrice = parseInt(registrarStore.config.get(key.toString()));
        return Math.round((duration * standardPrice) / 1_000_000) / 1_000_000;
    }

    async function verifyCommitmentSpent(commitment: string) {
        assert.isUndefined(await registrarStore.commitments.get(commitment));
    }
});
