import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';
import { setDomain } from './nameRegistry.common';

describe('NameRegistry.ClaimReverseRecord', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registryDeployment: UpgradeableDeployment;
    let registry: Contract;
    let store: any;

    before(async () => {
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'tez', alice.pkh, alice.pkh, {}, 1);
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh);
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('expired.tez'));
        setDomain(records, 'not-expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('not-expired.tez'));

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));
        expiryMap.set(encodeString('not-expired.tez'), new Date(2100, 0, 0));

        let reverseRecords = new MichelsonMap();
        reverseRecords.set(bob.pkh, {
            name: null,
            owner: bob.pkh,
            internal_data: new MichelsonMap(),
        });

        registryDeployment = await nameRegistryUpgradeable.deploy(
            { actionNames: ['ClaimReverseRecord'] },
            {
                records,
                expiry_map: expiryMap,
                reverse_records: reverseRecords,
            }
        );
        registry = registryDeployment.contract;
        let mainStore: any = await registry.storage();
        store = mainStore.store;
    });

    async function claimReverseRecord(options: any = {}) {
        let sender = options.sender || alice;
        let param = {
            name: encodeString(options.name || 'alice.tez'),
            owner: sender.pkh,
        };
        await registryDeployment.callProxy('ClaimReverseRecord', param, sender.sk, options.amount);
    }

    it('should create corresponding reverse record', async () => {
        await claimReverseRecord();

        let record = await store.reverse_records.get(alice.pkh);
        assert.equal(record.name, encodeString('alice.tez'));
        assert.equal(record.owner, alice.pkh);
    });

    it('should create corresponding reverse record when domain has valid expiry', async () => {
        await claimReverseRecord({ sender: eve, name: 'not-expired.tez' });

        let record = await store.reverse_records.get(eve.pkh);
        assert.equal(record.name, encodeString('not-expired.tez'));
        assert.equal(record.owner, eve.pkh);
    });

    it('should update corresponding reverse record', async () => {
        await claimReverseRecord({ sender: bob, name: 'bob.tez' });

        let record = await store.reverse_records.get(bob.pkh);
        assert.equal(record.name, encodeString('bob.tez'));
        assert.equal(record.owner, bob.pkh);
    });

    itShouldFail(claimReverseRecord, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(claimReverseRecord, 'NAME_ADDRESS_MISMATCH', { name: 'unknown.tez', when: 'corresponding domain does not exist' }); // corresponding forward record does not exist
    itShouldFail(claimReverseRecord, 'NAME_ADDRESS_MISMATCH', { sender: eve, name: 'expired.tez', when: 'corresponding domain expired' }); // corresponding forward record expired
    itShouldFail(claimReverseRecord, 'NAME_ADDRESS_MISMATCH', { sender: bob, when: 'corresponding domain points elsewhere' }); // sender does not correspond to forward record
});
