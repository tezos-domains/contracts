import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { encodeString } from '../common/convert';
import { nameRegistry_setExpiry } from '../common/lambdas/nameRegistry_setExpiry';
import nameRegistry_setRecord from '../common/lambdas/nameRegistry_setRecord';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import configKeys from '../common/tldRegistrar.configKeys';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';

describe('TLDRegistrar.Renew', () => {
    const alice = accounts.alice;
    const treasury = 'tz1burnburnburnburnburnburnburjAYjjX';

    let registrar: Contract;
    let registryStore: any;
    let registrarStore: any;
    let registrarDeployment: UpgradeableDeployment;
    let tezos = tezosToolkit();
    let previousTreasuryBalance: number;

    before(async () => {
        let registryDeployment = await nameRegistryUpgradeable.deploy({ actionNames: ['SetExpiry'] });
        let registry = registryDeployment.contract;
        let mainRegistryStore: any = await registry.storage();
        registryStore = mainRegistryStore.store;

        let records = new MichelsonMap<string, any>();
        records.set(encodeString('alice'), {
            expiry: new Date(2100, 1, 1),
            internal_data: new MichelsonMap(),
        });
        records.set(encodeString('alice1'), {
            expiry: new Date(2100, 1, 1),
            internal_data: new MichelsonMap(),
        });
        records.set(encodeString('alice2'), {
            expiry: new Date(2100, 1, 1),
            internal_data: new MichelsonMap(),
        });
        records.set(encodeString('expired'), {
            expiry: new Date(123456),
            internal_data: new MichelsonMap(),
        });
        records.set(encodeString('test'), {
            expiry: new Date(2100, 1, 1),
            internal_data: new MichelsonMap(),
        });

        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                actionNames: ['Renew'],
                standardPrices: [null, null, null, null, null, null, 2739726027 * 3],
                treasury,
            },
            {
                records,
            }
        );
        registrar = registrarDeployment.contract;
        let mainRegistrarStore: any = await registrar.storage();
        registrarStore = mainRegistrarStore.store;

        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord('tez', registrar.address, 1)).send());
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord('alice.tez', alice.pkh, 2, 'alice.tez')).send());
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setExpiry('alice.tez', new Date(2100, 1, 1))).send());
    });

    beforeEach(async () => {
        previousTreasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
    });

    it('should renew domain with default amount', async () => {
        let label = `alice`;
        let duration = 367;
        let aliceRecord = await registrarStore.records.get(encodeString(label));
        let previousExpiry = Date.parse(aliceRecord.expiry);

        await renew({ label, duration });

        await verifyRecord(label, previousExpiry, duration);
        await verifyProceedsTransfered(label, duration);
    });

    it('should renew domain with amount from config (6 letter)', async () => {
        let label = `alice1`;
        let duration = 367;
        let aliceRecord = await registrarStore.records.get(encodeString(label));
        let previousExpiry = Date.parse(aliceRecord.expiry);

        await renew({ label, duration });

        await verifyRecord(label, previousExpiry, duration);
        await verifyProceedsTransfered(label, duration);
    });

    itShouldFail(renew, 'DURATION_TOO_LOW', { duration: 30 });
    itShouldFail(renew, 'AMOUNT_TOO_LOW', { amountDiff: -0.001 });
    itShouldFail(renew, 'AMOUNT_TOO_LOW', { label: 'alice2', amount: 1, when: 'more expensive' });
    itShouldFail(renew, 'AMOUNT_TOO_HIGH', { amountDiff: 0.001 });
    itShouldFail(renew, 'LABEL_EXPIRED', { label: 'expired' });
    itShouldFail(renew, 'LABEL_NOT_FOUND', { label: 'wendy' });

    async function renew(options: any = {}) {
        const label = options.label || 'test';
        let param = {
            label: encodeString(label),
            duration: options.duration || 365,
        };
        let amount = (options.amount || calculatePrice(label, param.duration)) + (options.amountDiff || 0);
        let sender = options.sender || alice;
        return await registrarDeployment.callProxy('Renew', param, sender.sk, amount);
    }

    async function verifyRecord(label: string, baseTime: number, duration: number) {
        let expectedExpiry = baseTime + duration * 24 * 60 * 60 * 1000;

        let registrarRecord = await registrarStore.records.get(encodeString(label));
        assert.equal(Date.parse(registrarRecord.expiry), expectedExpiry);

        let actualExpiry = await registryStore.expiry_map.get(encodeString(`${label}.tez`));
        assert.equal(actualExpiry, registrarRecord.expiry);
    }

    async function verifyProceedsTransfered(label: string, duration: number) {
        let balance = (await tezos.tz.getBalance(registrarDeployment.contract.address)).toNumber();
        assert.equal(balance, 0);

        let treasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
        assert.equal(treasuryBalance - previousTreasuryBalance, calculatePrice(label, duration) * 1_000_000);
    }

    function calculatePrice(label: string, duration: number) {
        const key = label.length === 6 ? configKeys.standardPricePerDay(label.length) : configKeys.standardPricePerDay();
        const standardPrice = parseInt(registrarStore.config.get(key.toString()));
        return Math.round((duration * standardPrice) / 1_000_000) / 1_000_000;
    }
});
