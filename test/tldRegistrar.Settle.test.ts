import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { encodeString } from '../common/convert';
import nameRegistry_setRecord from '../common/lambdas/nameRegistry_setRecord';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { UpgradeableDeployment } from '../common/upgradeables';
import { getTezosTime, itShouldFail } from './common';
import { setAuction, setRecord } from './tldRegistrar.common';

describe('TLDRegistrar.Settle', () => {
    const { alice, bob } = accounts;
    const treasury = 'tz1burnburnburnburnburnburnburjAYjjX';

    let startTezosTime;
    let settleableAuctionEnd: any;
    let registryStore: any;

    let registrarDeployment: UpgradeableDeployment;
    let registrar: Contract;
    let registrarStore: any;
    let tezos = tezosToolkit();
    let previousTreasuryBalance: number;

    before(async () => {
        startTezosTime = await getTezosTime();

        let registryDeployment = await nameRegistryUpgradeable.deploy({ actionNames: ['SetChildRecord'] });
        let registry = registryDeployment.contract;
        let mainRegistryStore: any = await registry.storage();
        registryStore = mainRegistryStore.store;

        settleableAuctionEnd = startTezosTime - 60 * 60 * 1000;
        let auctions = new MichelsonMap<string, any>();
        setAuction(auctions, 'bid', 15_000_000, new Date(startTezosTime + 60 * 60 * 1000));
        setAuction(auctions, 'settleable', 15_000_000, new Date(settleableAuctionEnd));
        setAuction(auctions, 'in-auction-again', 5_000_000, new Date(startTezosTime - 366 * 24 * 60 * 60 * 1000));
        setAuction(auctions, 'in-auction-again-and-expired', 5_000_000, new Date(startTezosTime - 380 * 24 * 60 * 60 * 1000));

        let records = new MichelsonMap<string, Date>();
        setRecord(records, 'taken', new Date(2100, 1, 1));

        registrarDeployment = await tldRegistrarUpgradeable.deploy(
            {
                actionNames: ['Settle'],
                launchDates: [
                    new Date(startTezosTime),
                    null,
                    new Date(2100, 1, 1), // future launch date for 3-letter labels
                    new Date(2020, 1, 1), // 4-letter labels in FIFS
                ],
                treasury,
                balance: 20,
            },
            {
                auctions,
                records,
            }
        );
        registrar = registrarDeployment.contract;
        let mainRegistrarStore: any = await registrar.storage();
        registrarStore = mainRegistrarStore.store;

        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord('tez', registrar.address, 1)).send());
    });

    beforeEach(async () => {
        previousTreasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
    });

    itShouldFail(settle, 'LABEL_NOT_AVAILABLE', { label: 'a', when: 'never launched' });
    itShouldFail(settle, 'LABEL_NOT_AVAILABLE', { label: 'al', when: 'not launched yet' });
    itShouldFail(settle, 'LABEL_TAKEN', { label: 'taken' });
    itShouldFail(settle, 'NOT_SETTLEABLE', { label: 'implicit', when: 'not settleable yet with implicit auction' });
    itShouldFail(settle, 'NOT_SETTLEABLE', { label: 'bid', when: 'not settleable yet with existing bid' });
    itShouldFail(settle, 'NOT_SETTLEABLE', { label: 'in-auction-again', when: 'settlement period expired, in auction again' });
    itShouldFail(settle, 'NOT_SETTLEABLE', { label: 'in-auction-again-and-expired', when: 'settlement period expired, after second auction with no bids' });
    itShouldFail(settle, 'NOT_SETTLEABLE', { label: 'unknown', when: 'no such auction' });
    itShouldFail(settle, 'NOT_AUTHORIZED', { label: 'settleable', actor: bob, when: 'unauthorized account' });

    it('should settle', async () => {
        let actor = alice;
        let label = 'settleable';

        await settle({ actor, label });

        // check balance in contract is empty
        let balanceInContract = await registrarStore.bidder_balances.get(actor.pkh);
        assert.isUndefined(balanceInContract);

        // check the auction state is removed
        let registrarAuction = await registrarStore.auctions.get(encodeString(label));
        assert.isUndefined(registrarAuction);

        // check the record has been created
        await verifyRecord(label, settleableAuctionEnd, 365);

        // check proceeds have been transfered:
        // before settle: 20 tez on the contract
        // after settle: 15 tez transfered to the treasury, 5 tez remain in the contract
        let balance = (await tezos.tz.getBalance(registrarDeployment.contract.address)).toNumber();
        assert.equal(balance, 5_000_000);
        let treasuryBalance = (await tezos.tz.getBalance(treasury)).toNumber();
        assert.equal(treasuryBalance - previousTreasuryBalance, 15_000_000);
    });

    async function settle(options: any = {}) {
        let actor = options.actor || alice;
        let param = {
            owner: alice.pkh,
            address: bob.pkh,
            label: encodeString(options.label || 'test'),
            data: new MichelsonMap(),
        };
        param.data.set('foo', encodeString('bar'));
        await registrarDeployment.callProxy('Settle', param, actor.sk, 0);
    }

    async function verifyRecord(label: string, baseTime: number, duration: number) {
        let registrarRecord = await registrarStore.records.get(encodeString(label));
        let expectedExpiry = baseTime + duration * 24 * 60 * 60 * 1000;
        assert.equal(Date.parse(registrarRecord.expiry), expectedExpiry);

        let registryRecord = await registryStore.records.get(encodeString(`${label}.tez`));
        assert.equal(registryRecord.address, bob.pkh);
        assert.equal(registryRecord.owner, alice.pkh);
        assert.equal(registryRecord.data.get('foo'), encodeString('bar'));

        let actualExpiry = await registryStore.expiry_map.get(encodeString(`${label}.tez`));
        assert.equal(actualExpiry, registrarRecord.expiry);
    }
});
