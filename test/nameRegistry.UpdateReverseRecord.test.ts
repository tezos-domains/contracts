import { Contract, MichelsonMap } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';
import { setDomain } from './nameRegistry.common';

describe('NameRegistry.UpdateReverseRecord', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registryDeployment: UpgradeableDeployment;
    let registry: Contract;
    let store: any;

    before(async () => {
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'tez', alice.pkh, alice.pkh, {}, 1);
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh);
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('expired.tez'));
        setDomain(records, 'not-expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('not-expired.tez'));

        let expiryMap = new MichelsonMap<string, Date>();
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));
        expiryMap.set(encodeString('not-expired.tez'), new Date(2100, 0, 0));

        let reverseRecords = new MichelsonMap();
        reverseRecords.set(alice.pkh, {
            name: null,
            owner: alice.pkh,
            internal_data: new MichelsonMap(),
        });
        reverseRecords.set(bob.pkh, {
            name: null,
            owner: alice.pkh,
            internal_data: new MichelsonMap(),
        });
        reverseRecords.set(eve.pkh, {
            name: null,
            owner: eve.pkh,
            internal_data: new MichelsonMap(),
        });

        registryDeployment = await nameRegistryUpgradeable.deploy(
            { actionNames: ['UpdateReverseRecord'] },
            {
                records,
                expiry_map: expiryMap,
                reverse_records: reverseRecords,
            }
        );

        registry = registryDeployment.contract;
        let mainStore: any = await registry.storage();
        store = mainStore.store;
    });

    async function updateReverseRecord(options: any = {}) {
        let sender = options.sender || alice;
        let name = options.name !== undefined ? options.name : `alice.tez`;
        let param = {
            address: options.address || alice.pkh,
            name: name != null ? encodeString(name) : null,
            owner: alice.pkh,
        };
        await registryDeployment.callProxy('UpdateReverseRecord', param, sender.sk, options.amount);
    }

    it('should update reverse record', async () => {
        await updateReverseRecord();

        let record = await store.reverse_records.get(alice.pkh);
        assert.equal(record.name, encodeString('alice.tez'));
        assert.equal(record.owner, alice.pkh);
    });

    it('should update reverse record with no name', async () => {
        await updateReverseRecord({ name: null });

        let record = await store.reverse_records.get(alice.pkh);
        assert.isNull(record.name);
        assert.equal(record.owner, alice.pkh);
    });

    it("should update other account's reverse record", async () => {
        await updateReverseRecord({ address: bob.pkh, name: 'bob.tez' });

        let record = await store.reverse_records.get(bob.pkh);
        assert.equal(record.name, encodeString('bob.tez'));
        assert.equal(record.owner, alice.pkh);
    });

    itShouldFail(updateReverseRecord, 'NAME_ADDRESS_MISMATCH', { address: alice.pkh, name: 'bob.tez', when: 'corresponding domain points elsewhere' });
    itShouldFail(updateReverseRecord, 'NAME_ADDRESS_MISMATCH', {
        address: eve.pkh,
        sender: eve,
        name: 'unknown.tez',
        when: 'corresponding domain does not exist',
    });
    itShouldFail(updateReverseRecord, 'NAME_ADDRESS_MISMATCH', { address: eve.pkh, sender: eve, name: 'expired.tez', when: 'corresponding domain expired' });
    itShouldFail(updateReverseRecord, 'NOT_AUTHORIZED', { sender: bob });
    itShouldFail(updateReverseRecord, 'RECORD_NOT_FOUND', { address: 'tz1VBLpuDKMoJuHRLZ4HrCgRuiLpEr7zZx2E' });
    itShouldFail(updateReverseRecord, 'AMOUNT_NOT_ZERO', { amount: 123 });
});
