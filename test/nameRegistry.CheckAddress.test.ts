import { MichelsonMap } from '@taquito/taquito';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';
import { setDomain } from './nameRegistry.common';

describe('NameRegistry.CheckAddress', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;

    let registryUpgradeable: UpgradeableDeployment;

    before(async () => {
        // deploy new
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, { test: '1234' });
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh, {}, 2, null);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh);

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('alice.tez'), new Date(2500, 0, 0));
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        registryUpgradeable = await nameRegistryUpgradeable.deploy(
            { actionNames: ['CheckAddress'] },
            {
                records,
                expiry_map: expiryMap,
            }
        );
    });

    it('should succeed', async () => checkAddress({ name: 'alice.tez', address: alice.pkh }));
    it('should succeed when no expiration', async () => checkAddress({ name: 'bob.tez', address: bob.pkh }));

    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: 'alice.tez', address: bob.pkh, when: 'not matching' });
    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: 'bob.tez', address: alice.pkh, when: 'not matching with no expiry' });
    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: 'expired.tez', address: eve.pkh, when: 'expired' });
    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: 'unknown.tez', address: alice.pkh, when: "domain doesn't exist" });
    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: '9(*&!@#', address: alice.pkh, when: 'name invalid' });
    itShouldFail(checkAddress, 'NAME_ADDRESS_MISMATCH', { name: '', address: alice.pkh, when: 'name empty' });

    async function checkAddress(options: any = {}) {
        let param = {
            name: encodeString(options.name),
            address: options.address,
        };
        await registryUpgradeable.callProxy('CheckAddress', param, alice.sk, options.amount);
    }
});
