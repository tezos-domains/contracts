import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import { operators } from '../common/nameRegistry.internalData';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';
import { setDomain, packOperators } from './nameRegistry.common';

describe('NameRegistry.UpdateRecord', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registryDeployment: UpgradeableDeployment;
    let registry: Contract;
    let store: any;
    let tezos: TezosToolkit;

    before(async () => {
        tezos = tezosToolkit();

        let records = new MichelsonMap<string, any>();
        setDomain(records, 'tez', alice.pkh, alice.pkh, {}, 1);
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, {}, 2, encodeString('alice.tez'));
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('expired.tez'));
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh);
        setDomain(records, 'eve1.tez', eve.pkh, eve.pkh);
        setDomain(records, 'eve2.tez', eve.pkh, eve.pkh);
        setDomain(records, 'charlie.tez', eve.pkh, eve.pkh, {}, 2, encodeString('charlie.tez'), {
            operators: packOperators([alice.pkh]),
            other: encodeString('a'),
        });
        setDomain(records, 'david.tez', eve.pkh, eve.pkh, {}, 2, encodeString('david.tez'), {
            operators: packOperators([alice.pkh]),
            other: encodeString('a'),
        });
        setDomain(records, 'foo.tez', alice.pkh, null);
        setDomain(records, 'operated.tez', alice.pkh, alice.pkh, {}, 2, encodeString('alice.tez'));

        let reverseRecords = new MichelsonMap();
        reverseRecords.set(bob.pkh, {
            name: encodeString('bob.tez'),
            owner: bob.pkh,
            internal_data: new MichelsonMap(),
        });
        reverseRecords.set(eve.pkh, {
            name: encodeString('eve1.tez'),
            owner: eve.pkh,
            internal_data: new MichelsonMap(),
        });

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('alice.tez'), new Date(2100, 0, 0));
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let tokens = new MichelsonMap();
        tokens.set(1, encodeString('operated.tez'));

        registryDeployment = await nameRegistryUpgradeable.deploy(
            { actionNames: ['UpdateRecord', 'UpdateOperators'] },
            {
                records,
                reverse_records: reverseRecords,
                expiry_map: expiryMap,
                tzip12_tokens: tokens,
            }
        );

        registry = registryDeployment.contract;
        let mainStore: any = await registry.storage();
        store = mainStore.store;
    });

    async function updateRecord(options: any = {}) {
        let param = {
            name: encodeString(options.name || 'alice.tez'),
            address: options.address || null,
            owner: options.owner || alice.pkh,
            data: MichelsonMap.fromLiteral({
                hero: encodeString('Joker'),
            }),
        };
        let sender = options.sender || alice;
        await registryDeployment.callProxy('UpdateRecord', param, sender.sk, options.amount);
    }

    async function updateOperators(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.update_operators(options.operators || []).send({ amount: options.amount || 0 }));
    }

    it('should update existing record', async () => {
        await updateRecord({ address: bob.pkh, owner: bob.pkh });

        let record = await store.records.get(encodeString('alice.tez'));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, bob.pkh);
        assert.equal(record.data.get('hero'), encodeString('Joker'));
    });

    it('should update existing record on behalf on its owner', async () => {
        await updateOperators({ operators: [{ add_operator: { token_id: 1, operator: eve.pkh, owner: alice.pkh } }] });

        await updateRecord({ name: 'operated.tez', address: bob.pkh, owner: bob.pkh, sender: eve });

        let record = await store.records.get(encodeString('operated.tez'));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, bob.pkh);
        assert.equal(record.data.get('hero'), encodeString('Joker'));
    });

    it('should update existing record with no previous address', async () => {
        await updateRecord({ name: 'foo.tez', address: bob.pkh, owner: bob.pkh });

        let record = await store.records.get(encodeString('foo.tez'));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, bob.pkh);
        assert.equal(record.data.get('hero'), encodeString('Joker'));
    });

    it('should clear operators when owner is changed', async () => {
        await updateRecord({ name: 'charlie.tez', address: bob.pkh, owner: bob.pkh, sender: eve });

        let record = await store.records.get(encodeString('charlie.tez'));
        assert.equal(record.internal_data.get('other'), encodeString('a'));
        assert.isUndefined(record.internal_data.get(operators.key));
    });

    it('should not clear operators when owner is not changed', async () => {
        await updateRecord({ name: 'david.tez', address: bob.pkh, owner: eve.pkh, sender: eve });

        let record = await store.records.get(encodeString('david.tez'));
        assert.equal(record.internal_data.get('other'), encodeString('a'));
        assert.isDefined(record.internal_data.get(operators.key));
    });

    it('should not change reverse record if same address but mapped to different name', async () => {
        await updateRecord({
            name: `eve2.tez`,
            address: alice.pkh,
            sender: eve,
        });

        let record = await store.reverse_records.get(eve.pkh);
        assert.equal(record.name, encodeString('eve1.tez'));
        assert.equal(record.owner, eve.pkh);
    });

    it('should remove reverse record address when setting address to empty', async () => {
        await updateRecord({
            sender: bob,
            name: 'bob.tez',
        });

        let record = await store.reverse_records.get(bob.pkh);
        assert.isNull(record.name);
        assert.equal(record.owner, bob.pkh);
    });

    it('should remove reverse record address when setting address to different', async () => {
        await updateRecord({
            name: `eve1.tez`,
            address: alice.pkh,
            sender: eve,
        });

        let record = await store.reverse_records.get(eve.pkh);
        assert.isNull(record.name);
        assert.equal(record.owner, eve.pkh);
    });

    itShouldFail(updateRecord, 'NOT_AUTHORIZED', { sender: eve }); // eve is not the owner or operator
    itShouldFail(updateRecord, 'AMOUNT_NOT_ZERO', { amount: 123, sender: bob });
    itShouldFail(updateRecord, 'RECORD_NOT_FOUND', { name: 'nonsense', when: 'record does not exist' });
    itShouldFail(updateRecord, 'RECORD_NOT_FOUND', { name: 'expired.tez', when: 'record expired' });
});
