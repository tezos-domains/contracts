import { assert } from 'chai';
import accounts from '../common/sandbox-accounts';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { setDomain } from './nameRegistry.common';
import { encodeString } from '../common/convert';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { UpgradeableDeployment } from '../common/upgradeables';
import { invalidLabels, itShouldFail, validLabels } from './common';
import { InMemorySigner } from '@taquito/signer';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { childrenAreTzip12Tokens } from '../common/nameRegistry.internalData';

describe('NameRegistry.SetChildRecord', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let registryDeployment: UpgradeableDeployment;
    let registry: Contract;
    let store: any;
    let tezos: TezosToolkit;

    before(async () => {
        tezos = tezosToolkit();

        let records = new MichelsonMap<string, any>();
        setDomain(records, 'tez', alice.pkh, alice.pkh, {}, 1, null, { [childrenAreTzip12Tokens.key]: childrenAreTzip12Tokens.values.direct });
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh, {}, 2, encodeString('expired.tez'));
        setDomain(records, 'bar.tez', bob.pkh, bob.pkh);
        setDomain(records, 'baz.tez', bob.pkh, bob.pkh);
        setDomain(records, 'foo.tez', eve.pkh, eve.pkh);
        setDomain(records, 'operated.tez', alice.pkh, alice.pkh);
        setDomain(records, 'direct-children-are-tzip12-tokens.tez', alice.pkh, alice.pkh, {}, 2, null, {
            [childrenAreTzip12Tokens.key]: childrenAreTzip12Tokens.values.direct,
        });
        setDomain(records, 'unset-children-are-tzip12-tokens.tez', alice.pkh, alice.pkh, {}, 2);

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let reverseRecords = new MichelsonMap();
        reverseRecords.set(eve.pkh, {
            name: encodeString('foo.tez'),
            owner: eve.pkh,
            internal_data: new MichelsonMap(),
        });
        reverseRecords.set(bob.pkh, {
            name: encodeString('bar.tez'),
            owner: bob.pkh,
            internal_data: new MichelsonMap(),
        });

        let tokens = new MichelsonMap();
        tokens.set(10, encodeString('operated.tez'));

        registryDeployment = await nameRegistryUpgradeable.deploy(
            { actionNames: ['SetChildRecord', 'UpdateOperators'] },
            {
                records,
                expiry_map: expiryMap,
                reverse_records: reverseRecords,
                tzip12_tokens: tokens,
            }
        );
        registry = registryDeployment.contract;
        store = (await registry.storage<any>()).store;
    });

    async function setChildRecord(options: any = {}) {
        let param = {
            address: options.address !== undefined ? options.address : bob.pkh,
            data: MichelsonMap.fromLiteral({
                hero: encodeString('Batman'),
            }),
            label: encodeString(options.label !== undefined ? options.label : 'test'),
            owner: options.owner || alice.pkh,
            parent: encodeString(options.parent || 'tez'),
            expiry: new Date(5555555555000),
        };
        let sender = options.sender || alice;
        await registryDeployment.callProxy('SetChildRecord', param, sender.sk, options.amount);
    }

    async function updateOperators(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));
        await runOperation(() => registry.methods.update_operators(options.operators || []).send({ amount: options.amount || 0 }));
    }

    it('should create record for given label', async () => {
        await setChildRecord({ label: 'alice' });

        let record = await store.records.get(encodeString(`alice.tez`));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, alice.pkh);
        assert.equal(record.data.get('hero'), encodeString('Batman'));
        assert.equal(record.expiry_key, encodeString(`alice.tez`));
        assert.equal(record.level, 2);
        assert.equal(record.tzip12_token_id, 1);
        let actualExpiry = await store.expiry_map.get(encodeString('alice.tez'));
        assert.equal(Date.parse(actualExpiry), 5555555555000);

        let token = await store.tzip12_tokens.get('1');
        assert.equal(token, encodeString('alice.tez'));
    });

    it('should create subdomain on behalf of parent owner', async () => {
        await updateOperators({ operators: [{ add_operator: { token_id: 10, operator: eve.pkh, owner: alice.pkh } }] });

        await setChildRecord({ label: 'foo', parent: 'operated.tez', sender: eve });

        let record = await store.records.get(encodeString(`foo.operated.tez`));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, alice.pkh);
        assert.equal(record.data.get('hero'), encodeString('Batman'));
        assert.equal(record.expiry_key, encodeString(`operated.tez`));
        assert.equal(record.level, 3);
        assert.isNull(record.tzip12_token_id);
    });

    it('should create subdomain', async () => {
        await setChildRecord({ label: 'foo', parent: 'alice.tez' });

        let record = await store.records.get(encodeString(`foo.alice.tez`));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, alice.pkh);
        assert.equal(record.data.get('hero'), encodeString('Batman'));
        assert.equal(record.expiry_key, encodeString(`alice.tez`));
        assert.equal(record.level, 3);
        assert.isNull(record.tzip12_token_id);
    });

    it('should create subsubdomain', async () => {
        await setChildRecord({ label: 'bar', parent: 'foo.alice.tez' });

        let record = await store.records.get(encodeString(`bar.foo.alice.tez`));
        assert.equal(record.address, bob.pkh);
        assert.equal(record.owner, alice.pkh);
        assert.equal(record.data.get('hero'), encodeString('Batman'));
        assert.equal(record.expiry_key, encodeString(`alice.tez`));
        assert.equal(record.level, 4);
        assert.isNull(record.tzip12_token_id);
    });

    it('should create record with incremented token_id when parent tzip12_children is set to direct', async () => {
        await setChildRecord({ label: 'banana', parent: 'direct-children-are-tzip12-tokens.tez' });

        let record = await store.records.get(encodeString(`banana.direct-children-are-tzip12-tokens.tez`));
        assert.equal(record.tzip12_token_id, 2);

        let token = await store.tzip12_tokens.get('2');
        assert.equal(token, encodeString('banana.direct-children-are-tzip12-tokens.tez'));
    });

    it('should update record and re-use token_id when parent tzip12_children is set to direct', async () => {
        await setChildRecord({ label: 'banana', parent: 'direct-children-are-tzip12-tokens.tez' });

        let record = await store.records.get(encodeString(`banana.direct-children-are-tzip12-tokens.tez`));
        assert.equal(record.tzip12_token_id, 2);

        let token = await store.tzip12_tokens.get('2');
        assert.equal(token, encodeString('banana.direct-children-are-tzip12-tokens.tez'));
    });

    it("shouldn't create record with incremented token_id when parent tzip12_children is unset", async () => {
        await setChildRecord({ label: 'grape', parent: 'unset-children-are-tzip12-tokens.tez' });

        let record = await store.records.get(encodeString(`grape.unset-children-are-tzip12-tokens.tez`));
        assert.isNull(record.tzip12_token_id);

        let token = await store.tzip12_tokens.get('3');
        assert.isUndefined(token);
    });

    it('should not change reverse record with same address but mapped to different name', async () => {
        await setChildRecord({
            label: `baz`,
            address: alice.pkh,
        });

        let record = await store.reverse_records.get(bob.pkh);
        assert.equal(record.name, encodeString('bar.tez'));
        assert.equal(record.owner, bob.pkh);
    });

    it('should remove reverse record address when setting address to empty', async () => {
        await setChildRecord({
            label: `foo`,
            address: null,
        });

        let record = await store.reverse_records.get(eve.pkh);
        assert.isNull(record.name);
        assert.equal(record.owner, eve.pkh);
    });

    it('should remove reverse record address when setting address to different', async () => {
        await setChildRecord({
            label: `bar`,
            address: alice.pkh,
        });

        let record = await store.reverse_records.get(bob.pkh);
        assert.isNull(record.name);
        assert.equal(record.owner, bob.pkh);
    });

    for (let label of validLabels) {
        it(`should succeed on valid label '${label}'`, async () => {
            await setChildRecord({ label });
        });
    }

    for (let i = 0; i < invalidLabels.length; i++) {
        let label = invalidLabels[i];
        itShouldFail(setChildRecord, 'INVALID_LABEL', { label, when: `called with label ${i}: "${label}"` });
    }
    itShouldFail(setChildRecord, 'LABEL_EMPTY', { label: '' });

    itShouldFail(setChildRecord, 'NOT_AUTHORIZED', { sender: bob }); // bob is not the owner or operator
    itShouldFail(setChildRecord, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(setChildRecord, 'LABEL_TOO_LONG', {
        label: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    }); // 101 characters
    itShouldFail(setChildRecord, 'NAME_TOO_LONG', {
        label: 'a'.repeat(100),
        parent: `${'b'.repeat(100)}.${'c'.repeat(100)}.${'d'.repeat(98)}.tez`, // 400 is max
    });
    itShouldFail(setChildRecord, 'PARENT_NOT_FOUND', { parent: 'nonsense', label: 'test', when: 'parent does not exist' });
    itShouldFail(setChildRecord, 'PARENT_NOT_FOUND', { parent: 'expired.tez', label: 'test', when: 'parent expired' });
});
