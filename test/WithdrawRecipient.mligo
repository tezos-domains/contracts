type param = 
    | Foo of unit
    | Bar of nat

let main (p, s : param * unit) : (operation list * unit) =
    ([] : operation list), ()
