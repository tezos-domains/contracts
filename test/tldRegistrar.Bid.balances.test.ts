import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
import { UpgradeableDeployment } from '../common/upgradeables';
import { getTezosTime, itShouldFail } from './common';

describe('TLDRegistrar.Bid', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;

    let time: number;
    let registrarDeployment: UpgradeableDeployment;
    let registrarStore: any;

    before(async () => {
        time = await getTezosTime();

        await nameRegistryUpgradeable.deploy({ actionNames: [] });

        registrarDeployment = await tldRegistrarUpgradeable.deploy({
            launchDates: [new Date(time)],
            actionNames: ['Bid'],
        });

        let mainStorage: any = await registrarDeployment.contract.storage();
        registrarStore = mainStorage.store;
    });

    it('should not create balance when bid = amount', async () => {
        await bid({ label: 'foo1', bid: 1000000 });
        assert.isUndefined(await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should create balance when bid < amount', async () => {
        await bid({ label: 'foo2', bid: 1000000, amount: 2 });
        assert.equal(1_000_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should add balance when bid < amount', async () => {
        await bid({ label: 'foo3', bid: 1000000, amount: 2 });
        assert.equal(2_000_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    itShouldFail(bid, 'AMOUNT_TOO_LOW', { label: 'foo4', bid: 2500001, amount: 0.5, when: 'used balance is too low' });

    it('should use balance when bid > amount', async () => {
        await bid({ label: 'foo4', bid: 1000000, amount: 0.5 });
        assert.equal(1_500_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should remove balance when bid > amount', async () => {
        await bid({ label: 'foo5', bid: 1500000, amount: 0 });
        assert.isUndefined(await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should create balance for overbid account when same account', async () => {
        await bid({ label: 'bar1', bid: 1000000 });
        await bid({ label: 'bar1', bid: 1100000 });
        assert.equal(1_000_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should add balance for overbid account when same account', async () => {
        await bid({ label: 'bar1', bid: 1200000 });
        assert.equal(2_100_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should add balance for overbid account when different account', async () => {
        await bid({ label: 'bar1', bid: 1300000, actor: bob });
        assert.equal(3_300_000, await registrarStore.bidder_balances.get(alice.pkh));
    });

    it('should create balance for overbid account when different account', async () => {
        await bid({ label: 'bar2', bid: 1000000, actor: bob });
        await bid({ label: 'bar2', bid: 1100000, actor: eve });
        assert.equal(1_000_000, await registrarStore.bidder_balances.get(bob.pkh));
    });

    async function bid(options: any = {}) {
        let param = {
            label: encodeString(options.label),
            bid: options.bid || 1000000,
        };
        let amount = options.amount;
        if (amount === undefined) {
            amount = (options.bid ? options.bid / 1000000 : undefined) || 1;
        }
        await registrarDeployment.callProxy('Bid', param, (options.actor || alice).sk, amount);
    }
});
