import { MichelsonMap } from '@taquito/taquito';
import { encodeString } from '../common/convert';
import accounts from '../common/sandbox-accounts';
const alice = accounts.alice;

export function setRecord(records: MichelsonMap<string, any>, label: string, expiry: Date) {
    records.set(encodeString(label), {
        expiry: expiry,
        internal_data: new MichelsonMap(),
    });
}

export function setAuction(auctions: MichelsonMap<string, any>, label: string, lastBid: number, endsAt: Date) {
    auctions.set(encodeString(label), {
        last_bid: lastBid,
        last_bidder: alice.pkh,
        ends_at: endsAt,
        ownership_period: 365,
    });
}
