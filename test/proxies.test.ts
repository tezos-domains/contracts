import { InMemorySigner } from '@taquito/signer';
import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import noop from '../common/lambdas/noop';
import proxy_updateContract from '../common/lambdas/proxy_updateContract';
import accounts from '../common/sandbox-accounts';
import { tezosToolkit } from '../common/tezos-toolkit';
import { Upgradeable } from '../common/upgradeables';
import { assertThrowsAsync } from './common';

describe('(proxy)', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    let registryDeployment;
    let registrarDeployment;

    before(async () => {
        registryDeployment = await nameRegistryUpgradeable.deploy();
        registrarDeployment = await tldRegistrarUpgradeable.deploy();
    });

    testProxies(nameRegistryUpgradeable);
    testProxies(tldRegistrarUpgradeable);

    function testProxies(upgradeable: Upgradeable) {
        upgradeable.actions
            .filter(action => action.proxy)
            .forEach(action => {
                it(`${action.name}.proxy_admin_update should update contract address`, async () => {
                    const newContractAddress = 'KT1B6xdhCtf9i5Y2YHFUksAy7QMbT5zjwRHp';
                    let tezos = tezosToolkit();
                    tezos.setSignerProvider(new InMemorySigner(alice.sk));

                    let contract = await tezos.contract.at(upgradeable.lastDeployment.proxies[action.name].proxyContract.address);
                    let op = await contract.methods.proxy_admin_update(proxy_updateContract(newContractAddress)).send();
                    await op.confirmation();

                    // assert the contract address was changed
                    let storage: any = await contract.storage();
                    assert.equal(storage.contract, newContractAddress);
                    assert.equal(storage.owner, alice.pkh);
                });

                it(`${action.name}.proxy_admin_update should fail with NOT_AUTHORIZED`, async () => {
                    let tezos = tezosToolkit();
                    tezos.setSignerProvider(new InMemorySigner(bob.sk));
                    let contract = await tezos.contract.at(upgradeable.lastDeployment.proxies[action.name].proxyContract.address);
                    await assertThrowsAsync(async () => await contract.methods.proxy_admin_update(noop).send(), 'NOT_AUTHORIZED');
                });
            });
    }
});
