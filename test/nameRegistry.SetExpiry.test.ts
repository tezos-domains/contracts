import { assert } from 'chai';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { encodeString } from '../common/convert';
import nameRegistry_addTrustedSenders from '../common/lambdas/nameRegistry_addTrustedSenders';
import nameRegistry_setRecord from '../common/lambdas/nameRegistry_setRecord';
import accounts from '../common/sandbox-accounts';
import { runOperation } from '../common/tezos-toolkit';
import { UpgradeableDeployment } from '../common/upgradeables';
import { itShouldFail } from './common';

describe('NameRegistry.SetExpiry', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    let registryDeployment: UpgradeableDeployment;
    let store: any;

    before(async () => {
        registryDeployment = await nameRegistryUpgradeable.deploy({ actionNames: ['SetExpiry'] });
        let registry = registryDeployment.contract;
        let mainStore: any = await registry.storage();
        store = mainStore.store;
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord('tez', alice.pkh, 1)).send());
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_setRecord('alice.tez', alice.pkh, 2)).send());
        await runOperation(async () => await registry.methods.admin_update(await nameRegistry_addTrustedSenders(alice.pkh)).send());
    });

    async function setExpiry(options: any = {}) {
        let param = {
            label: encodeString('alice'),
            parent: encodeString(options.parent || 'tez'),
            expiry: new Date(7766666666000),
        };
        let paramType = {
            prim: 'pair',
            args: [
                {
                    prim: 'bytes',
                    annots: ['%label'],
                },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'bytes',
                            annots: ['%parent'],
                        },
                        {
                            prim: 'option',
                            args: [
                                {
                                    prim: 'timestamp',
                                },
                            ],
                            annots: ['%expiry'],
                        },
                    ],
                },
            ],
        };
        let sender = options.sender || alice;

        await registryDeployment.callAction('SetExpiry', param, paramType, sender.pkh, options.amount);
    }

    it('should set only expiry', async () => {
        await setExpiry();

        let actualExpiry = await store.expiry_map.get(encodeString('alice.tez'));
        assert.equal(Date.parse(actualExpiry), 7766666666000);
    });

    itShouldFail(setExpiry, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(setExpiry, 'PARENT_NOT_TLD', { parent: 'alice.tez' });
    itShouldFail(setExpiry, 'NOT_AUTHORIZED', { sender: bob });
    itShouldFail(setExpiry, 'PARENT_NOT_FOUND', { parent: 'unknown' });
});
