import { assert } from 'chai';
import { tezosToolkit } from '../common/tezos-toolkit';

export async function assertThrowsAsync(fn: () => Promise<any>, expectedError: string) {
    try {
        await fn();
    } catch (e) {
        assert.equal(e.message, expectedError);
        return;
    }
    assert.fail('did not fail');
}

export async function getTezosTime(block?: number) {
    let tezos = tezosToolkit();
    let header = await tezos.rpc.getBlockHeader({ block: block ? block.toString() : 'head' });
    return Date.parse(header.timestamp);
}

export function itShouldFail(act: (options: any) => Promise<any>, expectedError: string, options: any = {}) {
    it(`should fail with ${expectedError}${options.when ? ` when ${options.when}` : ''}`, async () => {
        await (options.beforeTest ? options.beforeTest() : null);

        await assertThrowsAsync(() => act(options), expectedError);
    });
}

export const validLabels = ['valid', 'eve-123', '9gagg', 'more---hyphens'];
export const invalidLabels = ['ali_ce', 'wtf?!', 'dot.here', 'Upper', '-hypen-start', 'hypen-end-', 'tezoš', 'with space', ' ', '\t', '\n'];
