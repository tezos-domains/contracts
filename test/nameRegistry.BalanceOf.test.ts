import { assert } from 'chai';
import { Contract, MichelsonMap } from '@taquito/taquito';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { setDomain } from './nameRegistry.common';
import accounts from '../common/sandbox-accounts';
import { encodeString } from '../common/convert';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { compileContract } from '../common/compile';
import { itShouldFail } from './common';

describe('NameRegistry.BalanceOf', () => {
    let registry: Contract;
    let callbackAddress: string;
    let callbackContract: Contract;
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;

    before(async () => {
        let tezos = tezosToolkit();

        // deploy new
        let records = new MichelsonMap<string, any>();
        setDomain(records, 'alice.tez', alice.pkh, alice.pkh, { test: '1234' });
        setDomain(records, 'bob.tez', bob.pkh, bob.pkh, {}, 2, null);
        setDomain(records, 'expired.tez', eve.pkh, eve.pkh);

        let expiryMap = new MichelsonMap();
        expiryMap.set(encodeString('alice.tez'), new Date(2500, 0, 0));
        expiryMap.set(encodeString('expired.tez'), new Date(2000, 0, 0));

        let tokens = new MichelsonMap();
        tokens.set(1, encodeString('alice.tez'));
        tokens.set(2, encodeString('bob.tez'));
        tokens.set(3, encodeString('expired.tez'));

        let registryUpgradeable = await nameRegistryUpgradeable.deploy(
            { actionNames: ['BalanceOf'] },
            {
                records,
                expiry_map: expiryMap,
                tzip12_tokens: tokens,
            }
        );
        registry = registryUpgradeable.contract;

        // deploy BalanceOfCallback
        let code = await compileContract('test/BalanceOfCallback.mligo');
        let op = await tezos.contract.originate({
            code: code,
            storage: null,
        });
        callbackAddress = op.contractAddress!;
        await op.confirmation();
        callbackContract = await tezos.contract.at(callbackAddress);
    });

    it('should call back with empty', async () => {
        await balanceOf({ requests: [] });

        let callbackContractStorage: Array<any> = await callbackContract.storage();
        assert.isArray(callbackContractStorage);
        assert.lengthOf(callbackContractStorage, 0);
    });

    it('should call back with one', async () => {
        await balanceOf({ requests: [{ owner: alice.pkh, token_id: 1 }] });

        let callbackContractStorage: Array<any> = await callbackContract.storage();
        assert.isArray(callbackContractStorage);
        assert.lengthOf(callbackContractStorage, 1);
        assert.equal(callbackContractStorage[0].balance, 1);
        assert.equal(callbackContractStorage[0].request.owner, alice.pkh);
        assert.equal(callbackContractStorage[0].request.token_id, 1);
    });

    it('should call back with multiple', async () => {
        await balanceOf({
            requests: [
                { owner: alice.pkh, token_id: 1 },
                { owner: alice.pkh, token_id: 2 },
                { owner: bob.pkh, token_id: 2 },
            ],
        });

        let callbackContractStorage: Array<any> = await callbackContract.storage();
        assert.isArray(callbackContractStorage);
        assert.lengthOf(callbackContractStorage, 3);
        assert.equal(callbackContractStorage[0].balance, 1);
        assert.equal(callbackContractStorage[0].request.owner, alice.pkh);
        assert.equal(callbackContractStorage[0].request.token_id, 1);
        assert.equal(callbackContractStorage[1].balance, 0);
        assert.equal(callbackContractStorage[1].request.owner, alice.pkh);
        assert.equal(callbackContractStorage[1].request.token_id, 2);
        assert.equal(callbackContractStorage[2].balance, 1);
        assert.equal(callbackContractStorage[2].request.owner, bob.pkh);
        assert.equal(callbackContractStorage[2].request.token_id, 2);
    });

    it('should call back with zero balance when expired', async () => {
        await balanceOf({ requests: [{ owner: eve.pkh, token_id: 3 }] });

        let callbackContractStorage: Array<any> = await callbackContract.storage();
        assert.isArray(callbackContractStorage);
        assert.lengthOf(callbackContractStorage, 1);
        assert.equal(callbackContractStorage[0].balance, 0);
        assert.equal(callbackContractStorage[0].request.owner, eve.pkh);
        assert.equal(callbackContractStorage[0].request.token_id, 3);
    });

    itShouldFail(balanceOf, 'FA2_TOKEN_UNDEFINED', { requests: [{ owner: alice.pkh, token_id: 4 }] });

    async function balanceOf(options: any = {}) {
        await runOperation(() => registry.methods.balance_of(options.requests, callbackAddress).send());
    }
});
