import { InMemorySigner } from '@taquito/signer';
import { Contract } from '@taquito/taquito';
import { nameRegistryUpgradeable } from '../common/contracts/nameRegistry.upgradeable';
import { tldRegistrarUpgradeable } from '../common/contracts/tldRegistrar.upgradeable';
import noop from '../common/lambdas/noop';
import accounts from '../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../common/tezos-toolkit';
import { assertThrowsAsync } from './common';

describe('TLDRegistrar', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;

    let registrar: Contract;
    let tezos = tezosToolkit();

    before(async () => {
        await nameRegistryUpgradeable.deploy({ actionNames: [] });
        let registrarDeployment = await tldRegistrarUpgradeable.deploy({ actionNames: [] });

        // we need use Taquito directly to be able to change signers
        registrar = await tezos.contract.at(registrarDeployment.contract.address);
    });

    it('execute should fail with NOT_TRUSTED_SENDER', async () => {
        tezos.setSignerProvider(new InMemorySigner(bob.sk));
        await assertThrowsAsync(() => registrar.methods.execute('Foo', '', alice.pkh).send(), 'NOT_TRUSTED_SENDER');
    });

    it('admin_update should succeed', async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(() => registrar.methods.admin_update(noop).send());
    });

    it('admin_update should fail with NOT_AUTHORIZED', async () => {
        tezos.setSignerProvider(new InMemorySigner(bob.sk));
        await assertThrowsAsync(() => registrar.methods.admin_update(noop).send(), 'NOT_AUTHORIZED');
    });
});
