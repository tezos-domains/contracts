import { MichelsonType, packDataBytes, unpackDataBytes } from '@taquito/michel-codec';
import { MichelsonMap } from '@taquito/taquito';
import { encodeString } from '../common/convert';
import { operators as operatorsData } from '../common/nameRegistry.internalData';

export function setDomain(
    records: MichelsonMap<string, any>,
    name: string,
    owner: string,
    address: string | null,
    data: Record<string, string> = {},
    level: number = 2,
    expiryKey?: string | null,
    internalData: Record<string, string> = {}
) {
    records.set(encodeString(name), {
        owner: owner,
        address: address,
        level: level || 2,
        data: MichelsonMap.fromLiteral(data),
        internal_data: MichelsonMap.fromLiteral(internalData),
        expiry_key: expiryKey === undefined ? encodeString(name) : expiryKey,
    });
}

const OPERATORS_TYPE: MichelsonType = {
    prim: 'set',
    args: [{ prim: 'address' }],
};

export function packOperators(operators: string[]) {
    return packDataBytes(
        operators.map(o => ({ string: o })),
        OPERATORS_TYPE
    ).bytes;
}

export function unpackOperators(record: any) {
    const rawOperators = record.internal_data.get(operatorsData.key);

    const operators = unpackDataBytes({ bytes: rawOperators }, OPERATORS_TYPE) as Array<{ string: string }>;

    return new Set(operators.map(o => o.string));
}
